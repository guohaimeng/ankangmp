package com.hq.dao.auth;

import com.hq.utils.MysqlBaseUtil;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class AuthDao {
    /**
     * 校验微信用户
     * @param openid
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> checkOpenid (String openid) throws SQLException {
        String sql = "select uid,phone_number,grade,openid,mail from t_user where openid=?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(openid);
        return MysqlBaseUtil.getOneRow(sql,params);
    }


    /**
     * 校验手机号
     * @param phone_number
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getMobile (String phone_number) throws SQLException {
        String sql = "select phone_number,grade from t_phone_number where phone_number=?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(phone_number);
        return MysqlBaseUtil.getOneRow(sql,params);
    }


    /**
     * 根据手机号查询邮件
     * @param phone_number
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getMailByMobile (String phone_number) throws SQLException {
        String sql = "select mail from t_user where phone_number=?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(phone_number);
        return MysqlBaseUtil.getOneRow(sql,params);
    }



    /**
     * 绑定手机
     * @param phone_number
     * @param uid
     * @return
     * @throws SQLException
     */
    public static boolean bindPhone (String phone_number,String uid,String grade,String openid) throws SQLException {
        String sql = "insert into t_user (uid,phone_number,grade,openid) values(?,?,?,?)";

//        String sql = "update t_user set phone_number=?,grade=? where uid=?";
//        String upsql = "update t_user set phone_number=null where phone_number=? and uid !=?";
        String upsql = "delete from  t_user where phone_number=? and uid !=?";
        ArrayList<String> sqlList = new ArrayList<>();
        sqlList.add(sql);
        sqlList.add(upsql);

        ArrayList<Object> params = new ArrayList<>();
        params.add(uid);
        params.add(phone_number);
        params.add(grade);
        params.add(openid);
        ArrayList<Object> paramup = new ArrayList<>();
        paramup.add(phone_number);
        paramup.add(uid);


        ArrayList<ArrayList<Object>> paramsList = new ArrayList<>();
        paramsList.add(params);
        paramsList.add(paramup);

        return MysqlBaseUtil.batchUpdateSql(sqlList,paramsList);
    }


    /**
     * 创建用户
     * @param userInfo
     * @return
     * @throws SQLException
     */
    public static boolean createUser (HashMap<String,String> userInfo ) throws SQLException {
        String upsql = "delete from  t_user where phone_number=?";

        ArrayList<Object> paramsup = new ArrayList<>();
        paramsup.add(userInfo.get("phone_number"));


        String sql = "insert into t_user (uid,phone_number,grade,openid) values(?,?,?,?)";
        ArrayList<Object> params = new ArrayList<>();
        params.add(userInfo.get("uid"));
        params.add(userInfo.get("phone_number"));
        params.add(userInfo.get("grade"));
        params.add(userInfo.get("openid"));


        ArrayList<String> sqlList = new ArrayList<>();
        sqlList.add(upsql);
        sqlList.add(sql);

        ArrayList<ArrayList<Object>> paramsList = new ArrayList<>();
        paramsList.add(paramsup);
        paramsList.add(params);




        return  MysqlBaseUtil.batchUpdateSql(sqlList,paramsList);

    }



    /**
     * 根据id查询用户
     * @param uid
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getUserByUid (String uid) throws SQLException {
        String sql = "select uid,phone_number,grade,openid from t_user where uid=?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(uid);
        return MysqlBaseUtil.getOneRow(sql,params);
    }
    /**
     * 根据id查询用户
     * @param uid
     * @return
     * @throws SQLException
     */
    public static boolean modifyUergrade (String uid,String  grade,String phone_number,String mail) throws SQLException {
        String  sql ="update t_user ";
        ArrayList<Object> params = new ArrayList<>();
        String  upsql="";
        ArrayList<Object> paramup = new ArrayList<>();
        if(StringUtils.isNotEmpty(grade) && StringUtils.isEmpty(mail)){
            sql+="set grade=?";
            params.add(grade);
            upsql ="update t_phone_number set grade=? where phone_number=?";
            paramup.add(grade);
            paramup.add(phone_number);
        }else if(StringUtils.isNotEmpty(mail) && StringUtils.isEmpty(grade)){
            sql+="set mail=?";
            params.add(mail);
        }else if(StringUtils.isNotEmpty(mail) && StringUtils.isNotEmpty(grade)){
            sql+="set grade=?,mail=?";
            params.add(grade);
            params.add(mail);
            upsql ="update t_phone_number set grade=? where phone_number=?";
            paramup.add(grade);
            paramup.add(phone_number);
        }else{
           //
        }
        sql+=" where uid=?";
        params.add(uid);

        ArrayList<String> sqlList = new ArrayList<>();
        sqlList.add(sql);
        if(StringUtils.isNotEmpty(upsql)){
            sqlList.add(upsql);
        }

        ArrayList<ArrayList<Object>> paramsList = new ArrayList<>();
        paramsList.add(params);
        if(paramup!=null && paramup.size()>0){
            paramsList.add(paramup);
        }
        return MysqlBaseUtil.batchUpdateSql(sqlList,paramsList);
    }

    /**
     * 解除绑定
     * @param uid
     * @param uid
     * @throws SQLException
     */
    public static  boolean loginOffWx(String uid )throws SQLException{
        boolean flag=false;
        String sql = " delete from t_user  where  uid=?";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(uid);
        int result = MysqlBaseUtil.updateSQL(sql,params);
        if(result>=0){
            flag=true;
        }
        return flag;

    }






}
