package com.hq.filter;

import com.hq.api.utils.ApiErrorCode;
import com.hq.api.utils.CheckParameterException;
import com.hq.api.utils.CheckParameters;
import com.hq.api.utils.DatabaseManager;
import com.hq.dao.auth.AuthDao;
import com.hq.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GlobalFilter implements Filter {
	private static final Logger m_logger = Logger.getLogger(GlobalFilter.class);
	public GlobalFilter() {
	}

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		req.setCharacterEncoding("UTF-8");
		String contentEncrypt = req.getHeader("Content-encrypt");
		HttpServletResponse res = (HttpServletResponse) response;
		res.setCharacterEncoding("UTF-8");
		res.setHeader("Access-Control-Allow-Origin", "*");
		res.setHeader("Access-Control-Allow-Methods", "*");
		res.setHeader("Content-type", "application/json;charset=UTF-8");




		String requestURL = req.getRequestURL().toString();
		try {
			if(!StringUtils.contains(requestURL,"/auth/")){
				String ss = req.getParameter("ss");
				Map<String,String[]> ssParameters = new HashMap<>();
				ssParameters.put("ss",new String[]{ss});
				CheckParameters check = new CheckParameters(request.getParameterMap());
				m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
				check.addParameter("ss", CheckParameters.paraType.EXP, Config.getInstance().getString("params_ss"));

				// session校验
				Map<String, String> selfInfo = DatabaseManager.checkSession(check.opt("ss").toString());
				if (selfInfo == null || selfInfo.size() == 0) {
					String ret = ApiErrorCode.echoErr(ApiErrorCode.SESSION_INVALID);
					m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SESSION_INVALID));
					response.getWriter().write(ret);
					return;
				}
				String uid = selfInfo.get("uid");
				selfInfo = AuthDao.getUserByUid(uid);
				if (selfInfo == null || selfInfo.size() == 0) {
					DatabaseManager.redisdelKey(new String []{check.opt("ss").toString(),uid});
					String ret = ApiErrorCode.echoErr(ApiErrorCode.SESSION_INVALID);
					m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.SESSION_INVALID));
					response.getWriter().write(ret);
					return;
				}
				if(StringUtils.isEmpty(selfInfo.get("phone_number"))){
					String ret = ApiErrorCode.echoErr(ApiErrorCode.no_mobile);
					m_logger.info(String.format("OUTPUT ret_code=%s ", ApiErrorCode.no_mobile));
					response.getWriter().write(ret);
					return;
				}

				req.getSession().setAttribute("selfInfo",selfInfo);
			}
		} catch (CheckParameterException e) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.PARAMETER_ERROR));
			response.getWriter().write(ret);
			return;
		}catch (Exception e){
			res.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
			m_logger.error("GlobalFilter error",e);
			return;
		}
		chain.doFilter(req, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}
	
	

}
