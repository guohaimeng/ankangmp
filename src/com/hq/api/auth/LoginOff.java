package com.hq.api.auth;

import com.hq.api.utils.ApiErrorCode;
import com.hq.api.utils.CheckParameterException;
import com.hq.api.utils.CheckParameters;
import com.hq.api.utils.DatabaseManager;
import com.hq.dao.auth.AuthDao;
import com.hq.dispatcher.Config;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 退出登录
 */
@WebServlet( "/auth/loginOff.do")
public class LoginOff extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(LoginOff.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
            //参数校验
            check.addParameter("ss", CheckParameters.paraType.EXP, Config.getInstance().getString("params_ss"));
            //session校验
            Map<String, String> selfInfo= DatabaseManager.checkSession(check.opt("ss").toString());
            if(selfInfo == null || selfInfo.size() == 0){
                String ret=ApiErrorCode.echoErr(ApiErrorCode.SESSION_INVALID);
                m_logger.info(String.format("OUTPUT  ret_code=%s",ApiErrorCode.SESSION_INVALID));
                response.getWriter().write(ret);
                return;
            }

            //解除绑定
            if(AuthDao.loginOffWx(selfInfo.get("uid").toString())){
                String ret=ApiErrorCode.echoOk();
                response.getWriter().write(ret);
                m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.SUCCESS));
                return;
            }else{
                String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
                m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.SYSTEM_ERROR));
                response.getWriter().write(ret);
                return;
            }
        }catch (CheckParameterException e) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR,e));
            response.getWriter().write(ret);
            return;
        }catch(Exception e){
            String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.SYSTEM_ERROR,e));
            response.getWriter().write(ret);
            return;
        }

    }


}
