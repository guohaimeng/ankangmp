/**
 * 系统登录
 */
package com.hq.api.auth;

import com.hq.api.utils.*;
import com.hq.dao.auth.AuthDao;
import com.hq.dispatcher.Startup;
import com.hq.utils.aes.AesCBC;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;

/**
 * 微信登录接口
 *
 */
@WebServlet("/auth/wxlogin.do")
public class Wxlogin extends HttpServlet {

	private static final long serialVersionUID = -8030229540888167965L;
	private static final Logger m_logger = Logger.getLogger(Wxlogin.class);
    private static final String WX_APPID="wx159dd5a8558de297";
    private static final String WX_APPSECRET="d393011647de2d6f11e77271843e43de";
    private static final String WX_API_URL="https://api.weixin.qq.com/sns/jscode2session";
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			//返回结果
			HashMap<String, Object> map = new HashMap<String, Object>();
			//参数校验
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s", StringUtils.replace(check.getParameterJSON().toString()," ","+")));
			check.addParameter("code", CheckParameters.paraType.STRING,1,50);
			check.addParameter("encrypteddata", CheckParameters.paraType.STRING,1,800);
			check.addParameter("iv", CheckParameters.paraType.STRING,1,50);


			String  encrypteddata = StringUtils.replace(check.get("encrypteddata").toString()," ","+");
			String iv =  StringUtils.replace(check.get("iv").toString()," ","+");

            String params = "appid="+WX_APPID+"&secret="+WX_APPSECRET+"&js_code="+check.get("code").toString()+"&grant_type=authorization_code";

			String result = HttpRequest.get(WX_API_URL,params);
			m_logger.info("wxapi_result:"+result);
			JSONObject wxresult = new JSONObject(result);
			String openid=wxresult.getString("openid");

			HashMap<String,String> userInfo = AuthDao.checkOpenid(openid);

			String ss = Tools.getRandomStr(30);

			if(userInfo.size()==0){
				HashMap<String,String>	phoneInfo = new HashMap<>();
				String decryptedData = AesCBC.getInstance().decrypt(encrypteddata,"utf-8",wxresult.getString("session_key"),iv);
				m_logger.info("decryptedData:"+decryptedData);
				String wxphoneNumber = new JSONObject(decryptedData).getString("phoneNumber");
				if(!StringUtils.isEmpty(wxphoneNumber)){
					phoneInfo  = AuthDao.getMobile(wxphoneNumber);
				}
				userInfo.put("uid", String.valueOf(Startup.getId()));
				userInfo.put("openid", openid);
				if(StringUtils.isEmpty(phoneInfo.get("phone_number"))){
					map.put("isbind", "0");
					userInfo.put("phone_number", "");
					userInfo.put("grade", "");
				}else {
					userInfo.put("phone_number", phoneInfo.get("phone_number"));
					userInfo.put("grade", phoneInfo.get("grade"));
					boolean res = AuthDao.createUser(userInfo);
					if (res) {
						//userInfo.put("session_key",wxresult.getString("session_key"));
						map.put("isbind", "1");
						map.put("phonenumber", userInfo.get("phone_number"));
						map.put("grade", userInfo.get("grade"));
						map.put("mail", "");
					} else {
						String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
						m_logger.error(String.format("OUTPUT event=%s ret_code=%s", ApiErrorCode.SYSTEM_ERROR));
						response.getWriter().write(ret);
						return;
					}
				}
			}else{
				map.put("isbind",StringUtils.isEmpty(userInfo.get("phone_number"))?"0":"1");
				if(!StringUtils.isEmpty(userInfo.get("phone_number"))){
					map.put("grade",userInfo.get("grade"));
					if(StringUtils.isNotEmpty(userInfo.get("mail"))){
						map.put("mail",userInfo.get("mail"));
					}else{
						map.put("mail","");
					}

					map.put("phonenumber",userInfo.get("phone_number"));
				}
			}
			map.put("ss",ss);
			//userInfo.put("session_key",wxresult.getString("session_key"));
			userInfo.put("ss",ss);
			DatabaseManager.addProfile(userInfo);

			String ret = ApiErrorCode.echoOkMap(map);
			response.getWriter().write(ret);
			m_logger.info(String.format("OUTPUT ret_code=%s",ret));
			return;
		} catch (CheckParameterException e) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR,e));
			response.getWriter().write(ret);
			return;
		}catch(Exception e){
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			m_logger.error(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.SYSTEM_ERROR,e),e);
			response.getWriter().write(ret);
			return;
		}
	}


	public static void main (String[] args) throws Exception {
//		String session_key = "zPxBB8PV8CUGQ+6Yqsk52A==";
//		String encrypteddata ="vzXFoYsn0/i1om2fpeBSOq9IqRELYlhX+z7gNKxytYlrxQD+ppn4YlbdFYOonpd/HzQAVYxcjitMdmVsbXkcSo0swOXkyj9Jyxhageqk4s37Yo28kUXgerLVl3nftCi+PV4Fi/5FICpnFbxNzUgqsBUunVCHOhYX6AZmv7OYg6XMtZBa0rTTBgwtkWLU5CL7K8BEL9LRTkDRzRz4B6ajug==";
//		String iv = "Pp5Qfy4mSILvZFmvmDRnEg==";
//		String decryptedData = AesCBC.getInstance().decrypt(encrypteddata,"utf8",session_key,iv);
//		System.out.println(decryptedData);
	}



}
