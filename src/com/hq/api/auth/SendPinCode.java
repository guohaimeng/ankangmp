/**
 * 发送验证码
 * POST
	phone（必填，string）：手机号，11位长，13、14、15、16、17、18，19开头
	type（必填，int） ： 1.绑定手机号  2.找回密码
 */
package com.hq.api.auth;


import com.hq.api.utils.*;
import com.hq.dao.auth.AuthDao;
import com.hq.dispatcher.Config;
import com.hq.dispatcher.ConfigKey;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 校验手机号并发送验证码
 * @author Administrator
 *
 */
@WebServlet("/auth/sendPinCode.do")
public class SendPinCode extends HttpServlet {
	private static final long serialVersionUID = -7414785058770121280L;
	private static final Logger m_logger = Logger.getLogger(SendPinCode.class);
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			//参数校验
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s",check.getParameterJSON()));
			// 1 参数校验
			check.addParameter("ss", CheckParameters.paraType.EXP, Config.getInstance().getString("params_ss"));
	    	check.addParameter("phone",CheckParameters.paraType.EXP,Config.getInstance().getString("params_mobile"));


			//session校验
			Map<String, String> selfInfo=DatabaseManager.checkSession(check.opt("ss").toString());
			if(selfInfo == null || selfInfo.size() == 0){
				String ret=ApiErrorCode.echoErr(ApiErrorCode.SESSION_INVALID);
				m_logger.info(String.format("OUTPUT  ret_code=%s",ApiErrorCode.SESSION_INVALID));
				response.getWriter().write(ret);
				return;
			}

			HashMap<String,String>	phoneInfo = AuthDao.getMobile(check.opt("phone").toString());
	    		if(phoneInfo.size()==0){
	    			String ret=ApiErrorCode.echoErr(ApiErrorCode.no_discern_mobile);
	        		m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.no_discern_mobile));
	        		response.getWriter().write(ret);
	        		return;
	    		}


	    	//发送手机pincode码
	    	int service_type = Integer.parseInt(ApiErrorCode.DEFAULT_TYPE);
	    	int pin_max_try_count = Config.getInstance().getInt(ConfigKey.PIN_TRY_COUNT);//GlobalSet.getIntValue("pin_try_count");
	    	int pin_max_try_timeout = Config.getInstance().getInt(ConfigKey.PIN_TIMEOUT);//GlobalSet.getIntValue("pin_timeout");
	    	HashMap<Integer,String> results = DatabaseManager.sendPincode(check.opt("phone").toString(),service_type,pin_max_try_count,pin_max_try_timeout);
	    	m_logger.debug(String.format("OUTPUT mobile,sendPincode=%s %s",check.opt("phone"),results.toString()));
	    	String ret_code = results.get(1);
	    	String ret="";
        	switch(ret_code){
	        	case "1":
	        		m_logger.info(String.format("SUCCESS params=%s,send pincode=%s",check.getParameterJSON(),results.get(2)));
	        		//调用发送短信接口
	        		String pincode = results.get(2);
	        		boolean flag = SMS.SendPinCode(check.opt("phone").toString(), pincode);
	        		if(flag){
		        		ret = ApiErrorCode.echoOk();		
		        		response.getWriter().write(ret);
		        		return;  
	        		}else{
	        			ret = ApiErrorCode.echoErr(ApiErrorCode.PINCODE_FAIL);		
		        		response.getWriter().write(ret);
		        		return;
	        		}
	        	case "-2": //当天超出次数
	        		m_logger.info(String.format("FAILED params=%s %s",check.getParameterJSON(),"send pincode overtop"));
	        		ret = ApiErrorCode.echoErr(ApiErrorCode.PINCODE_OUT_COUNTDAY);		
	        		response.getWriter().write(ret);
	        		return;    		
	        	case "-1": //过程异常及其错误
	        		m_logger.info(String.format("FAILED params=%s %s",ret_code,"send pincode error"));
	        		ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);		
	        		response.getWriter().write(ret);
	        		return;
	        	default:
					break;	
        	}
			//返回值
        	ret=ApiErrorCode.echoOk();
	        response.getWriter().write(ret);
			m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.SUCCESS));
	        return;
		}catch(CheckParameterException e){
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
    		m_logger.info(String.format("OUTPUT ret_code=%s %s",ApiErrorCode.PARAMETER_ERROR,e));
    		response.getWriter().write(ret);
    		return;
		}catch(Exception e){
    		String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
    		m_logger.info(String.format("OUTPUT ret_code=%s %s",ApiErrorCode.SYSTEM_ERROR,e));
    		response.getWriter().write(ret);
    		return;
    	}
		
	}
	

}
