/**
 * 绑定手机号码
 */
package com.hq.api.auth;


import com.hq.api.utils.ApiErrorCode;
import com.hq.api.utils.CheckParameterException;
import com.hq.api.utils.CheckParameters;
import com.hq.api.utils.DatabaseManager;
import com.hq.dao.auth.AuthDao;
import com.hq.dispatcher.Config;
import com.hq.dispatcher.ConfigKey;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 绑定手机号码
 * @author Administrator
 *
 */
@WebServlet("/auth/bindPhone.do")
public class BindPhone extends HttpServlet {

	private static final long serialVersionUID = 1553960794127572178L;
	private static final Logger m_logger = Logger.getLogger(BindPhone.class);
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			CheckParameters check = new CheckParameters(request.getParameterMap());
			m_logger.info(String.format("INPUT params=%s", check.getParameterJSON()));
			//参数校验
			check.addParameter("ss", CheckParameters.paraType.EXP,Config.getInstance().getString("params_ss"));
			if(!StringUtils.isEmpty(check.get("phone").toString())){
				check.addParameter("phone",CheckParameters.paraType.EXP,Config.getInstance().getString("params_mobile"));
			}else{
				m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.PHONE_IS_NOT_NULL));
				response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PHONE_IS_NOT_NULL));
				return;
			}
			if(!StringUtils.isEmpty(check.get("pincode").toString())){
				check.addParameter("pincode",CheckParameters.paraType.EXP,Config.getInstance().getString("parmas_code"));
			}else{
				m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.PINCODE_IS_NOT_NULL));
				response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PINCODE_IS_NOT_NULL));
				return;
			}


			//session校验
			String ss = check.opt("ss").toString();
			Map<String, String> u = DatabaseManager.checkSession(ss);
			if(null==u || u.size() == 0){
				String ret = ApiErrorCode.echoErr(ApiErrorCode.SESSION_INVALID);
	    		m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.SESSION_INVALID));
	    		response.getWriter().write(ret);
	    		return;
			}else{
				m_logger.debug(String.format("OUTPUT sessionUserInfo=%s",u.toString()));
			}
			

	    	//校验验证码是否正确
			int service_type = 1;
	    	int pin_max_try_count = Config.getInstance().getInt(ConfigKey.PIN_TRY_COUNT);//GlobalSet.getIntValue("pin_try_count");
	    	int pin_max_try_timeout = Config.getInstance().getInt(ConfigKey.PIN_TIMEOUT);//GlobalSet.getIntValue("pin_timeout");

			HashMap<String,String>	phoneInfo = AuthDao.getMobile(check.opt("phone").toString());
			if(phoneInfo.size()==0){
				String ret=ApiErrorCode.echoErr(ApiErrorCode.no_discern_mobile);
				m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.no_discern_mobile));
				response.getWriter().write(ret);
				return;
			}


			String flag = DatabaseManager.checkPincode(check.opt("phone").toString(), check.opt("pincode").toString(), service_type, pin_max_try_count, pin_max_try_timeout);
			m_logger.debug("bindPhone::flag"+flag);
			switch (flag) {
				case "-1": //存储错误
		    		 m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.SYSTEM_ERROR));
		    		 response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
					 return;
				case "-2": //验证码次数超出
		    		 m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.PINCODE_OUT_COUNT));
		    		 response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PINCODE_OUT_COUNT));
		    		 return;
				case "-3": //验证码不存在
		    		 m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.PINCODE_ISNOT_EXIST));
		    		 response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PINCODE_ISNOT_EXIST));
		    		 return;
				case "-4": //验证码不正确
		    		 m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.PINCODE_ERR));
		    		 response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PINCODE_ERR));
		    		 return;
				case "-5": //验证码超时
		    		 m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.PINCODE_TIME_OUT));
		    		 response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PINCODE_TIME_OUT));
		    		 return;
				default:
					break;
			}

			HashMap<String,String>	mailInfo =  AuthDao.getMailByMobile(check.opt("phone").toString());
			String mail="";
			if(mailInfo!=null  && mailInfo.size()>0){
				mail=mailInfo.get("mail");

			}
			AuthDao.bindPhone(check.opt("phone").toString(), u.get("uid"),phoneInfo.get("grade"),u.get("openid").toString());

	        	//重建用户redis
		    Map<String, String> userInfo = new HashMap<String, String>();
		    userInfo.put("phone_number", check.opt("phone").toString());
			userInfo.put("grade",phoneInfo.get("grade"));
			userInfo.put("mail",mail);
		    DatabaseManager.updateProfile(u.get("uid"), userInfo);
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("grade",phoneInfo.get("grade"));
			map.put("mail",mail);
			//返回值	       	
	        String ret = ApiErrorCode.echoOkMap(map);
	        response.getWriter().write(ret);
			m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.SUCCESS));
	        return;
		}catch (CheckParameterException e) {
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR,e));
			response.getWriter().write(ret);
			return;
		}catch(Exception e){
			String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
			m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.SYSTEM_ERROR,e));
			response.getWriter().write(ret);
			return;
		}
		
		
	}
}
