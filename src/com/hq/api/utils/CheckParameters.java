package com.hq.api.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import org.json.JSONArray;


public class CheckParameters {
	
	

	public enum paraType{
		LONG,INT,STRING,DATETIME,DOUBLE,FLOAT,EXP,JSONARR,JSONOBJ;
	}
	
	/**
	 * 校验后的参数列表
	 */
	private HashMap<String,String> parameterMap = new HashMap<String,String>();
	
	/**
	 * 校验后的参数列表
	 */	
	private JSONObject parameterJSON = new JSONObject();
//	private JSONArray jsonArray;
	
	
	/**
	 * 参数校验规则
	 * 1 调用构造函数遍历把所有的参数输出到JSON对象中
	 * 2 通过设置每个参数校验规则，对JSON中每个数据进行校验
	 * @param postParameters
	 * @throws JSONException
	 */
	public CheckParameters(Map<String,String[]> postParameters){
		Set<String> paramKeys =postParameters.keySet();		
		for(String key:paramKeys){
			String values=getParameterValues(postParameters.get(key));
			try {
				parameterJSON.put(key, values.trim());
				parameterMap.put(key, values.trim());
			} catch (JSONException e) {
			}
		}
	};
	
	public CheckParameters(HashMap<String,String> postParameters){
		Set<String> paramKeys =postParameters.keySet();		
		for(String key:paramKeys){
			try {
				parameterJSON.put(key, postParameters.get(key));
				parameterMap.put(key, postParameters.get(key));
			} catch (JSONException e) {
			}
		}
	};
	
	
	//字符串数组拼接
	private String getParameterValues(String[] values){
		StringBuffer sb = new StringBuffer();		
		int offset = values.length;
		for( int i = 0; i < offset; i++ )
		{
		    sb.append(values[i]);
		}				 
		return sb.toString();
	}
	
	
	/**
	 *校验参数是否存在 
	 * @param paramName	 参数名称
	 * 
	 */
	public void addParameter(String... name) throws CheckParameterException {
		for(String tmp : name)
		if(!parameterJSON.has(tmp)){
			throw new CheckParameterException(tmp, CheckParameterException.errLostPara);
		}
	}

	/**
	 * 该方法对参数的类型进行校验
	 * @param name 参数名称
	 * @param ptype	 参数类型
	 */
	public void addParameter(String name,paraType ptype) throws CheckParameterException {
		if(parameterJSON.has(name)){
			String value="";
			try {
				value = parameterJSON.getString(name);
				switch(ptype){
				case LONG:
					parameterJSON.put(name,Long.parseLong(value));
					break;
				case INT:
					parameterJSON.put(name,Integer.parseInt(value));
					break;
				case DOUBLE:
					parameterJSON.put(name,Double.parseDouble(value));
					break;
				case FLOAT:
					parameterJSON.put(name,Float.parseFloat(value));
					break;
				case DATETIME:
					SimpleDateFormat strFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date datetime=new Date();
					datetime=strFormat.parse(value);
					parameterJSON.put(name,datetime);					
					break;
				case JSONARR:
//					JSONArray JsonArr = new JSONArray(value);
//					int arrayLen = JsonArr.length();
					break;
				default:			
					parameterJSON.put(name,value);
				}
				
			} catch (JSONException e) {				
				throw new CheckParameterException(name,value, CheckParameterException.errParameter);
			} catch(NumberFormatException nfe){
				throw new CheckParameterException(name,value, CheckParameterException.errDataType);
			}catch (ParseException pe){
				throw new CheckParameterException(name,value, CheckParameterException.errDateTime);
			}
		}else{
			throw new CheckParameterException(name, CheckParameterException.errLostPara);
		}			
	}
	
	/**
	 * @param name 参数名称
	 * @param ptype	 参数类型
	 * @param startScope	 参数开始范围（只针对ptype=LONG,INT,DOUBLIE）
	 * @param stopScope	 参数结束范围
	 */
	public void addParameter(String name,paraType ptype,Object startScope,Object stopScope) throws CheckParameterException {
		if(parameterJSON.has(name)){
			String value="";
			Object realValue=new Object();
			try {
				value = parameterJSON.getString(name);
				switch(ptype){
				case LONG:
					realValue=Long.parseLong(value);
					parameterJSON.put(name,realValue);						
					break;
				case INT:
					realValue=Integer.parseInt(value);
					parameterJSON.put(name,realValue);
					break;
				case DOUBLE:
					realValue=Double.parseDouble(value);
					parameterJSON.put(name,realValue);
					break;
				case FLOAT:
					realValue=Float.parseFloat(value);
					parameterJSON.put(name,realValue);
					break;
				case DATETIME:					
					SimpleDateFormat strFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					realValue=strFormat.parse(value);
					parameterJSON.put(name,realValue);				
					break;
				default:			
					realValue = value;
					parameterJSON.put(name,realValue);
				}
				isDefined(name,realValue,startScope,stopScope);

			} catch (JSONException e) {				
				throw new CheckParameterException(name,value, CheckParameterException.errParameter);
			} catch(NumberFormatException nfe){
				throw new CheckParameterException(name,value, CheckParameterException.errDataType);
			}catch (ParseException pe){
				throw new CheckParameterException(name,value, CheckParameterException.errDateTime);
			}	
		}else{
			throw new CheckParameterException(name, CheckParameterException.errLostPara);
		}
	}
	
	public void addParameter(String name,paraType ptype,String[] scope) throws CheckParameterException {
		if(parameterJSON.has(name)){
			String value = parameterJSON.optString(name);
			try {
				switch(ptype){
				case LONG:
					parameterJSON.put(name,Long.parseLong(value));						
					break;
				case INT:
					parameterJSON.put(name,Integer.parseInt(value));
					break;
				case DOUBLE:
					parameterJSON.put(name,Double.parseDouble(value));
					break;
				case FLOAT:
					parameterJSON.put(name,Float.parseFloat(value));
					break;
				case DATETIME:					
					SimpleDateFormat strFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date datetime=new Date();
					datetime=strFormat.parse(value);
					parameterJSON.put(name,datetime);					
					break;
				default:			
					parameterJSON.put(name,value);
				}
				isDefined(name,value,scope);

			} catch (JSONException e) {				
				throw new CheckParameterException(name,value, CheckParameterException.errParameter);
			} catch(NumberFormatException nfe){
				throw new CheckParameterException(name,value, CheckParameterException.errDataType);
			}catch (ParseException pe){
				throw new CheckParameterException(name,value, CheckParameterException.errDateTime);
			}	
		}else{
			throw new CheckParameterException(name, CheckParameterException.errLostPara);
		}
	}
	public void addParameter(String name,paraType ptype,String exp) throws CheckParameterException {
		if(parameterJSON.has(name)){
			String value = parameterJSON.optString(name);
			switch(ptype){
			case EXP:
				Pattern p = Pattern.compile(exp);
				Matcher m = p.matcher(value);
				if(!m.matches())
					throw new CheckParameterException(name,value, CheckParameterException.errFormat);
				break;
			default:			
				throw new CheckParameterException(name,value, CheckParameterException.errFormat);
			}
		}
		else{
			throw new CheckParameterException(name,"", CheckParameterException.errLostPara);
		}
	}
	
	/**
	 * 判断某个值是否在自定义的范围内
	 * @param value
	 * @param scope
	 */
	private static void isDefined(String name,String value,String[] scope) throws CheckParameterException {
		for(int i=0;i<scope.length;i++){
			String dst = scope[i];
			if(dst.equals(value)){
				return;
			}			
		}
		throw new CheckParameterException(name,value, CheckParameterException.errScope);
	} 
	
	private static void isDefined(String name,Object value,Object startScope,Object stopScope) throws CheckParameterException {
		try{
			if(value instanceof String){
				if(((String) value).length()>=(int)startScope && ((String)value).length()<=(int)stopScope){
					return;
				}
			}	
			else if(value instanceof Long){
				if((long)value>=Long.parseLong(startScope.toString()) && (long)value<=Long.parseLong(stopScope.toString())){
					return;
				}

			}else if(value instanceof Integer){
				if((int)value>=Long.parseLong(startScope.toString()) && (int)value<=Long.parseLong(stopScope.toString())){
					return;
				}
			}		
			else if(value instanceof Double){
				if((double)value>=Double.parseDouble(startScope.toString()) && (double)value<=Double.parseDouble(stopScope.toString())){
					return;
				}
			}	
			else if(value instanceof Float){
				if((float)value>=Double.parseDouble(startScope.toString()) && (float)value<=Double.parseDouble(stopScope.toString())){
					return;
				}
			}else{
				throw new CheckParameterException(name,value, CheckParameterException.errFormat);
			}
		}catch(Exception e){
			throw new CheckParameterException(name,value, CheckParameterException.errFormat);
		}
		throw new CheckParameterException(name,value, CheckParameterException.errScope);
	} 
	
	public HashMap<String,String> getParameterMap(){
		return parameterMap;
	}
	
	public JSONObject getParameterJSON(){
		return parameterJSON;
	}
	
	
	public Object get(String name) throws CheckParameterException {
		try {
			return parameterJSON.get(name);
		} catch (JSONException e) {
			throw new CheckParameterException(name, CheckParameterException.errLostPara);
		}
	}
	
	/**
	 * 不抛出异常返回
	 * @param name
	 * @return
	 */
	public Object opt(String name){
		return parameterJSON.opt(name);
	}
	
	public void put(String key,String value){
		try {
			parameterJSON.put(key, value);
			parameterMap.put(key, value);
		} catch (JSONException e) {
		}
	}

}
