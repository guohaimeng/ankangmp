package com.hq.api.utils;


import com.hq.dispatcher.Config;
import com.hq.dispatcher.ConfigKey;
import org.apache.log4j.Logger;


public class SMS {
	private static final Logger m_logger = Logger.getLogger(SMS.class);
	private static String params = null;
	private static String api;
	private static void init(String mobile,String content){
		params = "method=Submit&account={account}&password={password}&mobile={mobile}&content={content}";
		api= Config.getInstance().getString(ConfigKey.SMS_API);
		String acc= Config.getInstance().getString(ConfigKey.SMS_USER);
		String pwd= Config.getInstance().getString(ConfigKey.SMS_PASS);
		params=params.replace("{account}", acc).replace("{password}", pwd).replace("{mobile}", mobile).replace("{content}", content);
	}
	public static void Send(String mobile,String content) throws Exception{
		init(mobile,content);
		HttpRequest.get(api, params);
	}
	public static boolean SendPinCode(String mobile,String pincode) throws Exception{
		String tpl = Config.getInstance().getString(ConfigKey.SMS_TPL);
		tpl = tpl.replace("{pincode}", pincode);
		init(mobile,tpl);
		String result = HttpRequest.get(api, params);
		m_logger.info("sind pincode result:"+result);
		if(result.contains("<code>2</code>")){
			return true;
		}else{
			return false;
		}
	}
}