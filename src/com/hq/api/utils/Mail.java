package com.hq.api.utils;


import com.hq.dispatcher.Config;
import com.hq.dispatcher.ConfigKey;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

public class Mail {
	static MimeMessage msg;
	private static final String sendUser="超级试卷王";
	public static void send(String mailaddr,String subject,String content,String mailUser) throws MessagingException, UnsupportedEncodingException{
		init(sendUser,mailUser);
		
	    InternetAddress[] address = {new InternetAddress(mailaddr)};
	    msg.setRecipients(Message.RecipientType.TO, address);
	    msg.setSubject(MimeUtility.encodeText(subject,"UTF-8","B"));
	    msg.setSentDate(new Date());
	    msg.setContent(content, "text/html;charset=utf8");
	    //msg.setHeader("X-Mailer", "smtpsend");
	    //msg.setText(content);
	    Transport.send(msg);
	}
	private static void init(String nick,String mailUser) throws AddressException, MessagingException{
		// create some properties and get the default Session
		Properties props = new Properties();
		String host=Config.getInstance().getString(ConfigKey.MAIL_SMTP);
		String user=mailUser;
		String pass=Config.getInstance().getString(ConfigKey.MAIL_RES_PASS);
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.auth", "true"); //这样才能通过验证

	    MyAuthenticator myauth = new MyAuthenticator(user, pass);
	    Session session = Session.getInstance(props, myauth);
		 // create a message
	    msg = new MimeMessage(session);
		try {
			nick=MimeUtility.encodeText(nick,"UTF-8","B");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	    msg.setFrom(new InternetAddress(nick+"<"+user+">"));
	}
}

class MyAuthenticator extends javax.mail.Authenticator {
	private String strUser;
	private String strPwd;
	public MyAuthenticator(String user, String password) {
			this.strUser = user;
			this.strPwd = password;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(strUser, strPwd);
	}
}