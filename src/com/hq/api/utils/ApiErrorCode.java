package com.hq.api.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Set;

/**
 * API接口返回给client端的错误代码
 *
 */
public class ApiErrorCode {
	
	/**
	 * 当前返回码
	 */
	private int m_code = 0;

	/**
	 * 描述字符
	 */
	private String m_description = null;

	/**
	 * 返回码+描述字符
	 */
	private String m_string = null;

	public ApiErrorCode(){}
	public ApiErrorCode(int code, String description) {
		m_code = code;
		m_description = description;
		m_string = m_code + " " + m_description;
	}

	public String toString() {
		return m_string;
	}
	
	public int errCode(){
		return m_code;
	}
	
	public String errMsg(){
		return m_description;
	}	
	/**
	 * 默认错误码
	 */
	public static final int DEFAULT_SYSTEM_ERROR = 6001;
	public static final int DEFAULT_SDK_SUCCESS = 0;
	public static final int DEFAULT_SDK_PARAMS_ERROR = 9999;
	public static final int DEFAULT_SDK_SYSTEM_ERROR = 9998;
	public static final int DEFAULT_SDK_ILLEGAL_USER = 1006; //无效的用户
	public static final String DEFAULT_STATUS = "0";
	public static final String DEFAULT_TYPE = "1";

	public static final String GRADE_SCHOOL                 = "1";//小学
	public static final String MIDDLE_SCHOOL                = "2";//初中
	public static final String HIGH_SCHOOL                  = "3";//高中
	public static final String CHILD_EDUCATION              = "4";//幼教
	public static final String SECONDARY_SCHOOL             = "5";//中职
	
	/********************消息提示类型说明Start****************************************/



	
	
	/**
	 * 系统级错误码
	 */
	
	public static final ApiErrorCode SUCCESS           = new ApiErrorCode(0, "SUCCESS");
	public static final ApiErrorCode VOID_REQUEST      = new ApiErrorCode(6000, "无效请求");
	public static final ApiErrorCode SYSTEM_ERROR      = new ApiErrorCode(6001, "系统错误");
	public static final ApiErrorCode PARAMETER_ERROR   = new ApiErrorCode(6002, "参数错误");
	public static final ApiErrorCode SESSION_INVALID   = new ApiErrorCode(6003, "session非法");
	public static final ApiErrorCode no_mobile    		= new ApiErrorCode(6004, "未绑定手机号");
	public static final ApiErrorCode no_discern_mobile  = new ApiErrorCode(6005, "手机号不支持本业务");
	public static final ApiErrorCode PHONE_IS_NOT_NULL  = new ApiErrorCode(6006, "手机号不能为空");
	public static final ApiErrorCode PINCODE_OUT_COUNTDAY  = new ApiErrorCode(6100, "验证码今日次数已超限");
	public static final ApiErrorCode PINCODE_FAIL          = new ApiErrorCode(6101, "服务器繁忙，请稍后再试");
	public static final ApiErrorCode PINCODE_OUT_COUNT     = new ApiErrorCode(6102, "验证码已失效，请重新获取");
	public static final ApiErrorCode PINCODE_ERR  		   = new ApiErrorCode(6112, "验证码错误");
	public static final ApiErrorCode PINCODE_ISNOT_EXIST   = new ApiErrorCode(6125, "验证码不存在");
	public static final ApiErrorCode PINCODE_TIME_OUT      = new ApiErrorCode(6126, "验证码超时");
	public static final ApiErrorCode PINCODE_IS_NOT_NULL      = new ApiErrorCode(6127, "验证码不能为空");
	public static final ApiErrorCode MAIL_SEND_COUNT_OVERTOP      = new ApiErrorCode(6128, "今日邮件发送次数已经达到上限（每日10次），请明天再试");
	public static final ApiErrorCode MAIL_SEND_MIN_OVERTOP      = new ApiErrorCode(6129, "1分钟内只能发送一封邮件，请稍后再试");

	/**
	 * 资源
	 */

	public static final ApiErrorCode RES_NOT_EXIST  			= new ApiErrorCode(7500, "该资源不存在");

	private static String retJson(ApiErrorCode errInfo){
		JSONObject retJson = new JSONObject();
		try {
			retJson.put("errcode", errInfo.errCode());
			retJson.put("errmsg", errInfo.errMsg());
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}
	}
	
	public static String echoErr(ApiErrorCode errCode){
		return retJson(errCode);			
	}
	public static String echoOk(){
		return retJson(SUCCESS);	
	}
	
	public static String echoOkArr(HashMap<String,String> map){
		JSONObject retJson = new JSONObject();
		try {
			retJson.put("errcode", SUCCESS.errCode());
			retJson.put("errmsg", SUCCESS.errMsg());
			Set<String> keys = map.keySet();				
			for(String key:keys){
				retJson.put(key,map.get(key));
			}			
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}	
	}
	
	public static String echoErrArr(ApiErrorCode errCode, HashMap<String,Object> map){
		JSONObject retJson = new JSONObject();
		try {
			retJson.put("errcode", errCode.errCode());
			retJson.put("errmsg", errCode.errMsg());
			Set<String> keys = map.keySet();				
			for(String key:keys){
				retJson.put(key,map.get(key));
			}			
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}	
	}
	
	public static String echoErrMap(ApiErrorCode errCode, HashMap<String,Object> map){
		JSONObject retJson = new JSONObject();
		try {
			retJson.put("errcode", errCode.errCode());
			retJson.put("errmsg", errCode.errMsg());
			Set<String> keys = map.keySet();				
			for(String key:keys){
				if(map.get(key) instanceof JSONArray){
					retJson.put(key,map.get(key));
				}else if(map.get(key) instanceof JSONObject){
					retJson.put(key,map.get(key));
				}else{
					retJson.put(key,map.get(key).toString());
				}
					
			}			
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}
		
	}
	
	public static String echoOkMap(HashMap<String,Object> map){
		JSONObject retJson = new JSONObject();
		try {
			retJson.put("errcode", SUCCESS.errCode());
			retJson.put("errmsg", SUCCESS.errMsg());
			Set<String> keys = map.keySet();				
			for(String key:keys){
				if(map.get(key) instanceof JSONArray){
					retJson.put(key,map.get(key));
				}else if(map.get(key) instanceof JSONObject){
					retJson.put(key,map.get(key));
				}else{
					retJson.put(key,map.get(key).toString());
				}
					
			}			
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}
	}
	/**
	 * 班级模块输出
	 * @return
	 */
	public static String echoClassOk(HashMap<String,Object> map,String outType){
		JSONObject retJson = new JSONObject();
		JSONObject ret = new JSONObject();
		try {
			retJson.put("errcode", SUCCESS.errCode());
			retJson.put("errmsg", SUCCESS.errMsg());
			Set<String> keys = map.keySet();				
			for(String key:keys){
				if(map.get(key) instanceof JSONArray){
					ret.put(key,map.get(key));
				}else if(map.get(key) instanceof JSONObject){
					ret.put(key,map.get(key));
				}else{
					ret.put(key,map.get(key).toString());
				}
			}
			retJson.put(outType,ret);
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}
	}
	public static String echoClassOk(JSONObject ret,String outType){
		return retClassJson(SUCCESS,ret,outType);	
	}
	private static String retClassJson(ApiErrorCode errInfo, JSONObject ret, String outType){
		JSONObject retJson = new JSONObject();
		try {
			retJson.put("errcode", errInfo.errCode());
			retJson.put("errmsg", errInfo.errMsg());
			/*if(ret != null){
				retJson.put("result", ret);
			}else{
				retJson.put("result", "{}");
			}*/
			if(ret != null){
				retJson.put(outType,ret);
			}else{
				retJson.put(outType, "{}");
			}
			
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}
	}
	public static String echoClassOk(JSONObject ret){
		return retClassJson(SUCCESS,ret);	
	}
	private static String retClassJson(ApiErrorCode errInfo, JSONObject ret){
		try {
			ret.put("errcode", errInfo.errCode());
			ret.put("errmsg", errInfo.errMsg());
			return ret.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}
	}
	public static String echoClassMsg(HashMap<String,Object> map){
		JSONObject retJson = new JSONObject();
		try {
			if(map != null){
				Set<String> keys = map.keySet();				
				for(String key:keys){
					retJson.put(key,map.get(key));
				}
			}
			return retJson.toString();
		} catch (JSONException e) {
			return "{\"errcode\":\"6001\",\"errmsg\":\"JSON解析错误\"}";			
		}	
	}
	
}
