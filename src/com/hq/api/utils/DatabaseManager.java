package com.hq.api.utils;


import com.hq.utils.MysqlBaseUtil;
import com.hq.utils.RedisBaseUtil;
import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 消息端口处理
 * 
 */

public class DatabaseManager {
	public final static String REDIS_CITY_KEY="city.codes";

	private static final Logger m_logger = Logger.getLogger(DatabaseManager.class);



	/**
	 *  session校验
	 *
	 * @param session
	 *            需要返回对应的fields值
	 * @return
	 */
	public static Map<String, String> checkSession(String session) throws Exception {
		Jedis redis = null;
		Map<String, String> suidInfo = null;
		String suid = null;
		try {
			redis = RedisBaseUtil.getBaseRedis().getResource();
			suid = redis.get(session);
			if (suid != null) {
				suidInfo = redis.hgetAll(suid + ".inf");
				if (suidInfo == null || suidInfo.size() == 0) {
					redisdelKey(new String[]{session});
				}
			}

		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
				redis=null;
			}
			m_logger.info(" connect exception:" + e);
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
		return suidInfo;
	}



	/**
	 *  获取session
	 *
	 * @param session
	 *            需要返回对应的fields值
	 * @return
	 */
	public static Map<String, String> getSession(HttpServletRequest request) throws Exception {
		return (Map<String, String>)request.getSession().getAttribute("selfInfo");
	}





	/**
	 * 删除redis数据
	 * @param keys
	 * @return
	 * @throws Exception
     */
	public static Long redisdelKey(String... keys)  {
		Jedis redis = null;
		Long res = 0l;
		try {
			redis = RedisBaseUtil.getBaseRedis().getResource();
			res = redis.del(keys);

		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
				redis=null;
			}
			m_logger.info(" redisdelKey exception:" + e);
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
		return res;
	}


	/**
	 * 删除redis数据
	 * @param keys
	 * @return
	 * @throws Exception
	 */
	public static Long redisdelById(String... keys)  {
		Jedis redis = null;
		Long res = 0l;
		try {
			redis = RedisBaseUtil.getBaseRedis().getResource();
			res = redis.del(keys);

		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
				redis=null;
			}
			m_logger.info(" connect exception:" + e);
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
		return res;
	}




	/**
	 * 通过用户uid获取用户缓存信息
	 *
	 * @param suid
	 * @return
	 */
	public static Map<String, String> getUserInfoByUidfromRedis(String suid) throws Exception {
		Jedis redis = null;
		Map<String, String> suidInfo = null;
		try {
			redis = RedisBaseUtil.getBaseRedis().getResource();
			suidInfo = redis.hgetAll(suid + ".inf");
		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
			}
			m_logger.info(" connect exception:" + e);
			// throw e;
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
		return suidInfo;
	}







	/**
	 * 更新用户信息到redis
	 *
	 * @param suid
	 * @param userInfo
	 * @throws Exception
	 */
	public static void updateProfile(String suid, Map<String, String> userInfo) throws Exception {
		Jedis redis = null;
		Map<String, String> filterUserInfo = new HashMap<String, String>();
		try {
			for (String key : userInfo.keySet()) {
				if (userInfo.get(key) != null) {
					filterUserInfo.put(key, userInfo.get(key));
				}
			}
			redis = RedisBaseUtil.getBaseRedis().getResource();
			redis.hmset(suid + ".inf", filterUserInfo);

		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
			}
			m_logger.info(" connect exception:" + e);
			// throw e;
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
	}

	/**
	 * 更新用户信息到redis
	 *
	 * @param suid
	 * @throws Exception
	 */
	public static void updateProfile(String suid) throws Exception {
		Jedis redis = null;
		try {
			redis = RedisBaseUtil.getBaseRedis().getResource();
			redis.del(suid + ".inf");
		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
			}
			m_logger.info(" connect exception:" + e);
			// throw e;
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
	}









	/**
	 * session校验
	 *
	 *            需要返回对应的fields值
	 * @return
	 */
	public static boolean addProfile(Map<String, String> userInfo) throws Exception {
		Jedis redis = null;
		boolean isSuccess = false;
		try {
			// 过滤value为null的项
			Map<String, String> filterUserInfo = new HashMap<String, String>();
			for (String key : userInfo.keySet()) {
				if (userInfo.get(key) != null) {
					filterUserInfo.put(key, userInfo.get(key));
				}
			}

			String session = userInfo.get("ss");
			String suid = userInfo.get("uid");
			redis = RedisBaseUtil.getBaseRedis().getResource();
			Pipeline pipeline = redis.pipelined();
			pipeline.hget(suid + ".inf", "ss");
			// 写入newsession与suid的映射关系
			pipeline.set(session, suid);
			pipeline.del(suid + ".inf");
			pipeline.hmset(suid + ".inf", filterUserInfo);

			List<Object> results = pipeline.syncAndReturnAll();
			// 删除旧的session和uid映射关系
			if (results != null && results.size() > 0) {
				if (results.get(0) != null) {
					String oldSession = results.get(0).toString();
					if (!"".equals(oldSession)) {
						redis.del(oldSession);
					}
				}
			}
			isSuccess = true;
		} catch (Exception e) {
			if (redis != null) {
				RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
			}
			m_logger.info("redis connect exception:" + e.getMessage());
		} finally {
			RedisBaseUtil.getBaseRedis().returnResource(redis);
		}
		return isSuccess;
	}







	/**
	 * 获取短信验证码
	 * @param mobile   手机号码
	 * @param service_type 请求类型
	 * @param max_try_count 最大请求次数
	 * @param try_timeout   超时时长
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<Integer,String> sendPincode(String mobile,int service_type,int max_try_count,int try_timeout) throws SQLException{
//		String sql = "{CALL p_production_pincode(?,?,?,?,?,?)}";
		String sql = "{CALL p_production_pincode(?,?,?,?,?,?)}";
		String service_type_str = Integer.toString(service_type);
		String max_try_count_str = Integer.toString(max_try_count);
		String try_timeout_str = Integer.toString(try_timeout);
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(service_type_str);
		params.add(mobile);
		params.add(max_try_count_str);
		params.add(try_timeout_str);
		try {
			HashMap<Integer,String> out = new HashMap<Integer,String>();
			MysqlBaseUtil.executeStoreProcedure(sql,params, 2, out);
			return out;
		} catch (SQLException e) {
			m_logger.debug("sendPincode fail");
			throw e;
		}
	}

	/**
	 * 验证手机的PIN码有效性
	 *
	 * @param mobile
	 *            手机号码
	 * @param pin
	 *            手机接收到的PIN
	 * @param service_type
	 *            pincode验证类型
	 * @param max_try_count
	 *            每个pincode记录允许验证的次数
	 * @param try_timeout
	 *            验证码有效时间(分钟)
	 * @return
	 */
	public static String checkPincode(String mobile,String pin,int service_type,int max_try_count,int try_timeout) throws SQLException{
		String sql = "{CALL p_mobile_sms_validate(?,?,?,?,?,?)}";
		String service_type_str = Integer.toString(service_type);
		String max_try_count_str = Integer.toString(max_try_count);
		String try_timeout_str = Integer.toString(try_timeout);
		//String [] params = new String[]{service_type_str,mobile,pin,max_try_count_str,try_timeout_str};
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(service_type_str);
		params.add(mobile);
		params.add(pin);
		params.add(max_try_count_str);
		params.add(try_timeout_str);
		try {
			HashMap<Integer, String> out = new HashMap<Integer, String>();
			MysqlBaseUtil.executeStoreProcedure(sql, params, 1, out);
			return out.get(1);
		} catch (SQLException e) {
			m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SYSTEM_ERROR, e));
			throw e;
		}
	}



}