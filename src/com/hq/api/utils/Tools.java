package com.hq.api.utils;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tools {

	public static String md5(String stext){
		MessageDigest md=null; 
		StringBuffer sb = null;
		try {
			md=MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			return null;
		} 
		md.update(stext.getBytes()); //MD5加密算法只是对字符数组而不是字符串进行加密计算，得到要加密的对象 
		byte[] bs=md.digest();   //进行加密运算并返回字符数组 
		sb=new StringBuffer(); 
		for(int i=0;i<bs.length;i++){    //字节数组转换成十六进制字符串，形成最终的密文 
			int v=bs[i]&0xff; 
			if(v<16){ 
				sb.append(0); 
			} 
			sb.append(Integer.toHexString(v)); 
		} 
		
		return sb.toString();
	}
	
	private static final String allChar = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static String getRandomStr(int length)
	{
		StringBuffer sb = new StringBuffer(); 
		Random random = new Random(); 
		for (int i = 0; i < length; i++) {
			sb.append(allChar.charAt(random.nextInt(allChar.length()))); 
		} 
		return sb.toString(); 
	}
	
	public static ByteArrayOutputStream InputStreamCopy(InputStream inStream) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		byte[] buffer = new byte[1024];  
		int len;  
		while ((len = inStream.read(buffer)) > -1 ) {  
			baos.write(buffer, 0, len);  
		}  
		baos.flush();
		return baos;
		//return new ByteArrayInputStream(baos.toByteArray());
	}
	
	
	public static String hashMapToJson(HashMap<String,String> map) {  
	    String string = "{";  
	    for (Iterator<Entry<String, String>> it = map.entrySet().iterator(); it.hasNext();) {  
	        Entry<String, String> e = it.next();  
	        string += "'" + e.getKey() + "':";  
	        string += "'" + e.getValue() + "',";  
	    }  
	    string = string.substring(0, string.lastIndexOf(","));  
	    string += "}";  
	    return string;  
	} 
	
	 /**
		 *@function 判断Session是否符合要求
		 *@param: string str 校验的字符串
		 *@boolean:
	 **/
	 public static boolean checkSession(String accountName){
		 if(accountName.equals("")||accountName==null){
				return false;
			}
			String exp = "(^[0-9a-zA-Z]*$)";
			if(Pattern.matches(exp, accountName) && accountName.length()>0 && accountName.length()<=100){
				return true;
			}
			return false;
	 }
	
	/**
	 * 
	 * 判断用户名 的格式，用户名是由字母开头的6-20位数字或字母组成，不允许除了"_","-"以外的特殊字符存在
	 * 
	 * */
	public static boolean checkAccount(String str){
		int PWD_MIN_LENGTH=6;
		int PWD_MAX_LENGTH=50;
		if(null == str || str.isEmpty()){
			return false;
		}
		
		if(str.length()<PWD_MIN_LENGTH || str.length()>PWD_MAX_LENGTH){
			return false;
		}
		/*可以是英文字母/数字/下划线/横线/英文点组合，邮箱地址*/
		String exp = "[.@A-Za-z0-9_-]{5,50}$";//"^[A-Za-z][A-Za-z0-9_-]{5,49}$";
		Pattern p = Pattern.compile(exp);
		Matcher m = p.matcher(str);
		return m.matches();
	}	    

	/*密码格式匹配:长度6-20,字母，数字，键盘上允许的特殊字符，半角，非中文*/
	public static boolean checkPwdFormat(String str){
		int PWD_MIN_LENGTH=6;
		int PWD_MAX_LENGTH=50;
		if(null == str || str.isEmpty()){
			return false;
		}
		
		if(str.length()<PWD_MIN_LENGTH || str.length()>PWD_MAX_LENGTH){
			return false;
		}
		
		String exp = "[(?=[\\x21-\\x7e]+)]{5,50}";
		Pattern p = Pattern.compile(exp);
		Matcher m = p.matcher(str);
		return m.matches();
	}
	public static boolean isNumeric(String str){ 
		   Pattern pattern = Pattern.compile("[0-9]*"); 
		   Matcher isNum = pattern.matcher(str);
		   if( !isNum.matches() ){
		       return false; 
		   } 
		   return true; 
	}

	
	/**
	 * 比较版本号的大小,前者大则返回一个正数,后者大返回一个负数,相等则返回0
	 * @param version1
	 * @param version2
	 * @return
	 */
	public static int compareVersion(String version1, String version2){
		if (version1 == null || version2 == null) {
			return -1;
		}
		String[] versionArray1 = version1.split("\\.");//注意此处为正则匹配，不能用"."；
		String[] versionArray2 = version2.split("\\.");
		int idx = 0;
		int minLength = Math.min(versionArray1.length, versionArray2.length);//取最小长度值
		int diff = 0;
		while (idx < minLength
				&& (diff = versionArray1[idx].length() - versionArray2[idx].length()) == 0//先比较长度
				&& (diff = versionArray1[idx].compareTo(versionArray2[idx])) == 0) {//再比较字符
			++idx;
		}
		//如果已经分出大小，则直接返回，如果未分出大小，则再比较位数，有子版本的为大；
		diff = (diff != 0) ? diff : versionArray1.length - versionArray2.length;
		return diff;
	}
	

	
	/**
	 * 将list<String>转换成以，分割字符串
	 * @param list
	 * @return
	 * @throws Exception
	 */
	public static String listToString(List<String> list) throws Exception{
		String convertString="";
		for (int i = 0; i < list.size(); i++) {
			convertString += list.get(i) + ",";
		}
		if(convertString!=""){
			convertString = convertString.substring(0, convertString.length() - 1);
		}
		return convertString;
	}
	
	
	
}
