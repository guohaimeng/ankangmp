package com.hq.api.subapps.oldexam;


import com.hq.api.subapps.common.ConstanQuantity;
import com.hq.api.utils.ApiErrorCode;
import com.hq.api.utils.CheckParameterException;
import com.hq.api.utils.DatabaseManager;
import com.hq.dispatcher.Config;
import com.hq.utils.DoResponseUtil;
import com.hq.utils.RedisBaseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import redis.clients.jedis.Jedis;
import com.hq.api.utils.CheckParameters;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * 获取真题资源
 */
@WebServlet("/subapps/getPastExam.do")
public class GetPastExams extends HttpServlet {
    private static final String REDIS_PASTEXAM_KEY = "PastExam.paper";
    private static final Logger m_logger = Logger.getLogger(GetPastExams.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            // session校验
            Map<String, String> sessionInfo = DatabaseManager.getSession(request);

            String  obj = getPastExam();
            String json = ApiErrorCode.echoClassOk(new JSONObject(obj));
            // 数据压缩
            DoResponseUtil.ResponseWrite(request, response, json);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            return;

        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR, ce));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("getPrepareLessons Exception=%s", e.getMessage()), e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            return;
        }
    }

    /**
     * 构造数据
     *
     * @return
     * @throws Exception
     */
    public static String constructPastExam() throws Exception {
        JSONArray label2 = new JSONArray();// 中学库标签
        JSONArray arr2 = new JSONArray();// 中学库
//        JSONArray label1 = new JSONArray();// 小学库标签
//        JSONArray arr1 = new JSONArray();// 小学库
//        JSONArray def1 = new JSONArray();// 小学默认
//        JSONArray def2 = new JSONArray();// 小学默认
		/*-------------------------------------------------------------------------------------*/
        // 标签
        // 中学标签
        String[] labelName2 = {  "地区", "年份", "学科" };
        String[] labelTid2 = {  "area", "year", "subject" };
//        // 小学标签
//        String[] labelName1 = { "真题类型", "学科" };
//        String[] labelTid1 = { "type", "subject" };
        // 中学
        for (int i = 0; i < labelName2.length; i++) {
            HashMap<String, Object> relation = new HashMap<>();
            relation.put("id", i + 1001);
            relation.put("name", labelName2[i]);
            relation.put("pid", "");
            relation.put("tid", labelTid2[i]);
            relation.put("sid", i + 1);
            label2.put(relation);
        }
//        // 小学
//        for (int i = 0; i < labelName1.length; i++) {
//            HashMap<String, Object> relation = new HashMap<>();
//            relation.put("id", i + 1001);
//            relation.put("name", labelName1[i]);
//            relation.put("pid", "");
//            relation.put("tid", labelTid1[i]);
//            relation.put("sid", i + 1);
//            label1.put(relation);
//        }
		/*-------------------------------------------------------------------------------------*/
        // 真题类型
//        HashMap<String, String> primaryType = new HashMap<>();
//        primaryType.put("id", "13");
//        primaryType.put("name", "小考真题");
//        primaryType.put("pid", "");
//        primaryType.put("tid", "1001");
//        arr1.put(primaryType);

//        String[] typeId = { "10", "12" };
//        String[] typeName = { "中考真题", "高考真题" };
//        for (int i = 0; i < typeId.length; i++) {
//            HashMap<String, Object> relation = new HashMap<>();
//            relation.put("id", typeId[i]);
//            relation.put("name", typeName[i]);
//            relation.put("pid", "");
//            relation.put("tid", "1001");
//            arr2.put(relation);
//        }
		/*-------------------------------------------------------------------------------------*/
        // 地区
        // 真题类型和地区关系配置(中考)
        for (int i = 0; i < ConstanQuantity.strArea.length; i++) {
            HashMap<String, String> relation = new HashMap<>();
            relation.put("id", String.valueOf(i));
            relation.put("name", ConstanQuantity.strArea[i]);
            relation.put("pid", "");
            relation.put("tid", "1001");
            arr2.put(relation);
        }
//        // 真题类型和地区关系配置(高考)
//        for (int i = 0; i < ConstanQuantity.strArea.length; i++) {
//            HashMap<String, Object> relation = new HashMap<>();
//            relation.put("id", i + 100);
//            relation.put("name", ConstanQuantity.strArea[i]);
//            relation.put("pid", "12");
//            relation.put("tid", "1002");
//            arr2.put(relation);
//        }
		/*-------------------------------------------------------------------------------------*/
        int getTodayYear;
        Calendar now = Calendar.getInstance();
        int beginDate = 2005;// 起始时间为2005
        int nowMonth = now.get(Calendar.MONTH) + 1;
        if (nowMonth >= 6) {
            getTodayYear = now.get(Calendar.YEAR);
        } else {
            getTodayYear = now.get(Calendar.YEAR) - 1;
        }
        int dateLen = getTodayYear - beginDate;
        int x = 1;
        Object[] strChange = new Object[dateLen + 2];
        strChange[0] = "不限";
        // strChange数组赋值
        for (int i = dateLen; i >= 0; i--) {
            strChange[x++] = beginDate + i;
        }
        // 地区和年份关系配置(中考)
        for (int i = 0; i < ConstanQuantity.strArea.length; i++) {
            int yz = getTodayYear;
            for (int j = 0; j < strChange.length; j++) {
                HashMap<String, String> relation = new HashMap<>();
                if(j == 0){
                    relation.put("id",String.valueOf(j + 2004) );
                }else{
                    relation.put("id", String.valueOf(yz--));
                }
                relation.put("name",  String.valueOf(strChange[j]));
                relation.put("pid", String.valueOf(i));
                relation.put("tid", "1002");
                arr2.put(relation);
            }
        }
//        // 地区和年份关系配置(高考)
//        for (int i = 0; i < ConstanQuantity.strArea.length; i++) {
//            int yg = getTodayYear + 100;
//            for (int j = 0; j < strChange.length; j++) {
//                HashMap<String, Object> relation = new HashMap<>();
//                if(j == 0){
//                    relation.put("id", j + 2104);
//                }else{
//                    relation.put("id", yg--);
//                }
//                relation.put("name", strChange[j]);
//                relation.put("pid", i + 100);
//                relation.put("tid", 1003);
//                arr2.put(relation);
//            }
//        }
		/*-------------------------------------------------------------------------------------*/
        // 年份和学科关系配置(中考)
        for (int i = 0; i < strChange.length; i++) {
            for (int j = 0; j < ConstanQuantity.strHighSubject.length; j++) {
                HashMap<String, String> relation = new HashMap<>();
                relation.put("id", ConstanQuantity.strHighId[j]);
                relation.put("name", ConstanQuantity.strHighSubject[j]);
                relation.put("pid", String.valueOf(i + 2004));
                relation.put("tid", "1003");
                arr2.put(relation);
            }
        }
//        // 年份和学科关系配置(高考)
//        for (int i = 0; i < strChange.length; i++) {
//            for (int j = 0; j < ConstanQuantity.strSeniorSubject.length; j++) {
//                HashMap<String, Object> relation = new HashMap<>();
//                relation.put("id", ConstanQuantity.strSeniorId[j]);
//                relation.put("name", ConstanQuantity.strSeniorSubject[j]);
//                relation.put("pid", i + 2104);
//                relation.put("tid", "1004");
//                arr2.put(relation);
//            }
//        }
        // 小考真题学科
//        String[] subjectId3 = { "1", "2", "6" };
//        String[] subjectName3 = { "语文", "数学", "英语" };
//        // 小考真题类型和学科关系
//        for (int i = 0; i < subjectName3.length; i++) {
//            HashMap<String, Object> relation = new HashMap<>();
//            relation.put("id", subjectId3[i]);
//            relation.put("name", subjectName3[i]);
//            relation.put("pid", "13");
//            relation.put("tid", "1002");
//            arr1.put(relation);
//        }

        // 数据组装
        // 中学
        JSONObject obj2 = new JSONObject();
        obj2.put("lab", label2);
        obj2.put("data", arr2);
        obj2.put("phase", ApiErrorCode.MIDDLE_SCHOOL);
//        obj2.put("def", def2);
//        // 小学
//        JSONObject obj1 = new JSONObject();
//        obj1.put("lab", label1);
//        obj1.put("data", arr1);
//        obj1.put("phase", ApiErrorCode.GRADE_SCHOOL);
//        obj1.put("def", def1);

        JSONArray json = new JSONArray();
//        json.put(obj1);
        json.put(obj2);

        JSONObject data = new JSONObject();
        data.put("data", json);
        String ret = ApiErrorCode.echoClassOk(data);
        return ret;
    }

    /**
     * 初始化请求数据
     *
     * @throws Exception
     */
    public static void initPastExam() throws Exception {
        Jedis redis = null;
        try {
            String PastExam = constructPastExam();
            if (StringUtils.isEmpty(PastExam)) {
                m_logger.debug("initPastExam fail,pastExam length 0");
                return;
            }
            redis = RedisBaseUtil.getBaseRedis().getResource();
            RedisBaseUtil.setByVersion(REDIS_PASTEXAM_KEY, PastExam,redis);
        } catch (Exception e) {
            m_logger.debug("initPastExam fail" + e.getCause());
            if (redis != null) {
                RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
            }
            throw e;
        } finally {
            RedisBaseUtil.getBaseRedis().returnResource(redis);
        }
    }

    /**
     * 获取数据
     *
     * @return
     * @throws Exception
     */
    public static String getPastExam() throws Exception {
        Jedis redis = null;
        String PastExam = null;
        try {
            redis = RedisBaseUtil.getBaseRedis().getResource();
            PastExam = RedisBaseUtil.getByVersion(REDIS_PASTEXAM_KEY,redis);
        } catch (Exception e) {
            m_logger.debug("initPastExam fail" + e.getCause());
            if (redis != null) {
                RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
            }
            throw e;
        } finally {
            RedisBaseUtil.getBaseRedis().returnResource(redis);
        }
        return PastExam;
    }
}
