package com.hq.api.subapps.oldexam;


import com.hq.api.subapps.common.ConstanQuantity;
import com.hq.api.subapps.datautils.HqdbDataSqlServeBaseUtil;
import com.hq.api.utils.ApiErrorCode;
import com.hq.dispatcher.Config;
import com.hq.utils.SqlServerBaseUtil;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class ExamsDatabase {


    /**
     * star = 2时，查询省份
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getProvinceArea2(String pid) throws SQLException{
        String sql = "select Name name from District where ID = (select ParentID pid from District where ID = (select ParentID pid from District where ID = ?))";
        ArrayList<Object> params = new ArrayList<>();
        params.add(pid);
        return SqlServerBaseUtil.getOneRow(sql,params);
    }

    /**
     * star = 1时，查询省份
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getProvinceArea1(String pid) throws SQLException{
        String sql = "select Name name from District where ID = (select ParentID pid from District where ID = ?)";
        ArrayList<Object> params = new ArrayList<>();
        params.add(pid);
        return SqlServerBaseUtil.getOneRow(sql,params);
    }

    /**
     * 查询老师学段
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getTeaPhase(String uid) throws SQLException{
        String sql = "select Phase phase from Teacher where UserID = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(uid);
        return SqlServerBaseUtil.getOneRow(sql,params);
    }

    /**
     * 查询学生学段
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getStuPhase(String uid) throws SQLException{
        String sql = "select Phase phase from Student where UserID = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(uid);
        return SqlServerBaseUtil.getOneRow(sql,params);
    }

    /**
     * 查询老师所在地区编码
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getTeaArea(String uid) throws SQLException{
        String sql = "select ID id,Name name,nStar star from District where ID = (select DistrictID districtid from School where ID = (select SchoolID schoolid from Teacher where UserID = ?))";
        ArrayList<Object> params = new ArrayList<>();
        params.add(uid);
        return SqlServerBaseUtil.getOneRow(sql,params);
    }

    /**
     * 查询学生所在地区编码
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getStuArea(String uid) throws SQLException{
        String sql = "select ID id,Name name,nStar star from District where ID = (select DistrictID districtid from School where ID = (select SchoolID schoolid from Student where UserID = ?))";
        ArrayList<Object> params = new ArrayList<>();
        params.add(uid);
        return SqlServerBaseUtil.getOneRow(sql,params);
    }

    /**
     * 查询中高考真题资源
     * @return
     * @throws SQLException
     */
    public static HashMap<String,Object> getCollegeExamList(HashMap<String,String> condition) throws SQLException{
        int pageSizeInt=10;
        int pagenumInt=1;
        HashMap<String,Object> map = new HashMap<String,Object>();

        String sql = "select id roid,topic title,DATEDIFF( SECOND, '1970-01-01 08:00:00',tim) as createtime,fsize size,url1 downurl,stars from res_data where hidden = 1";
        String sqlcount="select count(id) from res_data where hidden=1";
        int type=0;
        if(condition.get("type").equals(ApiErrorCode.GRADE_SCHOOL)){
            type=10;
        }else if(condition.get("type").equals(ApiErrorCode.MIDDLE_SCHOOL)){
            type=12;
        }
        ArrayList<Object> params = new ArrayList<>();
        sql+= " and c_id = ?";
        sqlcount+= " and c_id = ?";
        params.add(type);
        if(!StringUtils.isEmpty(condition.get("area")) && !condition.get("area").equals("0")){
            sql+= " and area = ?";
            sqlcount+= " and area = ?";
            params.add(condition.get("area"));
        }
        if(!StringUtils.isEmpty(condition.get("year")) && !condition.get("year").equals("0")){
            sql+= " and year1 = ?";
            sqlcount+= " and year1 = ?";
            params.add(condition.get("year"));
        }
        if(!StringUtils.isEmpty(condition.get("subid")) && !condition.get("subid").equals("0")){
            sql+= " and object_id = ?";
            sqlcount+= " and object_id = ?";
            params.add(condition.get("subid"));
        }
        if(!StringUtils.isEmpty(condition.get("keyword"))){
            sql+=  " and (topic like '%"+ condition.get("keyword") + "%' or keyes like '%"+ condition.get("keyword") + "%' )  ";
            sqlcount+=  " and (topic like '%"+ condition.get("keyword") + "%' or keyes like '%"+ condition.get("keyword") + "%' )  ";
        }

//        sql+=" and (topic not like '%"+ ConstanQuantity.NoAnswer + "%' and keyes not like '%"+ ConstanQuantity.NoAnswer + "%' )  and  convert(varchar(100),tim,23)>'2015-08-15' ";
//        sqlcount+=" and (topic not like '%"+ ConstanQuantity.NoAnswer + "%' and keyes not like '%"+ ConstanQuantity.NoAnswer + "%' )  and  convert(varchar(100),tim,23)>'2015-08-15' ";

        sql+=" and topic not like '%"+ ConstanQuantity.NoAnswer + "%'   and  convert(varchar(100),tim,23)>'2015-08-15' ";
        sqlcount+=" and topic not like '%"+ ConstanQuantity.NoAnswer + "%'   and  convert(varchar(100),tim,23)>'2015-08-15' ";



        switch (condition.get("sortby")){
            case "0":
                sql += " order by tim desc";
                break;
            case "1":
                sql += " order by tim desc";
                break;
            case "2":
                sql += " order by stars desc,id desc";
                break;
            case "3":
                sql += " order by counter desc,id desc";
                break;
            default:
                sql += " order by tim desc";
        }

        if(StringUtils.isEmpty(condition.get("pagenum")) || Integer.parseInt(condition.get("pagenum"))<1){
            pagenumInt = 1;
        }else{
            pagenumInt = Integer.parseInt(condition.get("pagenum"));
        }

        if(StringUtils.isEmpty(condition.get("pagesize"))){
            pageSizeInt = Config.getInstance().getInt("system_pageSize",10);
        }else {
            pageSizeInt = Integer.parseInt(condition.get("pagesize"));
        }

        sql += " OFFSET " + pageSizeInt*(pagenumInt-1)  +" ROWS ";
        sql += " FETCH NEXT " + pageSizeInt + " ROWS ONLY ";
        String rescount = HqdbDataSqlServeBaseUtil.getOneColumnByRow(sqlcount, params);
        ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>();
        if(!rescount.equals("0")){
            res = HqdbDataSqlServeBaseUtil.querySql(sql, params);
        }
        map.put("count",rescount);
        map.put("res", res);

        return map;
    }


}
