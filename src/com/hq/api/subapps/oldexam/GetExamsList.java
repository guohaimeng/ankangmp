package com.hq.api.subapps.oldexam;


import com.hq.api.subapps.common.ResourcDatabase;
import com.hq.api.utils.ApiErrorCode;
import com.hq.api.utils.CheckParameterException;
import com.hq.api.utils.DatabaseManager;
import com.hq.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import com.hq.api.utils.CheckParameters;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 真题列表资源
 */
@WebServlet("/subapps/getExamList.do")
public class GetExamsList extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(GetExamsList.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        JSONObject topicObj = null;
        try {
            //参数校验
            CheckParameters check = new CheckParameters(request.getParameterMap());
            String type = "";//类型  1中考真题  2高考真题
            String sortby = "";//排序
            String keyword = "";//关键字
            String area = "";//地区
            String year = "";//年份
            String subid = "";//学科
            String pagesize = "";
            String pagenum = "";
            check.addParameter("type", CheckParameters.paraType.EXP, "^[1,2]$");
            type = check.get("type").toString();

            if (!StringUtils.isEmpty(request.getParameter("sortby"))) {
                check.addParameter("sortby", CheckParameters.paraType.EXP, "^[0-3]$");//排序方式 0 默认排序,1 按上传时间,2 按星级,3 按下载次数
                sortby = check.get("sortby").toString();
            } else {
                sortby = "0";
            }
            if (!StringUtils.isEmpty(request.getParameter("keyword"))) {
                check.addParameter("keyword", CheckParameters.paraType.EXP, "[a-zA-Z0-9\u4e00-\u9fa5]{1,30}");
                keyword = check.get("keyword").toString();
            }

            if (!StringUtils.isEmpty(request.getParameter("pagesize"))) {
                check.addParameter("pagesize", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
                pagesize = check.get("pagesize").toString();
            }

            if (!StringUtils.isEmpty(request.getParameter("pagenum"))) {
                check.addParameter("pagenum", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
                pagenum = check.get("pagenum").toString();
            }

            if (!StringUtils.isEmpty(request.getParameter("area"))) {
                check.addParameter("area", CheckParameters.paraType.EXP, "^([0-9]|1[0-9]|2[0-9]|3[1,2])$");
                area = check.get("area").toString();
            }

            if (!StringUtils.isEmpty(request.getParameter("year"))) {
                check.addParameter("year", CheckParameters.paraType.EXP, "^([2][0][0][4-9]|[2][0][1-5][0-9])$");
                year = check.get("year").toString();
            }

            if (!StringUtils.isEmpty(request.getParameter("subid"))) {
                check.addParameter("subid", CheckParameters.paraType.STRING, 1, 5);
            }

            //session校验
            Map<String, String> sessionInfo = DatabaseManager.getSession(request);


            HashMap<String, String> condition = new HashMap<String, String>();
            condition.put("type", type);
            condition.put("sortby", sortby);
            condition.put("keyword", keyword);
            condition.put("pagesize", pagesize);
            condition.put("pagenum", pagenum);

            HashMap<String, Object> data = null;

            if (year.equals("2004")) {
                year = "";
            }
            condition.put("area", area);
            condition.put("year", year);
            condition.put("subid", subid);
            data = ExamsDatabase.getCollegeExamList(condition);


            ArrayList<HashMap<String, String>> res = (ArrayList<HashMap<String, String>>) data.get("res");
            for (HashMap<String, String> map : res) {
                if (type.equals("13")) {
                    map.put("phase", ApiErrorCode.GRADE_SCHOOL);
                } else {
                    map.put("phase", ApiErrorCode.MIDDLE_SCHOOL);
                }
                if (StringUtils.isEmpty(map.get("downurl"))) {
                    map.put("extname", "");
                } else {
                    map.put("extname", ResourcDatabase.getResExtname(map.get("downurl")));
                }
                map.put("size", ResourcDatabase.getResFileSize(map.get("phase"), map.get("size"), ResourcDatabase.getResExtname(map.get("downurl"))));
                map.remove("downurl");
            }

            topicObj = new JSONObject();
            topicObj.put("data", data.get("res"));
            topicObj.put("count", data.get("count"));
            String ret = ApiErrorCode.echoClassOk(topicObj);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            return;

        } catch (CheckParameterException ce) {
            m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR, ce));
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR));
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED Exception=%s", e.getMessage()), e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            return;
        }
    }
}