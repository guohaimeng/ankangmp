package com.hq.api.subapps.basedroll;

import com.hq.api.subapps.common.ConstanQuantity;
import com.hq.api.subapps.common.SharMethod;
import com.hq.api.utils.ApiErrorCode;
import com.hq.api.utils.CheckParameterException;
import com.hq.api.utils.CheckParameters;
import com.hq.api.utils.DatabaseManager;
import com.hq.dispatcher.Config;
import com.hq.utils.DoResponseUtil;
import com.hq.utils.RedisBaseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import redis.clients.jedis.Jedis;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 获取双基双测AB卷基础数据
 */
@WebServlet("/subapps/getBasedRoll.do")
public class GetBaseRoll extends HttpServlet {
    private final static String REDIS_DOUBASEDROLL_KEY = "Basedroll.paper";
    private static final Logger m_logger = Logger.getLogger(GetBaseRoll.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        try {
            // session校验
            Map<String, String> sessionInfo = DatabaseManager.getSession(request);

            String grade = sessionInfo.get("grade");
            String ret = getDouBasedroll();
            JSONObject obj = new JSONObject(ret);
            JSONArray jsonarrayGrade  =  obj.getJSONArray("data").getJSONObject(0).getJSONArray("def");
            JSONArray  jsonarrayMiddle  =  obj.getJSONArray("data").getJSONObject(1).getJSONArray("def");
//
            HashMap<String, String> preNature = null;
            if(Arrays.asList(ConstanQuantity.JuniorGrade).contains(grade)){
                preNature = SharMethod.getCommonMap(ApiErrorCode.GRADE_SCHOOL,SharMethod.schoolnameStr[0],"",SharMethod.tidStr[0]);
                jsonarrayGrade.put(preNature);
            }else if(Arrays.asList(ConstanQuantity.MiddleGrade).contains(grade)){
                preNature = SharMethod.getCommonMap(ApiErrorCode.MIDDLE_SCHOOL,SharMethod.schoolnameStr[1],"",SharMethod.tidStr[0]);
                jsonarrayMiddle.put(preNature);
            }else{
                //
            }




            String json = ApiErrorCode.echoClassOk(obj);
            // 数据压缩
            DoResponseUtil.ResponseWrite(request, response, json);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            return;

        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR, ce));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED Exception=%s", e.getMessage()), e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            return;
        }


    }

    /**
     * 获取双基双测AB卷数据构造
     * */
    public static String constructDouBasedroll() throws SQLException, JSONException {
        JSONArray label = new JSONArray();// 标签
        JSONArray arrJunior = new JSONArray();// 小学
        JSONArray arrMiddle = new JSONArray();// 中学
        JSONArray defJunior = new JSONArray();// 小学默认
        JSONArray defMiddle = new JSONArray();// 中学默认


        // 标签
        String[] strLabelName = { "学段", "学科", "年级", "版本" };
        String[] strLabelID = { "phase", "subid", "grid", "veid" };
        for (int i = 0; i < strLabelName.length; i++) {
            HashMap<String, Object> relation = new HashMap<>();
            relation.put("id", i + 1001);
            relation.put("name", strLabelName[i]);
            relation.put("pid", "");
            relation.put("tid", strLabelID[i]);
            relation.put("sid", i + 1);
            label.put(relation);
        }

        // 小学学段
        HashMap<String, String> juniorPeriod = new HashMap<String, String>();
        juniorPeriod.put("id", ApiErrorCode.GRADE_SCHOOL);
        juniorPeriod.put("name", "小学");
        juniorPeriod.put("pid", "");
        juniorPeriod.put("tid", "1001");
        arrJunior.put(juniorPeriod);

        // 小学学科,年级,版本
        ArrayList<HashMap<String, String>> JuniorRelation = BaseRollDatabase.getRelationByJunior();
        for (int i = 0; i < JuniorRelation.size(); i++) {
            HashMap<String, String> relation = JuniorRelation.get(i);
            if (relation.get("cid").toString().equals("0")) {
                relation.put("tid", "1002");
                relation.put("pid", "");
            }
            if (relation.get("cid").toString().equals("1")) {
                relation.put("tid", "1003");
            }
            if (relation.get("cid").toString().equals("2")) {
                relation.put("tid", "1004");
            }
            relation.remove("cid");
            arrJunior.put(relation);
        }

        // 数据组装
        JSONObject juniorData = new JSONObject();
        juniorData.put("data", arrJunior);
        juniorData.put("lab", label);
        juniorData.put("phase", ApiErrorCode.GRADE_SCHOOL);
        juniorData.put("def", defJunior);

        // 中学学段
        HashMap<String, String> middlePeriod = new HashMap<String, String>();
        middlePeriod.put("id", ApiErrorCode.MIDDLE_SCHOOL);
        middlePeriod.put("name", "中学");
        middlePeriod.put("pid", "");
        middlePeriod.put("tid", "1001");
        arrMiddle.put(middlePeriod);


        //获取中学科目"综合"ID 及 子级ID
        int complete = 15;
        ArrayList<HashMap<String, String>> CompleteSublevel = BaseRollDatabase.getCompleteSublevel(complete);
        String sublevelstr  = SharMethod.dealwithparam(CompleteSublevel);
        // 中学关系数据(排除 综合 学科及子级年级和版本)
        ArrayList<HashMap<String, String>> MiddleRelation = BaseRollDatabase.getRelationByMiddle(sublevelstr);
        for (int i = 0; i < MiddleRelation.size(); i++) {
            HashMap<String, String> relation = MiddleRelation.get(i);
            if (relation.get("cid").toString().equals("0")) {
                relation.put("tid", "1002");
                relation.put("pid", "");
            }
            if (relation.get("cid").toString().equals("1")) {
                relation.put("tid", "1003");
            }
            if (relation.get("cid").toString().equals("2")) {
                relation.put("tid", "1004");
            }
            relation.remove("cid");
            arrMiddle.put(relation);
        }

        // 数据组装
        JSONObject middleData = new JSONObject();
        middleData.put("lab", label);
        middleData.put("data", arrMiddle);
        middleData.put("phase", ApiErrorCode.MIDDLE_SCHOOL);
        middleData.put("def", defMiddle);

        JSONArray jsonarray = new JSONArray();
        jsonarray.put(juniorData);
        jsonarray.put(middleData);

        JSONObject Data = new JSONObject();
        Data.put("data", jsonarray);
        String ret = ApiErrorCode.echoClassOk(Data);
        return ret;
    }

    /**
     * 初始化双基双测AB卷数据
     *
     * @throws Exception
     */
    public static void initDouBasedroll() throws Exception {
        Jedis redis = null;
        try {
            String Basedroll = constructDouBasedroll();
            if (StringUtils.isEmpty(Basedroll)) {
                m_logger.debug("initBasedroll fail,courseware length 0");
                return;
            }
            redis = RedisBaseUtil.getBaseRedis().getResource();
            RedisBaseUtil.setByVersion(REDIS_DOUBASEDROLL_KEY, Basedroll,redis);
        } catch (Exception e) {
            m_logger.debug("initBasedroll fail" + e.getCause());
            if (redis != null) {
                RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
            }
            throw e;
        } finally {
            RedisBaseUtil.getBaseRedis().returnResource(redis);
        }
    }

    /**
     * 获取双基双测AB卷数据
     *
     * @return
     * @throws Exception
     */
    public static String getDouBasedroll() throws Exception {
        Jedis redis = null;
        String Basedroll = null;
        try {
            redis = RedisBaseUtil.getBaseRedis().getResource();
            Basedroll = RedisBaseUtil.getByVersion(REDIS_DOUBASEDROLL_KEY,redis);
        } catch (Exception e) {
            m_logger.debug("getBasedroll fail" + e.getCause());
            if (redis != null) {
                RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
            }
            throw e;
        } finally {
            RedisBaseUtil.getBaseRedis().returnResource(redis);
        }
        return Basedroll;
    }
}