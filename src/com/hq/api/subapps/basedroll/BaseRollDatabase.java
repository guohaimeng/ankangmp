package com.hq.api.subapps.basedroll;


import com.hq.api.subapps.common.ConstanQuantity;
import com.hq.api.subapps.datautils.HQPrimDataSqlServeBaseUtil;
import com.hq.api.subapps.datautils.HqdbDataSqlServeBaseUtil;
import com.hq.api.utils.ApiErrorCode;
import com.hq.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 2018/8/15.
 */
public class BaseRollDatabase {
    /**
     * 查询小学学科，年级，版本关系数据（有cid）
     * @reurn
     * @throws SQLException
     */
    public static ArrayList<HashMap<String,String>> getRelationByJunior() throws SQLException{
        String sql="select ID id,relationName name,Pid pid,ClassId cid from fs_channel_relation order by VersionId asc,GradeId asc";
        ArrayList<Object> params = new ArrayList<>();
        return HQPrimDataSqlServeBaseUtil.querySql(sql, params);
    }

    /**
     * 获取中学科目"综合"及子级ID
     * @reurn
     * @throws SQLException
     */
    public static ArrayList<HashMap<String,String>> getCompleteSublevel(int complete) throws SQLException{
        String sql="WITH complete(myocb_id,myocb_f) as( SELECT myocb_id,myocb_f FROM hq_ocb WHERE object_id =? and myocb_hidden=0 union all SELECT a.myocb_id,a.myocb_f FROM hq_ocb a,complete c WHERE a.myocb_f =c.myocb_id and a.myocb_hidden=0) SELECT myocb_id as id  from complete";
        ArrayList<Object> params = new ArrayList<>();
        params.add(complete);
        return HqdbDataSqlServeBaseUtil.querySql(sql, params);
    }
    /**
     * 查询中学学科，年级，版本关联数据（有cid）
     * @reurn
     * @throws SQLException
     */
    public static ArrayList<HashMap<String,String>> getRelationByMiddle(String strs) throws SQLException{
        String sql="select myocb_id as id,myocb_f as pid,myocb_name name,myocb_star as cid  from hq_ocb where myocb_hidden = 0";
        if(!StringUtils.isEmpty(strs)){
            sql+=" and  myocb_id not in("+strs+") ";
        }
        ArrayList<Object> params = new ArrayList<>();
        return HqdbDataSqlServeBaseUtil.querySql(sql, params);
    }

    /**
     * 根据ID查询中小学关系学科，版本，年级
     * @reurn
     * @throws SQLException
     */
    public static HashMap<String, String> getRelationById(String reid, String phase) throws SQLException{
        String sql="";
        ArrayList<Object> params = new ArrayList<>();
        params.add(reid);
        HashMap<String,String>  datamap = null;
        if(phase.equals(ApiErrorCode.GRADE_SCHOOL)){
            sql="select SubjectId as subid,GradeId as grid,VersionId as veid from fs_channel_relation where Id=?";
            datamap =  HQPrimDataSqlServeBaseUtil.getOneRow(sql, params);
        }else if (phase.equals(ApiErrorCode.MIDDLE_SCHOOL)){
            sql="select myocb_star star,myocb_parent parent,object_id subid,s_id grid,bb_id veid  from hq_ocb where myocb_id=?";
            datamap =  HqdbDataSqlServeBaseUtil.getOneRow(sql, params);
        }else{
            //
        }
        return datamap;
    }

    /**
     * 查询小学双基双测AB卷资源
     * @return
     * @throws SQLException
     */
    public static HashMap<String,Object> getCompleteByJunior(HashMap<String,String> condition) throws SQLException{
        int pageSizeInt=10;
        int pagenumInt=1;
        HashMap<String,Object> map = new HashMap<String,Object>();

        String sql="select 1 as phase, Id roid,title,ResourceUrl downurl,ResourseSize size,stars,DATEDIFF( SECOND, '1970-01-01 08:00:00',CreatTime) createtime from fs_channel_zyxz where hidden = 1 and ClassID = 51";
        String sqlcount="select count(id) from fs_channel_zyxz where hidden = 1 and ClassID = 51";
        ArrayList<Object> params = new ArrayList<>();
        if(!StringUtils.isEmpty(condition.get("subid")) && !condition.get("subid").equals("0")){
            sql+=" and SubjectId = ? ";
            sqlcount+=" and SubjectId = ? ";
            params.add(condition.get("subid"));
        }
        if(!StringUtils.isEmpty(condition.get("grid")) && !condition.get("grid").equals("0")){
            sql+=" and GradeId = ? ";
            sqlcount+=" and GradeId = ? ";
            params.add(condition.get("grid"));
        }
        if(!StringUtils.isEmpty(condition.get("veid")) && !condition.get("veid").equals("0")){
            sql+=" and VersionId = ? ";
            sqlcount+=" and VersionId = ? ";
            params.add(condition.get("veid"));
        }
        if(!StringUtils.isEmpty(condition.get("keyword"))){
            sql+=  " and (title like '%"+ condition.get("keyword") + "%' or Tags like '%"+ condition.get("keyword") + "%' )  ";
            sqlcount+=  " and (title like '%"+ condition.get("keyword") + "%' or Tags like '%"+ condition.get("keyword") + "%' )  ";
        }
//        sql+=" and (title not like '%"+ ConstanQuantity.NoAnswer + "%' and Tags not like '%"+ ConstanQuantity.NoAnswer + "%' )  and  convert(varchar(100),CreatTime,23)>='2015-08-15' ";
//        sqlcount+=" and (title not like '%"+ ConstanQuantity.NoAnswer + "%' and Tags not like '%"+ ConstanQuantity.NoAnswer + "%' )  and  convert(varchar(100),CreatTime,23)>='2015-08-15' ";

        sql+=" and title not like '%"+ ConstanQuantity.NoAnswer + "%'   and  convert(varchar(100),CreatTime,23)>='2015-08-15' ";
        sqlcount+=" and title not like '%"+ ConstanQuantity.NoAnswer + "%'   and  convert(varchar(100),CreatTime,23)>='2015-08-15' ";


        switch (condition.get("sortby")){
            case "0":
                sql += " order by CreatTime desc";
                break;
            case "1":
                sql += " order by CreatTime desc";
                break;
            case "2":
                sql += " order by stars desc,Id desc";
                break;
            case "3":
                sql += " order by downCount desc,Id desc";
                break;
            default:
                sql += " order by CreatTime desc";
        }

        if(StringUtils.isEmpty(condition.get("pagenum")) || Integer.parseInt(condition.get("pagenum"))<1){
            pagenumInt = 1;
        }else{
            pagenumInt = Integer.parseInt(condition.get("pagenum"));
        }

        if(StringUtils.isEmpty(condition.get("pagesize"))){
            pageSizeInt = Config.getInstance().getInt("system_pageSize",10);
        }else {
            pageSizeInt = Integer.parseInt(condition.get("pagesize"));
        }

        sql += " OFFSET " + pageSizeInt*(pagenumInt-1)  +" ROWS ";
        sql += " FETCH NEXT " + pageSizeInt + " ROWS ONLY ";
        String rescount = HQPrimDataSqlServeBaseUtil.getOneColumnByRow(sqlcount, params);
        ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>();
        if(!rescount.equals("0")){
            res = HQPrimDataSqlServeBaseUtil.querySql(sql, params);
        }
        map.put("count",rescount);
        map.put("res", res);

        return map;
    }


    /**
     * 查询中学双基双测AB卷资源
     * @return
     * @throws SQLException
     */
    public static HashMap<String,Object> getCompleteByMiddle(HashMap<String,String> relation) throws SQLException{
        int pageSizeInt=10;
        int pagenumInt=1;
        HashMap<String,Object> map = new HashMap<String,Object>();

        String sql="select 2 as phase,id roid,topic title,stars stars,url1 downurl,fsize size,DATEDIFF( SECOND, '1970-01-01 08:00:00',tim)createtime from res_data where hidden = 1 and c_id = 109";
        String sqlcount="select count(id) from res_data where hidden = 1 and c_id = 109";
        ArrayList<Object> params = new ArrayList<>();
        if(!StringUtils.isEmpty(relation.get("subid")) && !relation.get("subid").equals("0")){
            sql+=" and object_id = ? ";
            sqlcount+=" and object_id = ? ";
            params.add(relation.get("subid"));
        }
        if(!StringUtils.isEmpty(relation.get("grid")) && !relation.get("grid").equals("0")){
            sql+=" and s_id = ? ";
            sqlcount+=" and s_id = ? ";
            params.add(relation.get("grid"));
        }
        if(!StringUtils.isEmpty(relation.get("veid")) && !relation.get("veid").equals("0")){
            sql+=" and res_bb = ? ";
            sqlcount+=" and res_bb = ? ";
            params.add(relation.get("veid"));
        }

        if(!StringUtils.isEmpty(relation.get("keyword"))){
            sql+=  "  (topic like '%"+ relation.get("keyword") + "%' or keyes like '%"+ relation.get("keyword") + "%' )  ";
            sqlcount+=  " and (topic like '%"+ relation.get("keyword") + "%' or keyes like '%"+ relation.get("keyword") + "%' )  ";
        }


//        sql+=" and (topic not like '%"+ ConstanQuantity.NoAnswer + "%' and keyes not like '%"+ ConstanQuantity.NoAnswer + "%' )  and  convert(varchar(100),tim,23)>'2015-08-15' ";
//        sqlcount+=" and (topic not like '%"+ ConstanQuantity.NoAnswer + "%' and keyes not like '%"+ ConstanQuantity.NoAnswer + "%' )  and  convert(varchar(100),tim,23)>'2015-08-15' ";

        sql+=" and topic not like '%"+ ConstanQuantity.NoAnswer + "%'   and  convert(varchar(100),tim,23)>'2015-08-15' ";
        sqlcount+=" and topic not like '%"+ ConstanQuantity.NoAnswer + "%'   and  convert(varchar(100),tim,23)>'2015-08-15' ";


        switch (relation.get("sortby")){
            case "0":
                sql += " order by tim desc";
                break;
            case "1":
                sql += " order by tim desc";
                break;
            case "2":
                sql += " order by stars desc,id desc";
                break;
            case "3":
                sql += " order by counter desc,id desc";
                break;
            default:
                sql += " order by tim desc";
        }
        if(StringUtils.isEmpty(relation.get("pagenum")) || Integer.parseInt(relation.get("pagenum"))<1){
            pagenumInt = 1;
        }else{
            pagenumInt = Integer.parseInt(relation.get("pagenum"));
        }


        if(StringUtils.isEmpty(relation.get("pagesize"))){
            pageSizeInt = Config.getInstance().getInt("system_pageSize",10);
        }else {
            pageSizeInt = Integer.parseInt(relation.get("pagesize"));
        }

        sql += " OFFSET " + pageSizeInt*(pagenumInt-1)  +" ROWS ";
        sql += " FETCH NEXT " + pageSizeInt + " ROWS ONLY ";
        String rescount = HqdbDataSqlServeBaseUtil.getOneColumnByRow(sqlcount, params);
        ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>();
        if(!rescount.equals("0")){
            res = HqdbDataSqlServeBaseUtil.querySql(sql, params);
        }
        map.put("count",rescount);
        map.put("res", res);

        return map;
    }







}
