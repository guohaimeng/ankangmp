package com.hq.api.subapps.seniorsimulation;


import com.hq.api.subapps.common.ConstanQuantity;
import com.hq.api.subapps.datautils.HqdbDataSqlServeBaseUtil;
import com.hq.api.utils.ApiErrorCode;
import com.hq.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class SeniorSimulatioDataBase {

	/**
	 * 查询中学学科
	 *
	 * @throws SQLException
	 */
	public static ArrayList<HashMap<String, String>> getRelationByMiddle() throws SQLException {
		String sql = "select object_id id,myocb_f pid,myocb_name name from hq_ocb where myocb_hidden = 0 and myocb_f = 0 and object_id not in(15)";
		ArrayList<Object> params = new ArrayList<>();
		return HqdbDataSqlServeBaseUtil.querySql(sql, params);
	}

	/**
	 * 查询中考/高考模拟资源
	 *
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<String, Object> getSimulationByMiddle(HashMap<String, String> relation)
			throws SQLException {
		int pageSizeInt = 10;
		int pagenumInt = 1;
		HashMap<String, Object> map = new HashMap<String, Object>();

		String sql = "select id roid,topic title,stars stars,url1 downurl,fsize size,DATEDIFF( SECOND, '1970-01-01 08:00:00',tim)createtime from res_data where hidden = 1";
		String sqlcount = "select count(id) from res_data where hidden = 1";
		ArrayList<Object> params = new ArrayList<>();
		int  type=0;
		if(relation.get("type").equals(ApiErrorCode.GRADE_SCHOOL)){
			type=11;
		}else if(relation.get("type").equals(ApiErrorCode.MIDDLE_SCHOOL)){
			type=13;
		}
		sql+= " and c_id = ?";
		sqlcount+= " and c_id = ?";
		params.add(type);
		if (!StringUtils.isEmpty(relation.get("subid")) && !relation.get("subid").equals("0")) {
			sql += " and object_id = ? ";
			sqlcount += " and object_id = ? ";
			params.add(relation.get("subid"));
		}

		if(!StringUtils.isEmpty(relation.get("keyword"))){
			sql+=  " and (topic like '%"+ relation.get("keyword") + "%' or keyes like '%"+ relation.get("keyword") + "%' )  ";
			sqlcount+=  " and (topic like '%"+ relation.get("keyword") + "%' or keyes like '%"+ relation.get("keyword") + "%' )  ";
		}


//		sql+=" and (topic not like '%"+ ConstanQuantity.NoAnswer + "%' and keyes not like '%"+ ConstanQuantity.NoAnswer + "%' )  and (topic not like '%"+ ConstanQuantity.NoProvince + "%' and keyes not like '%"+ ConstanQuantity.NoProvince  + "%' )  and  convert(varchar(100),tim,23)>='2015-08-15' ";
//		sqlcount+=" and (topic not like '%"+ ConstanQuantity.NoAnswer + "%' and keyes not like '%"+ ConstanQuantity.NoAnswer + "%' ) and (topic not like '%"+ ConstanQuantity.NoProvince + "%' and keyes not like '%"+ ConstanQuantity.NoProvince  + "%' )  and  convert(varchar(100),tim,23)>='2015-08-15' ";

		sql+=" and topic not like '%"+ ConstanQuantity.NoAnswer + "%'     and  convert(varchar(100),tim,23)>='2015-08-15' ";
		sqlcount+=" and topic not like '%"+ ConstanQuantity.NoAnswer + "%'    and  convert(varchar(100),tim,23)>='2015-08-15' ";


		switch (relation.get("sortby")) {
			case "0":
				sql += " order by tim desc";
				break;
			case "1":
				sql += " order by tim desc";
				break;
			case "2":
				sql += " order by stars desc,id desc";
				break;
			case "3":
				sql += " order by counter desc,id desc";
				break;
			default:
				sql += " order by tim desc";
		}
		if (StringUtils.isEmpty(relation.get("pagenum")) || Integer.parseInt(relation.get("pagenum")) < 1) {
			pagenumInt = 1;
		} else {
			pagenumInt = Integer.parseInt(relation.get("pagenum"));
		}

		if (StringUtils.isEmpty(relation.get("pagesize"))) {
			pageSizeInt = Config.getInstance().getInt("system_pageSize", 10);
		} else {
			pageSizeInt = Integer.parseInt(relation.get("pagesize"));
		}

		sql += " OFFSET " + pageSizeInt * (pagenumInt - 1) + " ROWS ";
		sql += " FETCH NEXT " + pageSizeInt + " ROWS ONLY ";
		String rescount = HqdbDataSqlServeBaseUtil.getOneColumnByRow(sqlcount, params);
		ArrayList<HashMap<String, String>> res = new ArrayList<HashMap<String, String>>();
		if (!rescount.equals("0")) {
			res = HqdbDataSqlServeBaseUtil.querySql(sql, params);
		}
		map.put("count", rescount);
		map.put("res", res);

		return map;
	}

}
