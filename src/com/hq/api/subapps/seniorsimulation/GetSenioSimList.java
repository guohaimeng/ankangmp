package com.hq.api.subapps.seniorsimulation;


import com.hq.api.subapps.common.ResourcDatabase;
import com.hq.api.utils.ApiErrorCode;
import com.hq.api.utils.CheckParameterException;
import com.hq.api.utils.DatabaseManager;
import com.hq.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import com.hq.api.utils.CheckParameters;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 中高考模拟列表
 */
@WebServlet("/subapps/getSeniorSimList.do")
public class GetSenioSimList extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(GetSenioSimList.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        JSONObject ResourceObj = null;
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            check.addParameter("type", CheckParameters.paraType.EXP,"^[1,2]$");//类型(1为中考,2为高考)
            String subid="";//学科subid
            String sortby= "";//排序
            String keyword= "";//关键字
            String pagesize= "";//分页大小
            String pagenum= "";//分页页码

            if(!StringUtils.isEmpty(request.getParameter("subid"))){
                check.addParameter("subid", CheckParameters.paraType.STRING,1,8);
                subid = check.get("subid").toString();
            }

            if(!StringUtils.isEmpty(request.getParameter("sortby"))){
                check.addParameter("sortby", CheckParameters.paraType.EXP,"^[0-3]$"); //0 默认 1上传时间 2 星级 3 下载量
                sortby = check.get("sortby").toString();
            }else{
                sortby="0";
            }
            if(!StringUtils.isEmpty(request.getParameter("keyword"))){
                check.addParameter("keyword", CheckParameters.paraType.EXP,"[a-zA-Z0-9\u4e00-\u9fa5]{1,30}");
                keyword = check.get("keyword").toString();
            }

            if(!StringUtils.isEmpty(request.getParameter("pagesize"))){
                check.addParameter("pagesize", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
                pagesize = check.get("pagesize").toString();
            }

            if(!StringUtils.isEmpty(request.getParameter("pagenum"))){
                check.addParameter("pagenum", CheckParameters.paraType.EXP,Config.getInstance().getString("params_page"));
                pagenum = check.get("pagenum").toString();
            }

//            session校验
            Map<String, String> sessionInfo = DatabaseManager.getSession(request);

            HashMap<String,String> relation = new HashMap<>();
            relation.put("type", check.get("type").toString());
            relation.put("subid", subid);
            relation.put("sortby", sortby);
            relation.put("keyword", keyword);
            relation.put("pagenum", pagenum);
            relation.put("pagesize", pagesize);

            HashMap<String,Object> Resource = SeniorSimulatioDataBase.getSimulationByMiddle(relation);
            ArrayList<HashMap<String,String>> res = (ArrayList<HashMap<String, String>>) Resource.get("res");
//			 res  = ResourceDatabase.dealResExtname(ApiErrorCode.MIDDLE_SCHOOL,res);
            for (HashMap<String, String> map : res) {
                map.put("phase", ApiErrorCode.MIDDLE_SCHOOL);
                if(StringUtils.isEmpty(map.get("downurl"))){
                    map.put("extname", "");
                }else{
                    map.put("extname", ResourcDatabase.getResExtname(map.get("downurl")));
                }
                map.put("size", ResourcDatabase.getResFileSize(map.get("phase"),map.get("size"),ResourcDatabase.getResExtname(map.get("downurl"))));
                map.remove("downurl");
            }

            ResourceObj = new JSONObject();
            ResourceObj.put("data", res);
            ResourceObj.put("count",Resource.get("count"));
            String ret = ApiErrorCode.echoClassOk(ResourceObj);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            return;
        } catch(CheckParameterException ce){
            String ret=ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s %s",ApiErrorCode.PARAMETER_ERROR,ce));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s","system exception=",e),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            return;
        }
    }
}