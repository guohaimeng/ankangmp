package com.hq.api.subapps.seniorsimulation;


import com.hq.api.utils.ApiErrorCode;
import com.hq.api.utils.CheckParameterException;
import com.hq.api.utils.DatabaseManager;
import com.hq.dispatcher.Config;
import com.hq.utils.DoResponseUtil;
import com.hq.utils.RedisBaseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import redis.clients.jedis.Jedis;
import com.hq.api.utils.CheckParameters;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 获取中高考模拟基础数据
 */
@WebServlet("/subapps/getSeniorSimulation.do")
public class GetSeniorSimulatio extends HttpServlet {
    private static final String REDIS_SENIORSIMULATION_KEY = "Simulation.paper";
    private static final Logger m_logger = Logger.getLogger(GetSeniorSimulatio.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Map<String, String> sessionInfo = DatabaseManager.getSession(request);
            String ret = getSimulation();
            // 数据压缩
            DoResponseUtil.ResponseWrite(request, response, ret);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            return;

        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR, ce));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED Exception=%s", e.getMessage()), e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            return;
        }
    }

    /**
     * 构造数据
     *
     * @return
     * @throws Exception
     */
    public static String constructSimulation() throws Exception {
        JSONArray label = new JSONArray();// 标签
        JSONArray arr2 = new JSONArray();// 中学库
        JSONArray def2 = new JSONArray();// 中学默认

        HashMap<String, Object> relationLabel = null;// 标签变量

        // 标签
        String[] strLabName = { "学段", "学科" };
        String[] strLabID = { "phase", "subid" };
        for (int i = 0; i < strLabName.length; i++) {
            relationLabel = new HashMap<>();
            relationLabel.put("id", i + 1001);
            relationLabel.put("name", strLabName[i]);
            relationLabel.put("pid", "");
            relationLabel.put("tid", strLabID[i]);
            relationLabel.put("sid", i + 1);
            label.put(relationLabel);
        }

        // 学段
        HashMap<String, String> seniorPeriod = new HashMap<String, String>();
        seniorPeriod.put("id", ApiErrorCode.MIDDLE_SCHOOL);
        seniorPeriod.put("name", "中学");
        seniorPeriod.put("pid", "");
        seniorPeriod.put("tid", "1001");
        arr2.put(seniorPeriod);
        def2.put(seniorPeriod);

        // 学科
        ArrayList<HashMap<String, String>> seniorRelation = SeniorSimulatioDataBase.getRelationByMiddle();
        for (HashMap<String, String> relation : seniorRelation) {
            relation.put("tid", "1002");
            relation.put("pid", "");
            arr2.put(relation);
        }

        JSONObject seniorList2 = new JSONObject();
        seniorList2.put("lab", label);
        seniorList2.put("data", arr2);
        seniorList2.put("phase", ApiErrorCode.MIDDLE_SCHOOL);
        seniorList2.put("def", def2);

        JSONObject data = new JSONObject();
        data.put("data", seniorList2);
        String ret = ApiErrorCode.echoClassOk(data);
        return ret;
    }

    /**
     * 初始化请求数据
     *
     * @throws Exception
     */
    public static void initSimulation() throws Exception {
        Jedis redis = null;
        try {
            String seniorSimulation = constructSimulation();
            if (StringUtils.isEmpty(seniorSimulation)) {
                m_logger.debug("initSeniorSimulation fail,seniorSimulation length 0");
                return;
            }
            redis = RedisBaseUtil.getBaseRedis().getResource();
            RedisBaseUtil.setByVersion(REDIS_SENIORSIMULATION_KEY, seniorSimulation,redis);
        } catch (Exception e) {
            m_logger.debug("initSeniorSimulation fail" + e.getCause());
            if (redis != null) {
                RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
            }
            throw e;
        } finally {
            RedisBaseUtil.getBaseRedis().returnResource(redis);
        }
    }

    /**
     * 获取数据
     *
     * @return
     * @throws Exception
     */
    public static String getSimulation() throws Exception {
        Jedis redis = null;
        String seniorSimulation = null;
        try {
            redis = RedisBaseUtil.getBaseRedis().getResource();
            seniorSimulation = RedisBaseUtil.getByVersion(REDIS_SENIORSIMULATION_KEY,redis);
        } catch (Exception e) {
            m_logger.debug("initSeniorSimulation fail" + e.getCause());
            if (redis != null) {
                RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
            }
            throw e;
        } finally {
            RedisBaseUtil.getBaseRedis().returnResource(redis);
        }
        return seniorSimulation;
    }
}