package com.hq.api.subapps.examination;


import com.hq.api.subapps.common.ConstanQuantity;
import com.hq.api.subapps.common.SharMethod;
import com.hq.api.utils.ApiErrorCode;
import com.hq.api.utils.CheckParameterException;
import com.hq.api.utils.DatabaseManager;
import com.hq.dispatcher.Config;
import com.hq.utils.DoResponseUtil;
import com.hq.utils.RedisBaseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import redis.clients.jedis.Jedis;
import com.hq.api.utils.CheckParameters;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 获取期中期末试卷基础数据
 */
@WebServlet("/subapps/getExaminationPaper.do")
public class GetExaminatioPaper extends HttpServlet {
    private final static String REDIS_MIDTERM_KEY = "Examination.paper";
    private static final Logger m_logger = Logger.getLogger(GetExaminatioPaper.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            // session校验
            Map<String, String> sessionInfo = DatabaseManager.getSession(request);


            String grade = sessionInfo.get("grade");
            String ret = getMidtermPaper();
            JSONObject obj = new JSONObject(ret);
            JSONArray jsonarrayGrade  =  obj.getJSONArray("data").getJSONObject(0).getJSONArray("def");
            JSONArray  jsonarrayMiddle  =  obj.getJSONArray("data").getJSONObject(1).getJSONArray("def");
//            JSONArray  jsonarrayClassify  =  obj.getJSONArray("data").getJSONObject(1).getJSONArray("data");

//            HashMap<String,String> midtype = SharMethod.getCommonMap("80","学业测试","","1002");
            HashMap<String,String> graClassdef = SharMethod.getCommonMap("50","月考试卷","","1002");
            HashMap<String,String> midClassdef = SharMethod.getCommonMap("37","月考试卷","","1002");
            jsonarrayGrade.put(graClassdef);
            jsonarrayMiddle.put(midClassdef);

            HashMap<String, String> preNature = null;
            if(Arrays.asList(ConstanQuantity.JuniorGrade).contains(grade)){
                preNature = SharMethod.getCommonMap(ApiErrorCode.GRADE_SCHOOL,SharMethod.schoolnameStr[0],"",SharMethod.tidStr[0]);
                jsonarrayGrade.put(preNature);
            }else if(Arrays.asList(ConstanQuantity.MiddleGrade).contains(grade)){
                preNature = SharMethod.getCommonMap(ApiErrorCode.MIDDLE_SCHOOL,SharMethod.schoolnameStr[1],"",SharMethod.tidStr[0]);
                jsonarrayMiddle.put(preNature);
//                jsonarrayClassify.put(midtype);
            }else{
                //
            }
            String json = ApiErrorCode.echoClassOk(obj);
            // 数据压缩
            DoResponseUtil.ResponseWrite(request, response, json);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            return;

        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR, ce));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s", "system exception=", e), e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            return;
        }
    }

    /**
     * 构造数据
     *
     * @return
     * @throws Exception
     */
    public static String constructMidtermPaper() throws Exception {
        JSONArray label = new JSONArray();// 标签
        JSONArray arr2 = new JSONArray();// 中学库
        JSONArray arr1 = new JSONArray();// 小学库
        JSONArray def1 = new JSONArray();// 小学默认
        JSONArray def2 = new JSONArray();// 中学默认

        // 标签
        for (int i = 0; i < ConstanQuantity.strLabelName.length; i++) {

                HashMap<String, Object> relation = new HashMap<>();
                relation.put("id", i + 1001);
                relation.put("name", ConstanQuantity.strLabelName[i]);
                relation.put("pid", "");
                relation.put("tid", ConstanQuantity.strlabelTid[i]);
                relation.put("sid", i + 1);
                label.put(relation);

        }

        // 小学学段
        HashMap<String, String> juniorPeriod = new HashMap<String, String>();
        juniorPeriod.put("id", ApiErrorCode.GRADE_SCHOOL);
        juniorPeriod.put("name", "小学");
        juniorPeriod.put("pid", "");
        juniorPeriod.put("tid", "1001");
        arr1.put(juniorPeriod);
//        // 小学类型
        String[] strstrCategoryName1 = { "期中试卷", "期末试卷", "月考试卷"};
        String[] strstrCategoryId1 = { "8", "9", "50"};
        for (int i = 0; i < strstrCategoryName1.length; i++) {
            HashMap<String, Object> relation = new HashMap<>();
            relation.put("id", strstrCategoryId1[i]);
            relation.put("name", strstrCategoryName1[i]);
            relation.put("pid", "");
            relation.put("tid", "1002");
            arr1.put(relation);
//            if (relation.get("name").equals("月考试卷")) {
//				def1.put(relation);
//			}
        }

        // 小学关系数据
        ArrayList<HashMap<String, String>> JuniorRelation = ExaminatioDataBase.getRelationByJunior();
        for (int i = 0; i < JuniorRelation.size(); i++) {
            HashMap<String, String> relation = JuniorRelation.get(i);
            if (relation.get("cid").toString().equals("0")) {
                relation.put("tid", "1003");
                relation.put("pid", "");
            }
            if (relation.get("cid").toString().equals("1")) {
                relation.put("tid", "1004");
            }
            if (relation.get("cid").toString().equals("2")) {
                relation.put("tid", "1005");
            }
            relation.remove("cid");
            arr1.put(relation);
        }
        // 数据组装
        JSONObject juniorData = new JSONObject();
        juniorData.put("data", arr1);
        juniorData.put("lab", label);
        juniorData.put("phase", ApiErrorCode.GRADE_SCHOOL);
        juniorData.put("def", def1);

        // 中学学段
        HashMap<String, String> middlePeriod = new HashMap<String, String>();
        middlePeriod.put("id", ApiErrorCode.MIDDLE_SCHOOL);
        middlePeriod.put("name", "中学");
        middlePeriod.put("pid", "");
        middlePeriod.put("tid", "1001");
        arr2.put(middlePeriod);
//        // 中学类型
        String[] strstrCategoryName2 = { "月考试卷", "期中考试", "期末试卷"};
        String[] strstrCategoryId2 = { "37", "8", "9"};
        for (int i = 0; i < strstrCategoryName2.length; i++) {
            HashMap<String, Object> relation = new HashMap<>();
            relation.put("id", strstrCategoryId2[i]);
            relation.put("name", strstrCategoryName2[i]);
            relation.put("pid", "");
            relation.put("tid", "1002");
            arr2.put(relation);
//            if(relation.get("name").equals("月考试卷")){
//				def2.put(relation);
//			}

        }
        // 中学关系数据
        ArrayList<HashMap<String, String>> MiddleRelation = ExaminatioDataBase.getRelationByMiddle();
        for (int i = 0; i < MiddleRelation.size(); i++) {
            HashMap<String, String> relation = MiddleRelation.get(i);
            if (relation.get("cid").toString().equals("0")) {
                relation.put("tid", "1003");
                relation.put("pid", "");
            }
            if (relation.get("cid").toString().equals("1")) {
                relation.put("tid", "1004");
            }
            if (relation.get("cid").toString().equals("2")) {
                relation.put("tid", "1005");
            }
            relation.remove("cid");
            arr2.put(relation);
        }
        // 数据组装
        JSONObject middleData = new JSONObject();
        middleData.put("lab", label);
        middleData.put("data", arr2);
        middleData.put("phase", ApiErrorCode.MIDDLE_SCHOOL);
        middleData.put("def", def2);
        // 数据组装
        JSONArray jsonarray = new JSONArray();
        jsonarray.put(juniorData);
        jsonarray.put(middleData);

        JSONObject Data = new JSONObject();
        Data.put("data", jsonarray);
        String ret = ApiErrorCode.echoClassOk(Data);
        return ret;
    }

    /**
     * 初始化请求数据
     *
     * @throws Exception
     */
    public static void initMidtermPaper() throws Exception {
        Jedis redis = null;
        try {
            String examinationPaper = constructMidtermPaper();
            if (StringUtils.isEmpty(examinationPaper)) {
                m_logger.debug("initExaminationPaper fail,examinationPaper length 0");
                return;
            }
            redis = RedisBaseUtil.getBaseRedis().getResource();
            RedisBaseUtil.setByVersion(REDIS_MIDTERM_KEY,examinationPaper,redis);
        } catch (Exception e) {
            m_logger.debug("initExaminationPaper fail" + e.getCause());
            if (redis != null) {
                RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
            }
            throw e;
        } finally {
            RedisBaseUtil.getBaseRedis().returnResource(redis);
        }
    }

    /**
     * 获取数据
     *
     * @return
     * @throws Exception
     */
    public static String getMidtermPaper() throws Exception {
        Jedis redis = null;
        String examinationPaper = null;
        try {
            redis = RedisBaseUtil.getBaseRedis().getResource();
            examinationPaper = RedisBaseUtil.getByVersion(REDIS_MIDTERM_KEY,redis);
        } catch (Exception e) {
            m_logger.debug("initExaminationPaper fail" + e.getCause());
            if (redis != null) {
                RedisBaseUtil.getBaseRedis().returnBrokenResource(redis);
            }
            throw e;
        } finally {
            RedisBaseUtil.getBaseRedis().returnResource(redis);
        }
        return examinationPaper;
    }
}
