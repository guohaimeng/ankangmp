package com.hq.api.subapps.examination;


import com.hq.api.subapps.common.ResourcDatabase;
import com.hq.api.utils.ApiErrorCode;
import com.hq.api.utils.CheckParameterException;
import com.hq.api.utils.DatabaseManager;
import com.hq.dispatcher.Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import com.hq.api.utils.CheckParameters;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 月考试卷，期中试卷，期末试卷列表
 */
@WebServlet("/subapps/getExaminationList.do")
public class GetExaminatioList extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(GetExaminatioList.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        JSONObject ResourceObj = null;
        try {
            CheckParameters check = new CheckParameters(request.getParameterMap());
            String phase = "";//学段
            String classify = "";//分类
            String reid = "";//关系id
            String sortby = "";//排序
            String keyword = "";//关键字
            String pagesize = "";//分页大小
            String pagenum = "";//分页页码
            check.addParameter("phase", CheckParameters.paraType.EXP, "^[1,2]$");
            phase = check.get("phase").toString();
            if(phase.equals(ApiErrorCode.MIDDLE_SCHOOL)){
                check.addParameter("classify", CheckParameters.paraType.EXP,"^(0|[8,9]|3[7])$");
            }else{
                check.addParameter("classify", CheckParameters.paraType.EXP,"^(0|[8,9]|5[0])$");
            }
            classify = check.get("classify").toString();
            if (!StringUtils.isEmpty(request.getParameter("reid"))) {
                check.addParameter("reid", CheckParameters.paraType.STRING, 1, 8);
                reid = check.get("reid").toString();
            }

            if (!StringUtils.isEmpty(request.getParameter("sortby"))) {
                check.addParameter("sortby", CheckParameters.paraType.EXP, "^[0-3]$"); //0 默认 1上传时间 2 星级 3 下载量
                sortby = check.get("sortby").toString();
            } else {
                sortby = "0";
            }
            if (!StringUtils.isEmpty(request.getParameter("keyword"))) {
                check.addParameter("keyword", CheckParameters.paraType.EXP, "[a-zA-Z0-9\u4e00-\u9fa5]{1,30}");
                keyword = check.get("keyword").toString();
            }

            if (!StringUtils.isEmpty(request.getParameter("pagesize"))) {
                check.addParameter("pagesize", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
                pagesize = check.get("pagesize").toString();
            }

            if (!StringUtils.isEmpty(request.getParameter("pagenum"))) {
                check.addParameter("pagenum", CheckParameters.paraType.EXP, Config.getInstance().getString("params_page"));
                pagenum = check.get("pagenum").toString();
            }

            //session校验
            Map<String, String> sessionInfo = DatabaseManager.getSession(request);
            HashMap<String, String> condition = new HashMap<>();
            condition.put("classify", classify);
            condition.put("sortby", sortby);
            condition.put("keyword", keyword);
            condition.put("pagenum", pagenum);
            condition.put("pagesize", pagesize);

            HashMap<String, Object> Resource = null;
            Resource = getdata(phase, condition, reid);
            ArrayList<HashMap<String, String>> res = (ArrayList<HashMap<String, String>>) Resource.get("res");
            res = ResourcDatabase.dealResExtname(phase, res);

            ResourceObj = new JSONObject();
            ResourceObj.put("data", res);
            ResourceObj.put("count", Resource.get("count"));
            String ret = ApiErrorCode.echoClassOk(ResourceObj);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            return;
        } catch (CheckParameterException ce) {
            String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
            m_logger.info(String.format("OUTPUT ret_code=%s %s", ApiErrorCode.PARAMETER_ERROR, ce));
            response.getWriter().write(ret);
            return;
        } catch (Exception e) {
            m_logger.error(String.format("FAILED params=%s %s", "system exception=", e), e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            return;
        }
    }

    //获取子应用资源
    public static HashMap<String, Object> getdata(String phase, HashMap<String, String> condition, String reid) throws SQLException {
        HashMap<String, Object> Resourcemap = null;
        HashMap<String, Object> resmap = new HashMap<String, Object>();
        ArrayList<HashMap<String, String>> res = new ArrayList<HashMap<String, String>>();
        String count = "0";
        HashMap<String, String> relationmap = ExaminatioDataBase.getRelationById(reid, phase);
        if (relationmap != null && relationmap.size() > 0) {
            condition.put("subid", relationmap.get("subid"));
            condition.put("grid", relationmap.get("grid"));
            condition.put("veid", relationmap.get("veid"));
        }
        if (phase.equals(ApiErrorCode.GRADE_SCHOOL)) {//小学
            Resourcemap = ExaminatioDataBase.getTestWarehouseByJunior(condition);
            res = (ArrayList<HashMap<String, String>>) Resourcemap.get("res");
            count = (String) Resourcemap.get("count");
        } else if (phase.equals(ApiErrorCode.MIDDLE_SCHOOL)) {//中学
            Resourcemap = ExaminatioDataBase.getTestWarehouseByMiddle(condition);
            res = (ArrayList<HashMap<String, String>>) Resourcemap.get("res");
            count = (String) Resourcemap.get("count");
        } else {
            //
        }
        resmap.put("res", res);
        resmap.put("count", count);
        return resmap;

    }
}
