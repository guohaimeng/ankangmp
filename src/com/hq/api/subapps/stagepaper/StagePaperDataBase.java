package com.hq.api.subapps.stagepaper;

import com.hq.api.subapps.common.ConstanQuantity;
import com.hq.api.subapps.datautils.HQPrimDataSqlServeBaseUtil;
import com.hq.api.subapps.datautils.HqdbDataSqlServeBaseUtil;
import com.hq.api.utils.ApiErrorCode;
import com.hq.dispatcher.Config;
import org.apache.commons.lang.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 2018/9/20.
 */
public class StagePaperDataBase {

    /**
     * 查询小学试卷资源
     * @return
     * @throws SQLException
     */
    public static HashMap<String,Object> getTestWarehouseByJunior(HashMap<String,String> relation) throws SQLException{
        int pageSizeInt=10;
        int pagenumInt=1;
        HashMap<String,Object> map = new HashMap<String,Object>();

        String sql="select 1 as phase, Id roid,title,ResourceUrl downurl,ResourseSize size,stars,DATEDIFF( SECOND, '1970-01-01 08:00:00',CreatTime) createtime from fs_channel_zyxz where hidden = 1";
        String sqlcount="select count(id) from fs_channel_zyxz where hidden = 1";
        ArrayList<Object> params = new ArrayList<>();
        int  classify=0;
        if(relation.get("classify").equals(ApiErrorCode.GRADE_SCHOOL)){
            classify=50;
        }else if(relation.get("classify").equals(ApiErrorCode.MIDDLE_SCHOOL)){
            classify=9;
        }else if(relation.get("classify").equals(ApiErrorCode.HIGH_SCHOOL)){
            classify=8;
        }else{
            //
        }
        sql+=" and ClassID ="+classify;
        sqlcount+=" and ClassID ="+classify;
        if(!StringUtils.isEmpty(relation.get("subid")) && !relation.get("subid").equals("0")){
            sql+=" and SubjectId = ? ";
            sqlcount+=" and SubjectId = ? ";
            params.add(relation.get("subid"));
        }
        if(!StringUtils.isEmpty(relation.get("grid")) && !relation.get("grid").equals("0")){
            sql+=" and GradeId = ? ";
            sqlcount+=" and GradeId = ? ";
            params.add(relation.get("grid"));
        }
        if(!StringUtils.isEmpty(relation.get("veid")) && !relation.get("veid").equals("0")){
            sql+=" and VersionId = ? ";
            sqlcount+=" and VersionId = ? ";
            params.add(relation.get("veid"));
        }

        if(!StringUtils.isEmpty(relation.get("keyword"))){
            sql+=  " and (title like '%"+ relation.get("keyword") + "%' or Tags like '%"+ relation.get("keyword") + "%' )  ";
            sqlcount+=  " and (title like '%"+ relation.get("keyword") + "%' or Tags like '%"+ relation.get("keyword") + "%' )  ";
        }

//        sql+=" and (title not like '%"+ ConstanQuantity.NoAnswer + "%' and Tags not like '%"+ ConstanQuantity.NoAnswer + "%' )  and (title not like '%"+ ConstanQuantity.NoProvince + "%' and Tags not like '%"+ ConstanQuantity.NoProvince  + "%' )  and  convert(varchar(100),CreatTime,23)>='2015-08-15' ";
//        sqlcount+=" and (title not like '%"+ ConstanQuantity.NoAnswer + "%' and Tags not like '%"+ ConstanQuantity.NoAnswer + "%' ) and (title not like '%"+ ConstanQuantity.NoProvince + "%' and Tags not like '%"+ ConstanQuantity.NoProvince  + "%' )  and  convert(varchar(100),CreatTime,23)>='2015-08-15' ";

        sql+=" and title not like '%"+ ConstanQuantity.NoAnswer + "%'     and  convert(varchar(100),CreatTime,23)>='2015-08-15' ";
        sqlcount+=" and title not like '%"+ ConstanQuantity.NoAnswer + "%'    and  convert(varchar(100),CreatTime,23)>='2015-08-15' ";

        switch (relation.get("sortby")){
            case "0":
                sql += " order by CreatTime desc";
                break;
            case "1":
                sql += " order by CreatTime desc";
                break;
            case "2":
                sql += " order by stars desc,Id desc";
                break;
            case "3":
                sql += " order by downCount desc,Id desc";
                break;
            default:
                sql += " order by CreatTime desc";
        }

        if(StringUtils.isEmpty(relation.get("pagenum")) || Integer.parseInt(relation.get("pagenum"))<1){
            pagenumInt = 1;
        }else{
            pagenumInt = Integer.parseInt(relation.get("pagenum"));
        }

        if(StringUtils.isEmpty(relation.get("pagesize"))){
            pageSizeInt = Config.getInstance().getInt("system_pageSize",10);
        }else {
            pageSizeInt = Integer.parseInt(relation.get("pagesize"));
        }

        sql += " OFFSET " + pageSizeInt*(pagenumInt-1)  +" ROWS ";
        sql += " FETCH NEXT " + pageSizeInt + " ROWS ONLY ";
        String rescount = HQPrimDataSqlServeBaseUtil.getOneColumnByRow(sqlcount, params);
        ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>();
        if(!rescount.equals("0")){
            res = HQPrimDataSqlServeBaseUtil.querySql(sql, params);
        }
        map.put("count",rescount);
        map.put("res", res);

        return map;
    }


    /**
     * 查询中学试卷资源
     * @return
     * @throws SQLException
     */
    public static HashMap<String,Object> getTestWarehouseByMiddle(HashMap<String,String> relation) throws SQLException{
        int pageSizeInt=10;
        int pagenumInt=1;
        HashMap<String,Object> map = new HashMap<String,Object>();

        String sql="select 2 as phase, id roid,topic title,stars stars,url1 downurl,fsize size,DATEDIFF( SECOND, '1970-01-01 08:00:00',tim)createtime from res_data where hidden = 1";
        String sqlcount="select count(id) from res_data where hidden = 1";
        ArrayList<Object> params = new ArrayList<>();
        int classify=0;
        if(relation.get("classify").equals(ApiErrorCode.GRADE_SCHOOL)){
            classify=37;
        }else if(relation.get("classify").equals(ApiErrorCode.MIDDLE_SCHOOL)){
            classify=8;
        }else if(relation.get("classify").equals(ApiErrorCode.HIGH_SCHOOL)){
            classify=9;
        }else{
            //
        }
        sql+=" and c_id ="+classify;
        sqlcount+=" and c_id ="+classify;
        if(!StringUtils.isEmpty(relation.get("subid")) && !relation.get("subid").equals("0")){
            sql+=" and object_id = ? ";
            sqlcount+=" and object_id = ? ";
            params.add(relation.get("subid"));
        }
        if(!StringUtils.isEmpty(relation.get("grid")) && !relation.get("grid").equals("0")){
            sql+=" and s_id = ? ";
            sqlcount+=" and s_id = ? ";
            params.add(relation.get("grid"));
        }
        if(!StringUtils.isEmpty(relation.get("veid")) && !relation.get("veid").equals("0")){
            sql+=" and res_bb = ? ";
            sqlcount+=" and res_bb = ? ";
            params.add(relation.get("veid"));
        }

        if(!StringUtils.isEmpty(relation.get("keyword"))){
            sql+=  " and (topic like '%"+ relation.get("keyword") + "%' and keyes like '%"+ relation.get("keyword") + "%' )  ";
            sqlcount+=  " and (topic like '%"+ relation.get("keyword") + "%' and keyes like '%"+ relation.get("keyword") + "%' )  ";
        }

//        sql+=" and (topic not like '%"+ ConstanQuantity.NoAnswer + "%' and keyes not like '%"+ ConstanQuantity.NoAnswer + "%' )  and (topic not like '%"+ ConstanQuantity.NoProvince + "%' and keyes not like '%"+ ConstanQuantity.NoProvince  + "%' )  and  convert(varchar(100),[tim],23)>='2015-08-15' ";
//        sqlcount+=" and (topic not like '%"+ ConstanQuantity.NoAnswer + "%' and keyes not like '%"+ ConstanQuantity.NoAnswer + "%' ) and (topic not like '%"+ ConstanQuantity.NoProvince + "%' and keyes not like '%"+ ConstanQuantity.NoProvince  + "%' )  and  convert(varchar(100),[tim],23)>='2015-08-15' ";

        sql+=" and  topic not like '%"+ ConstanQuantity.NoAnswer + "%'   and (topic not like '%"+ ConstanQuantity.NoProvince + "%' and keyes not like '%"+ ConstanQuantity.NoProvince  + "%' )  and  convert(varchar(100),[tim],23)>='2015-08-15' ";
        sqlcount+=" and  topic not like '%"+ ConstanQuantity.NoAnswer + "%'  and (topic not like '%"+ ConstanQuantity.NoProvince + "%' and keyes not like '%"+ ConstanQuantity.NoProvince  + "%' )  and  convert(varchar(100),[tim],23)>='2015-08-15' ";


        switch (relation.get("sortby")){
            case "0":
                sql += " order by tim desc";
                break;
            case "1":
                sql += " order by tim desc";
                break;
            case "2":
                sql += " order by stars desc,id desc";
                break;
            case "3":
                sql += " order by counter desc,id desc";
                break;
            default:
                sql += " order by tim desc";
        }
        if(StringUtils.isEmpty(relation.get("pagenum")) || Integer.parseInt(relation.get("pagenum"))<1){
            pagenumInt = 1;
        }else{
            pagenumInt = Integer.parseInt(relation.get("pagenum"));
        }


        if(StringUtils.isEmpty(relation.get("pagesize"))){
            pageSizeInt = Config.getInstance().getInt("system_pageSize",10);
        }else {
            pageSizeInt = Integer.parseInt(relation.get("pagesize"));
        }

        sql += " OFFSET " + pageSizeInt*(pagenumInt-1)  +" ROWS ";
        sql += " FETCH NEXT " + pageSizeInt + " ROWS ONLY ";
        String rescount = HqdbDataSqlServeBaseUtil.getOneColumnByRow(sqlcount, params);
        ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>();
        if(!rescount.equals("0")){
            res = HqdbDataSqlServeBaseUtil.querySql(sql, params);
        }
        map.put("count",rescount);
        map.put("res", res);

        return map;
    }



    /**
     * 根据ID查询中小学关系学科，版本，年级
     * @reurn
     * @throws SQLException
     */
    public static HashMap<String, String> getRelationById(String reid, String phase) throws SQLException{
        String sql="";
        ArrayList<Object> params = new ArrayList<>();
        params.add(reid);
        HashMap<String,String>  datamap = null;
        if(phase.equals(ApiErrorCode.GRADE_SCHOOL)){
            sql="select SubjectId as subid,GradeId as grid,VersionId as veid from fs_channel_relation where Id=?";
            datamap =  HQPrimDataSqlServeBaseUtil.getOneRow(sql, params);
        }else if (phase.equals(ApiErrorCode.MIDDLE_SCHOOL)){
            sql="select myocb_star star,myocb_parent parent,object_id subid,s_id grid,bb_id veid  from hq_ocb where myocb_id=?";
            datamap =  HqdbDataSqlServeBaseUtil.getOneRow(sql, params);
        }else{
            //
        }
        return datamap;
    }






    /**
     * 查询小学学科，年级，版本关系数据
     * @reurn
     * @throws SQLException
     */
    public static ArrayList<HashMap<String,String>> getRelationByJunior() throws SQLException{
        String sql="select ID id,relationName name,Pid pid,ClassId cid from fs_channel_relation order by VersionId asc,GradeId asc";
        ArrayList<Object> params = new ArrayList<>();
        return HQPrimDataSqlServeBaseUtil.querySql(sql, params);
    }




    /**
     * 查询中学学科，年级，版本关联数据
     * @reurn
     * @throws SQLException
     */
    public static ArrayList<HashMap<String,String>> getRelationByMiddle() throws SQLException {
        String sql="select myocb_id id,myocb_f pid,myocb_name name,myocb_star cid from hq_ocb where myocb_hidden = 0 and myocb_id not in (90,93,465)";
        ArrayList<Object> params = new ArrayList<>();
        return HqdbDataSqlServeBaseUtil.querySql(sql, params);
    }





}
