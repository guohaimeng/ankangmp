package com.hq.api.subapps.common;


import com.hq.api.utils.ApiErrorCode;
import com.hq.utils.DoResponseUtil;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/1/17.
 */
public class SharMethod {
    public static final String[] schoolnameStr = {"小学","中学"};
    public static final String[] tiernameStr = {"id","name","pid","tid"};
    public static final String[] tidStr = {"1001","1002","1003","1004","1005"};

    private static final Logger m_logger = Logger.getLogger(SharMethod.class);
    //处理参数，获取id
    public static  String dealwithparam(ArrayList<HashMap<String, String>> list){
        String str  = "";
        if (list!=null && list.size()>0){
            for(int i=0;i<list.size();i++){
                if(i==0){
                    str+=""+list.get(i).get(tiernameStr[0]);
                }else{
                    str+=","+list.get(i).get(tiernameStr[0]);
                }
            }
        }
        return str;
    }
    //获取基础数据默认教学属性
    public static void getDefaultTeachingAttribute(HttpServletRequest request, HttpServletResponse response, Map<String, String> sessionInfo, String ret, String  applytype, HashMap<String,String> graClassdef, HashMap<String,String> midClassdef) throws SQLException, JSONException, IOException {
        String ssphase = sessionInfo.get("phase");
        // 获取用户学段
        HashMap<String, String> preNature = null;
        HashMap<String, String> preData = ResourcDatabase.getSubjectPre(sessionInfo.get("uid"));
        // 获取教学属性
        HashMap<String, String> teachingData = ResourcDatabase.getTeachingattr(sessionInfo.get("uid"));
        String reid = teachingData.get("subid") + "," + teachingData.get("grid") + "," + teachingData.get("veid");
        ArrayList<HashMap<String, String>> correID = ResourcDatabase.getCorreID(reid);
        JSONObject obj = new JSONObject(ret);
        JSONArray jsonarrayGrade  =  obj.getJSONArray("data").getJSONObject(0).getJSONArray("def");
        JSONArray jsonarrayMiddle  =  obj.getJSONArray("data").getJSONObject(1).getJSONArray("def");
        //无教学属性时，根据个人信息的学段去匹配第一层
        if(preData == null || preData.size() == 0){
            if(ApiErrorCode.GRADE_SCHOOL.equals(ssphase) ){
                preNature = getCommonMap(ApiErrorCode.GRADE_SCHOOL,schoolnameStr[0],"",tidStr[0]);
                jsonarrayGrade.put(preNature);
                if(applytype.equals(ApiErrorCode.DEFAULT_TYPE)){
                    jsonarrayGrade.put(graClassdef);
                    jsonarrayMiddle.put(midClassdef);
                }
            }else if(ApiErrorCode.MIDDLE_SCHOOL.equals(ssphase) || ApiErrorCode.HIGH_SCHOOL.equals(ssphase)){
                preNature = getCommonMap(ApiErrorCode.MIDDLE_SCHOOL,schoolnameStr[1],"",tidStr[0]);
                jsonarrayMiddle.put(preNature);
                if(applytype.equals(ApiErrorCode.DEFAULT_TYPE)){
                    jsonarrayGrade.put(graClassdef);
                    jsonarrayMiddle.put(midClassdef);
                }
            }else{
                //
            }
        }else{
            //有教学属性时，根据教学属性的学段去匹配第一层
            if(ApiErrorCode.GRADE_SCHOOL.equals(preData.get("phase"))){
                handleDefault(preData.get("phase"),jsonarrayGrade,correID,applytype);
                if(applytype.equals(ApiErrorCode.DEFAULT_TYPE)){
                    //applytype为1时，中小学默认数据增加类别
                    jsonarrayGrade.put(graClassdef);
                    jsonarrayMiddle.put(midClassdef);
                }
            }else if(ApiErrorCode.MIDDLE_SCHOOL.equals(preData.get("phase")) || ApiErrorCode.HIGH_SCHOOL.equals(preData.get("phase"))){
                handleDefault(preData.get("phase"),jsonarrayMiddle,correID,applytype);
                //applytype为1时，中小学默认数据增加类别
                if(applytype.equals(ApiErrorCode.DEFAULT_TYPE)){
                    jsonarrayGrade.put(graClassdef);
                    jsonarrayMiddle.put(midClassdef);
                }
            }else{
                //
            }

        }
        String json = ApiErrorCode.echoClassOk(obj);
        // 数据压缩
        DoResponseUtil.ResponseWrite(request, response, json);
        m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
        return;
    }



    private static void handleDefault(String phase, JSONArray commjsonarray, ArrayList<HashMap<String, String>> correID, String applytype) throws SQLException {
        HashMap<String, String> preNature = null;
        //学科
        HashMap<String, String> subject = null;
        // 年级
        HashMap<String, String> grade = null;
        // 版本
        HashMap<String, String> version = null;
        //学科默认
        HashMap<String, String> subjectDef = null;
        //年级默认
        HashMap<String, String> gradeDef = null;
        //版本默认
        HashMap<String, String> versionDef = null;
        // 存放对应ID
        ArrayList<String> depositID = new ArrayList<>();
        String correIDStr = "";
        if(correID == null || correID.size() == 0){
            // 不处理
        }else{
            if(ApiErrorCode.GRADE_SCHOOL.equals(phase)){
                preNature = getCommonMap(ApiErrorCode.GRADE_SCHOOL,schoolnameStr[0],"",tidStr[0]);
                for (int i = 0; i < correID.size(); i++) {
                    correIDStr = correID.get(i).get(tiernameStr[0]);
                    if(correIDStr != null){
                        String[] str = correIDStr.split("\\,");
                        depositID.add(str[0]);
                    }
                }
                if(depositID.size()>=1){
                    subject = ResourcDatabase.getPriSubject(depositID.get(0));
                }
                if(depositID.size()>=2){
                    grade = ResourcDatabase.getPriGrade(depositID.get(1), subject.get(tiernameStr[0]));
                }
                if(depositID.size()>=3){
                    version = ResourcDatabase.getPriVersion(depositID.get(2), grade.get(tiernameStr[0]));
                }

            }else if(ApiErrorCode.MIDDLE_SCHOOL.equals(phase) || ApiErrorCode.HIGH_SCHOOL.equals(phase)){
                preNature = getCommonMap(ApiErrorCode.MIDDLE_SCHOOL,schoolnameStr[1],"",tidStr[0]);
                for (int i = 0; i < correID.size(); i++) {
                    correIDStr = correID.get(i).get(tiernameStr[0]);
                    if(correIDStr != null){
                        String[] str = correIDStr.split("\\,");
                        depositID.add(str[1]);
                    }
                }
                if(depositID.size()>=1){
                    subject = ResourcDatabase.getMiddSubject(depositID.get(0));
                }
                if(depositID.size()>=2){
                    grade = ResourcDatabase.getMiddGrade(depositID.get(1), subject.get(tiernameStr[0]));
                }
                if(depositID.size()>=3){
                    version = ResourcDatabase.getMiddVersion(depositID.get(2), grade.get(tiernameStr[0]));
                }

            }else{
                //

            }
            //默认有类别，学科年级版本的tid相应低一层
            if(subject !=null && subject.size()>0){
                if(applytype.equals(ApiErrorCode.DEFAULT_STATUS)){
                    subjectDef = getCommonMap(subject.get(tiernameStr[0]),subject.get(tiernameStr[1]),"",tidStr[1]);
                }else if(applytype.equals(ApiErrorCode.DEFAULT_TYPE)){
                    //applytype为1时，中小学默认数据增加类别，且学科相应tid增加一级
                    subjectDef = getCommonMap(subject.get(tiernameStr[0]),subject.get(tiernameStr[1]),"",tidStr[2]);
                }else{

                }
            }
            if(grade !=null && grade.size()>0){
                if(applytype.equals(ApiErrorCode.DEFAULT_STATUS)){
                    gradeDef = getCommonMap(grade.get(tiernameStr[0]),grade.get(tiernameStr[1]),subject.get(tiernameStr[0]),tidStr[2]);
                }else if(applytype.equals(ApiErrorCode.DEFAULT_TYPE)){
                    //applytype为1时，中小学默认数据增加类别，且年级相应tid增加一级
                    gradeDef = getCommonMap(grade.get(tiernameStr[0]),grade.get(tiernameStr[1]),subject.get(tiernameStr[0]),tidStr[3]);
                }else{
                    //
                }
            }
            if(version !=null && version.size()>0){
                if(applytype.equals(ApiErrorCode.DEFAULT_STATUS)){
                    versionDef = getCommonMap(version.get(tiernameStr[0]),version.get(tiernameStr[1]),grade.get(tiernameStr[0]),tidStr[3]);
                }else if(applytype.equals(ApiErrorCode.DEFAULT_TYPE)){
                    //applytype为1时，中小学默认数据增加类别，且版本相应tid增加一级
                    versionDef = getCommonMap(version.get(tiernameStr[0]),version.get(tiernameStr[1]),grade.get(tiernameStr[0]),tidStr[4]);
                }else{

                }
            }
            commjsonarray.put(preNature);
            commjsonarray.put(subjectDef);
            commjsonarray.put(gradeDef);
            commjsonarray.put(versionDef);
        }

    }
    //子应用基础数据组装数据公共方法
    public static  HashMap<String,String>  getCommonMap(String id,String name,String pid,String tid){
        HashMap<String,String>  commMap = new HashMap<String,String>();
        commMap.put(tiernameStr[0],id);
        commMap.put(tiernameStr[1],name);
        commMap.put(tiernameStr[2],pid);
        commMap.put(tiernameStr[3],tid);
        return commMap;
    }

}
