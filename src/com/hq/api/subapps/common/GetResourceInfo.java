package com.hq.api.subapps.common;


import com.hq.api.utils.ApiErrorCode;
import com.hq.api.utils.CheckParameterException;
import com.hq.api.utils.CheckParameters;
import com.hq.api.utils.DatabaseManager;
import com.hq.dispatcher.Config;
import com.hq.utils.aes.AESCodecBase16;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * 获取资源详情
 */
@WebServlet("/subapps/getResourceInfo.do")
public class GetResourceInfo extends HttpServlet {
	
	private static final Logger m_logger = Logger.getLogger(GetResourceInfo.class);
	
	private static final String MiddleSchoolHost="http://go.hengqian.com/upload/";
    private static final String PrimarySchoolHost="http://go.hengqian.com.cn/";
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
    		//参数校验
    		CheckParameters check = new CheckParameters(request.getParameterMap());
    		check.addParameter("roid", CheckParameters.paraType.STRING,1,36);
    		check.addParameter("phase", CheckParameters.paraType.EXP, "^[1,2]$");
    		
    		//session校验
    		Map<String, String> sessionInfo = DatabaseManager.getSession(request);

        	
        	Map<String, String> condition = ResourcDatabase.inquireResource(check.get("roid").toString(), check.get("phase").toString());
        	//检验资源是否存在
        	if(condition == null || condition.size() == 0){        		
        		String ret = ApiErrorCode.echoErr(ApiErrorCode.RES_NOT_EXIST );
				m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.RES_NOT_EXIST ));
				response.getWriter().write(ret);
				return;    
        	}
        	
        	HashMap<String,String> res = new HashMap<>();
        	res.put("id", condition.get("id"));
        	res.put("roid", condition.get("roid"));
        	res.put("phase", check.get("phase").toString());
        	res.put("title", condition.get("title"));
        	res.put("stars", condition.get("stars"));
        	
        	String downurl = null;
        	String viewurl = null;
        	String extname = null;
        	//获取资源详情        	
        	if(StringUtils.isEmpty(condition.get("downurl"))){
        		downurl = "";
        		viewurl = "";
        		extname = "";
        	}else {
        		extname= ResourcDatabase.getResExtname(condition.get("downurl"));
        		if(condition.get("downurl").indexOf("http://") >= 0 || condition.get("downurl").indexOf("https://") >= 0){
        			downurl = condition.get("downurl").trim();
            	}else if(check.get("phase").toString().equals("1")){
            		downurl = PrimarySchoolHost + condition.get("downurl").trim();
                }else{
                	downurl = MiddleSchoolHost + condition.get("downurl").trim();
            	}
        		 try {
                     String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
                     String accessToken = URLEncoder.encode(downurl,"UTF-8")+"*8fe49abfd9ab1d66*"+timestamp;
                     accessToken = AESCodecBase16.encrypt(accessToken, "69b48c7385c7p8e1");
                     viewurl = "https://view.hengqian.net/preView.htm?accessToken="+accessToken;
                 }catch (Exception e){
                     m_logger.error(" ERROR:get viewurl",e);
                     viewurl="";
                 }
        	}
        	
        	
        	res.put("downurl", downurl);        	
        	res.put("extname", extname);
        	res.put("viewurl", viewurl);
        	res.put("size", ResourcDatabase.getResFileSize(check.get("phase").toString(),condition.get("size"),extname));
        	res.put("createtime", condition.get("createtime"));
        	res.put("intro", condition.get("intro"));
        	
        	JSONObject resourceObj  = new JSONObject();
        	resourceObj.put("resourcedetail", new JSONObject(res));
            String ret = ApiErrorCode.echoClassOk(resourceObj);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            return; 
        	
    	 } catch(CheckParameterException ce){
    		m_logger.info(String.format("OUTPUT ret_code=%s %s",ApiErrorCode.PARAMETER_ERROR,ce));
    		response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR));
    		return;
		 } catch(Exception e){
			m_logger.error(String.format("getResourceInfo Exception=%s",e.getMessage()),e);
			response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
    		return;
    	 }
	}
}
