package com.hq.api.subapps.common;


public class ConstanQuantity {
	//小学年级  代表 一年级到六年级
	public static final String[] JuniorGrade = { "1",  "2", "3", "4","5","6" };
	//中学年级  代表 七年级到高三
	public static final String[] MiddleGrade = { "7",  "8", "9", "10","11","12" };


	// 同步备课与试卷资源子应用标签常量
	public static final String[] strLabelName = { "学段", "类别", "学科", "年级", "版本" };
	public static final String[] strlabelTid = { "phase", "classify", "subid", "grid", "veid" };

	// 月考试卷，期中试卷，期末试卷常量配置
	public static final String[] strStagePageName = { "学段", "学科", "年级", "版本" };
	public static final String[] strStagePageTid = { "phase", "subid", "grid", "veid" };




	// 真题资源与备考资源子应用地区常量
	public static final String[] strArea = { "不限", "北京", "上海", "天津", "重庆", "吉林", "辽宁", "河北", "山东", "山西", "江苏", "浙江",
			"河南", "安徽", "福建", "湖北", "湖南", "江西", "海南", "广东", "广西", "四川", "云南", "贵州", "陕西", "甘肃", "宁夏", "青海", "新疆", "西藏",
			"内蒙古", "黑龙江", "全国" };

	// 真题资源与备考资源子应用高考学科常量
	public static final String[] strSeniorSubject = { "语文", "数学", "物理", "化学", "英语", "地理", "历史", "政治", "生物", "文综",
			"理综" };
	public static final String[] strSeniorId = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "12", "14" };

	// 真题资源与备考资源子应用中考学科常量
	public static final String[] strHighSubject = { "语文", "数学", "物理", "化学", "英语", "地理", "历史", "政治", "生物", "理化", "政史" };
	public static final String[] strHighId = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "28", "29" };

	public static final String NoAnswer="无答案";
	public static final String NoProvince="陕西";


}
