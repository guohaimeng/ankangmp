/**
 * 发送验证码
 * POST
	phone（必填，string）：手机号，11位长，13、14、15、16、17、18，19开头
	type（必填，int） ： 1.绑定手机号  2.找回密码
 */
package com.hq.api.subapps.common;


import com.hq.api.utils.*;
import com.hq.dispatcher.Config;
import com.hq.dispatcher.ConfigKey;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.helper.StringUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 发送资源链接
 * @author Administrator
 *
 */
@WebServlet("/subapps/sendResource.do")
public class SendResource extends HttpServlet {
	private static final long serialVersionUID = -7414785058770121280L;
	//配置邮件发送地址
    private static final String [] mainStr  = Config.getInstance().getString("mail.user.list").split(",");
	private static final Logger m_logger = Logger.getLogger(SendResource.class);
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			//参数校验
			CheckParameters check = new CheckParameters(request.getParameterMap());
	    	check.addParameter("mail",CheckParameters.paraType.EXP,Config.getInstance().getString("params_email"));
			check.addParameter("downurl",CheckParameters.paraType.STRING,1,300);
			check.addParameter("title",CheckParameters.paraType.STRING,1,300);

			//session校验
			Map<String, String> selfInfo=DatabaseManager.getSession(request);

	    	String ret="";


			SimpleDateFormat sdfInput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String msg = "<table style=\"width:560px;\" cellspacing=\"0\" cellpadding=\"10\" border=\"0\" align=\"center\">" +
					"    <tbody>" +
					"        <tr>" +
					"            <td align=\"left\"><IMG SRC=\"http://images.hengqian.net/theme/Dream/img/temporary/logo.png\" WIDTH=\"140\" HEIGHT=\"34\" BORDER=\"0\"></td>" +
					"        </tr>" +
					"</table>" +
					"<table style=\"width:560px;\" cellspacing=\"0\" cellpadding=\"10\" bordercolor=\"#E5E5E5\" border=\"2\" align=\"center\">" +
					"    <tbody>" +
					"        <tr class=\"firstRow\">" +
					"            <td style=\"background-color: #FFFFFF; border-width: 10px; border-style: solid; border-color: #E5E5E5);line-height:40px;font-size: 14px\" align=\"left\">" +
					"尊敬的用户，您好！<br />&nbsp;&nbsp;&nbsp;&nbsp;您于"+sdfInput.format(new Date())+"通过超级试卷王小程序给该邮箱发送了一份名称：《"+check.get("title").toString()+"》的试卷资源，请您点击以下按钮获取试卷资源:<br />" +
					"<a href="+check.get("downurl").toString()+"><IMG SRC=\"http://images.hengqian.net/theme/Dream/img/temporary/Button.png\" WIDTH=\"172\" HEIGHT=\"51\" BORDER=\"0\"></a>" +
					"&nbsp;&nbsp;&nbsp;&nbsp;<p align=\"right\" style=\"line-height:30px;\">超级试卷王<br />"+sdfInput.format(new Date())+"</p>" +
					"            </td>" +
					"        </tr>" +
					"        <tr>" +
					"            <td style=\"background-color: #E5E5E5; border-width: 10px; border-style: solid; border-color:#E5E5E5;line-height:25px;font-size: 14px;color:#999999\" align=\"center\">" +
					"                更多问题欢迎关注 超级试卷王，感谢您的支持。</BR>" +
					"或者拨打全国统一客服热线：400-715-6688，我们的客服人员将会在第一时间为您解答。" +
					"            </td>" +
					"        </tr>" +
					"    </tbody>" +
					"</table>";
			String  mailUser="";
			//查询最后一条邮件发送者
			String  mailLastone = ResourcDatabase.getMailUserLastone();
			if(StringUtils.isEmpty(mailLastone) || !Arrays.asList(mainStr).contains(mailLastone)){
				mailUser = mainStr[0];
			}else{
				//判断已发送是否为邮件最后一条
				if(mailLastone.equals(mainStr[mainStr.length-1])){
					mailUser=mainStr[0];
				}else{
				   for(int i=0;i<mainStr.length;i++){
					   if(mailLastone.equals(mainStr[i])){
						   mailUser = mainStr[i+1];
						   break;
					   }
					}
				}
			}

			HashMap<String,String> condition = new HashMap<String,String>();
			condition.put("uid",selfInfo.get("uid"));
			condition.put("send_mail",mailUser);
			condition.put("receive_mail",check.get("mail").toString());
			condition.put("send_url",check.get("downurl").toString());
			condition.put("send_title",check.get("title").toString());

			HashMap<String, String> sendMailInfo =  ResourcDatabase.getSendMailInfo(selfInfo.get("uid"));

			long min;
			int result;
			if(sendMailInfo==null || sendMailInfo.size()<=0){
				//如果没有发过邮件，发送并存取数据
				Mail.send(check.get("mail").toString(), check.get("title").toString()+"-超级试卷王",msg,mailUser);
				result = ResourcDatabase.insertsendMail(condition);
			}else{

				String sendcount = ResourcDatabase.getSendMailCount(selfInfo.get("uid"));
				if(Integer.parseInt(sendcount)>=10){
					 ret=ApiErrorCode.echoErr(ApiErrorCode.MAIL_SEND_COUNT_OVERTOP);
					m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.MAIL_SEND_COUNT_OVERTOP));
					response.getWriter().write(ret);
					return;
				}

				String create_time = sendMailInfo.get("create_time");
				String now_time = sendMailInfo.get("now_time");
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				long createtime = simpleDateFormat.parse(create_time).getTime();
				long nowtime = simpleDateFormat.parse(now_time).getTime();
				long between = nowtime - createtime;
				long day = between / (24 * 60 * 60 * 1000);
				long hour = (between / (60 * 60 * 1000) - day * 24);
				min = ((between / (60 * 1000)) - day * 24 * 60 - hour * 60);

				if(min<1){
					ret=ApiErrorCode.echoErr(ApiErrorCode.MAIL_SEND_MIN_OVERTOP);
					m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.MAIL_SEND_MIN_OVERTOP));
					response.getWriter().write(ret);
					return;
				}

				Mail.send(check.get("mail").toString(), check.get("title").toString()+"-超级试卷王",msg,mailUser);
				result = ResourcDatabase.insertsendMail(condition);

			}

			if(result>0){
				//返回值
				ret=ApiErrorCode.echoOk();
				response.getWriter().write(ret);
				m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.SUCCESS));
			}else{
				ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
				m_logger.info(String.format("OUTPUT ret_code=%s",ApiErrorCode.SYSTEM_ERROR));
				response.getWriter().write(ret);
				return;

			}



	        return;
		}catch(CheckParameterException e){
			String ret = ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR);
    		m_logger.info(String.format("OUTPUT ret_code=%s %s",ApiErrorCode.PARAMETER_ERROR,e));
    		response.getWriter().write(ret);
    		return;
		}catch(Exception e){
    		String ret = ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR);
    		m_logger.info(String.format("OUTPUT ret_code=%s %s",ApiErrorCode.SYSTEM_ERROR,e));
    		response.getWriter().write(ret);
    		return;
    	}
		
	}
	

}
