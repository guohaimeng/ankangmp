package com.hq.api.subapps.common;




import com.hq.api.subapps.datautils.HQPrimDataSqlServeBaseUtil;
import com.hq.api.subapps.datautils.HqdbDataSqlServeBaseUtil;
import com.hq.api.utils.ApiErrorCode;
import com.hq.dispatcher.Config;
import com.hq.dispatcher.Startup;
import com.hq.utils.MysqlBaseUtil;
import com.hq.utils.SqlServerBaseUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Administrator on 2018/7/23.
 */
public class ResourcDatabase {
    private static final Logger m_logger = Logger.getLogger(ResourcDatabase.class);


    /**
     * 应用列表
     * @param os
     * @param pagesize
     * @param pagenum
     * @return
     * @throws SQLException
     */
    public static ArrayList<HashMap<String, String>> getResourceslist(String os,String pagesize,String pagenum) throws SQLException {
        ArrayList<Object> params = new ArrayList<Object>();
        String sql="select res.appname,res.appstoreurl,res.icon,res.ID as id,res.islocal,res.phase,res.state,res.type,res.weburl,res.app_status from( SELECT t.*, " +
                "CASE WHEN ys.app_status IS NULL THEN 1 WHEN ys.app_status IS NOT NULL THEN ys.app_status END AS app_status,CASE WHEN ys.islocal IS NULL THEN 0 WHEN ys.app_status IS NOT NULL THEN ys.islocal END AS islocal " +
                "from (SELECT aa.*   FROM  (SELECT t.* FROM  " +
                "(SELECT a.AppName AS appname,a.id,a.AppUI AS icon,a.orders,a.QLoginMode AS type,a.AppCallBackUrl AS weburl,a.AppState AS state, " +
                "a.AppPhase as phase, " +
                "s.IOSAppStoreUrl AS appstoreurl,row_number () OVER ( ORDER BY  Orders DESC) AS row_num FROM OpenApp a INNER JOIN OpenAppSetting s ON a.id = s.appid WHERE (a.QLoginMode = ? " +
                "OR QLoginMode = 5) AND IsDrop = 0 AND AppState=3) t WHERE row_num>? AND row_num<=?) aa )as t left JOIN  (SELECT * FROM yx_subapp WHERE os_type=? " +
                "AND app_status=1) ys ON t.id=ys.OpenApp_id ) as res";
        String ostype="0";
        if(os.equals("android")){
            params.add(3);
            ostype="1";
        } else if(os.equals("IOS")){
            params.add(4);
            ostype="2";
        } else{
            return null;
        }
        int  pagenumInt=1;
        if(StringUtils.isEmpty(pagenum) || Integer.parseInt(pagenum)<1){
            pagenumInt = 1;
        }

        params.add((pagenumInt-1)*Integer.parseInt(pagesize));
        params.add(pagenumInt*Integer.parseInt(pagesize));

        params.add(ostype);
        return SqlServerBaseUtil.querySql(sql, params);
    }


    /**
     * 获取资源信息
     * @param roid
     * @param phase
     * @throws SQLException
     */
    public static Map<String,String> inquireResource(String roid, String phase) throws SQLException{
        String localsql="select id,res_original_id roid,title,stars, DATEDIFF( SECOND, '1970-01-01 08:00:00',create_time) createtime,file_size size,down_url downurl,intro from yx_resource where res_original_id = ? and res_database= ?";
        ArrayList<Object> localParams = new ArrayList<>();
        localParams.add(roid);
        localParams.add(phase);
        HashMap<String,String> localRes = SqlServerBaseUtil.getOneRow(localsql,localParams);
        if(localRes.size()==0){
            HashMap<String,String> originalres=null;
            String originalsql=null;
            ArrayList<Object> originalsqlParams = new ArrayList<>();
            originalsqlParams.add(roid);

            if(phase.equals("2")){
                //中学
                originalsql="select id roid,topic title,url1 downurl,fsize size,DATEDIFF( SECOND, '1970-01-01 08:00:00',tim) createtime,tim create_time,stars,c_id classify_id,object_id subject_id,s_id grade_id,res_bb version_id,word intro from res_data where hidden=1 and id= ?";
                originalres = HqdbDataSqlServeBaseUtil.getOneRow(originalsql,originalsqlParams);
            }else if(phase.equals("1")){
                //小学
                originalsql="select id roid,title,ResourceUrl downurl,ResourseSize size,DATEDIFF( SECOND, '1970-01-01 08:00:00',CreatTime) createtime,CreatTime create_time,stars,ClassID classify_id, SubjectId subject_id,GradeId grade_id,VersionId version_id,Content intro from fs_channel_zyxz where hidden=1 and id= ?";
                originalres = HQPrimDataSqlServeBaseUtil.getOneRow(originalsql,originalsqlParams);
            }else{
                return null;
            }
            if(originalres.size()>0){
                String insertlocalResSql = " insert into yx_resource (id,res_original_id,res_database,title,down_url,classify_id,subject_id,grade_id,version_id,file_size,stars,create_time,intro) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ArrayList<Object> insertlocalResSqlparams = new ArrayList<>();
                String newid = Startup.getId()+"";
                originalres.put("id",newid);
                insertlocalResSqlparams.add(newid);
                insertlocalResSqlparams.add(originalres.get("roid"));
                insertlocalResSqlparams.add(phase);
                insertlocalResSqlparams.add(originalres.get("title"));
                insertlocalResSqlparams.add(originalres.get("downurl"));
                insertlocalResSqlparams.add(originalres.get("classify_id"));
                insertlocalResSqlparams.add(originalres.get("subject_id"));
                insertlocalResSqlparams.add(originalres.get("grade_id"));
                insertlocalResSqlparams.add(originalres.get("version_id"));
                insertlocalResSqlparams.add(originalres.get("size"));
                insertlocalResSqlparams.add(originalres.get("stars"));
                insertlocalResSqlparams.add(originalres.get("create_time"));
                insertlocalResSqlparams.add(originalres.get("intro"));
                SqlServerBaseUtil.updateSQL(insertlocalResSql,insertlocalResSqlparams);

                return  originalres;
            }else{
                return null;
            }
        }else {
            return localRes;
        }
    }

    /**
     * 获取文章资源详情
     * @param roid
     * @throws SQLException
     */
    public static Map<String,String> inquireArticleResource(String roid,String phase) throws SQLException{
        String sql = null;
        HashMap<String,String> ret = null;
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(roid);
        if(phase.equals("1")){
            //暂无
        }else{
            sql = "select id roid,topic title,s_id classify,DATEDIFF(SECOND, '1970-01-01 08:00:00',tim) createtime,comto source,username issuer,word content from article where id = ?";
            ret = HqdbDataSqlServeBaseUtil.getOneRow(sql,params);
        }
        if(ret.size() > 0){
            String localSql = "select article_relation_id id,article_original_id roid,data_source source from yx_article_relation where article_original_id = ? and data_source = ?";
            ArrayList<Object> localParams = new ArrayList<Object>();
            localParams.add(roid);
            localParams.add(phase);
            HashMap<String,String> localVideo = SqlServerBaseUtil.getOneRow(localSql,localParams);

            if(!roid.equals(localVideo.get("roid")) && !phase.equals(localVideo.get("source"))){
                String insertlocalVideoSql = " insert into yx_article_relation (article_relation_id,article_original_id,data_source) values(?,?,?)";
                ArrayList<Object> insertlocalVideoSqlParams = new ArrayList<>();
                String newid = Startup.getId() + "";
                ret.put("id", newid);
                insertlocalVideoSqlParams.add(newid);
                insertlocalVideoSqlParams.add(roid);
                insertlocalVideoSqlParams.add(phase);
                SqlServerBaseUtil.updateSQL(insertlocalVideoSql,insertlocalVideoSqlParams);
            }else{
                ret.put("id", localVideo.get("id"));
            }
        }
        return ret;
    }

    /**
     * 获取综合素质应用详情
     * @param roid
     * @param phase
     * @throws SQLException
     */
    public static Map<String,String> inquireVideoInfo(String roid,String phase) throws SQLException{
        String sql = null;
        HashMap<String, String> ret = null;
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(roid);
        if(phase.equals("1")){
            sql = "select Id roid,VideoTitle title,(select SubjectName from fs_channnel_subject where Id = Subjectid)subject,(select GradeName from fs_channel_grade where Id = Gradeid)grade,(select VersionName from fs_channnel_version where Id = Versionid)version,(select classCName from fs_sys_channelclass where id = VideoClass)classify,DATEDIFF( SECOND, '1970-01-01 08:00:00',ReleaseTime) createtime,Speaker lecturer,VideoLanguage lang,VideoForm form,Rating stars,VideoIntroduction intro,VideoAddress viewurl,VideoPicturePath vimg,PlayCount hits from fs_channel_Video where Id = ?";
            ret = HQPrimDataSqlServeBaseUtil.getOneRow(sql,params);
        }else{
            sql = "select id roid,name title,(select myres_name from hq_object where myres_id = oID)subject,(select myres_name from hq_class where myres_id = gID)grade,(select mybb_name from hq_bb where mybb_id = bID)version,(select myres_name from hq_res where myres_id = c_id)classify,DATEDIFF( SECOND, '1970-01-01 08:00:00',tim) createtime,role lecturer,lang,types form,star stars,remark intro,VideoUrl viewurl,pic vimg,counter hits from video where id = ?";
            ret = HqdbDataSqlServeBaseUtil.getOneRow(sql,params);
        }
        if(ret.size() > 0){
            String localSql = "select video_relation_id id,video_original_id roid,data_source source from yx_video_relation where video_original_id = ? and data_source = ?";
            ArrayList<Object> localParams = new ArrayList<Object>();
            localParams.add(roid);
            localParams.add(phase);
            HashMap<String,String> localVideo = SqlServerBaseUtil.getOneRow(localSql,localParams);

            if(!roid.equals(localVideo.get("roid")) && !phase.equals(localVideo.get("source"))){
                String insertlocalVideoSql = " insert into yx_video_relation (video_relation_id,video_original_id,data_source,Hits) values(?,?,?,?)";
                ArrayList<Object> insertlocalVideoSqlParams = new ArrayList<>();
                String newid = Startup.getId() + "";
                ret.put("id", newid);
                insertlocalVideoSqlParams.add(newid);
                insertlocalVideoSqlParams.add(roid);
                insertlocalVideoSqlParams.add(phase);
                insertlocalVideoSqlParams.add(0);
                SqlServerBaseUtil.updateSQL(insertlocalVideoSql,insertlocalVideoSqlParams);
            }else{
                ret.put("id", localVideo.get("id"));
            }
        }
        return ret;
    }

    /**
     * 更新播放数量
     * @param id
     * @throws SQLException
     */
    public static int updateVideoHits(String id,String phase) throws SQLException{
        String sql = "update yx_video_relation set Hits = Hits + 1 where video_original_id = ? and data_source = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(id);
        params.add(phase);
        return SqlServerBaseUtil.updateSQL(sql, params);
    }

    /**
     * 查询播放数量
     * @throws SQLException
     */
    public static HashMap<String,String> queryVideoHits(String id,String phase) throws SQLException{
        String sql = "select Hits hits from yx_video_relation where video_original_id = ? and data_source = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(id);
        params.add(phase);
        return SqlServerBaseUtil.getOneRow(sql,params);
    }

    /**
     * 获取综合素质应用相关视频
     * @return
     */
    public static  ArrayList<HashMap<String,String>> inquireCorrelationVideos(String id,String phase)throws SQLException{
        String sql = null;
        ArrayList<HashMap<String,String>> ret = null;
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(id);
        if(phase.equals("1")){
            sql = "select top 5 Id roid,VideoTitle title,VideoPicturePath vimg,PlayCount hits,VideoAddress viewurl from fs_channel_Video where Id ! = ? and Id between 80 and 153 ORDER BY NEWID()";
            ret = HQPrimDataSqlServeBaseUtil.querySql(sql, params);
        }else{
            sql = "select top 5 id roid,name title,pic vimg,counter hits,VideoUrl viewurl from video where id ! = ? and id between 268 and 351 ORDER BY NEWID()";
            ret = HqdbDataSqlServeBaseUtil.querySql(sql, params);
        }
        return ret;
    }

    /**
     * 获取视频资源详情
     * @param roid
     * @throws SQLException
     */
    public static Map<String,String> inquireVideoResource(String roid) throws SQLException{
        String sql = "select ID roid,name title,(select Name from video_BaseData where ID = subjectID)subject,(select Name from video_BaseData where Id = gradeID)grade,file_path viewurl,DATEDIFF( SECOND, '1970-01-01 08:00:00',addtime)createtime,cover_url as vimg from video_resource where ID = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(Long.parseLong(roid));
        HashMap<String,String> ret = SqlServerBaseUtil.getOneRow(sql,params);
        if(ret.size() > 0){
            String localSql = "select video_relation_id id,video_original_id roid,data_source source from yx_video_relation where video_original_id = ? and data_source = 3";
            ArrayList<Object> localParams = new ArrayList<Object>();
            localParams.add(roid);
            HashMap<String,String> localVideo = SqlServerBaseUtil.getOneRow(localSql,localParams);

            if(!roid.equals(localVideo.get("roid"))){
                String insertlocalVideoSql = " insert into yx_video_relation (video_relation_id,video_original_id,data_source) values(?,?,?)";
                ArrayList<Object> insertlocalVideoSqlParams = new ArrayList<>();
                String newid = Startup.getId() + "";
                ret.put("id", newid);
                insertlocalVideoSqlParams.add(newid);
                insertlocalVideoSqlParams.add(roid);
                insertlocalVideoSqlParams.add("3");
                SqlServerBaseUtil.updateSQL(insertlocalVideoSql,insertlocalVideoSqlParams);
            }else{
                ret.put("id", localVideo.get("id"));
            }
        }
        return ret;
    }

    /**
     * 获取综合素质应用相关视频
     * @return
     */
    public static  ArrayList<HashMap<String,String>> inquireRelatedVideos(String id)throws SQLException{
        String sql = "select top 5 ID roid,name title,DATEDIFF( SECOND, '1970-01-01 08:00:00',addtime)createtime,cover_url as vimg from video_resource where ID ! = ? ORDER BY NEWID()";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(id);
        return SqlServerBaseUtil.querySql(sql, params);
    }

    /**
     * 获取教研提升应用相关文章
     * @return
     */
    public static  ArrayList<HashMap<String,String>> inquireRelatedArticle(String roid,String classify)throws SQLException{
        String sql = "select top 8 id roid,topic title,DATEDIFF( SECOND, '1970-01-01 08:00:00',tim)createtime from article where s_id = ? and id != ? order by tim desc";
        ArrayList<Object> params = new ArrayList<Object>();
        params.add(classify);
        params.add(roid);
        ArrayList<HashMap<String, String>> ret = HqdbDataSqlServeBaseUtil.querySql(sql, params);
        return ret;
    }

    /**
     * 获取资源真实下载地址
     * @throws SQLException
     */
    public static HashMap<String,String> queryReallyDown(String resfilename) throws SQLException{
        String sql = "select url1 downurl,resfilename resname,DATEDIFF(SECOND, '1970-01-01 08:00:00',tim)createtime from res_data where resfilename in(" + resfilename + ")";
        ArrayList<Object> params = new ArrayList<>();
        return HqdbDataSqlServeBaseUtil.getOneRow(sql,params);
    }

    /**
     * 获取资源扩展名
     * @param downurl
     * @return
     */
    public static String  getResExtname(String downurl) {
        String extname = "";
        downurl = downurl.trim();
        if (!StringUtils.isEmpty(downurl)) {
            String[] types = downurl.split("\\.");
            extname = types[types.length - 1].toLowerCase();
        }
        return extname;
    }

    /**
     * 截取视频资源前缀名
     * @param name
     * @return
     */
    public static String  getPrefixname(String name) {
        String prefixname = "";
        String[] strs = name.split("\\.");
        for (int i = 0, len = strs.length; i < len; i++) {
            prefixname = strs[strs.length - strs.length].toLowerCase();
        }
        return prefixname;
    }

    /**
     * 获取资源文件大小
     * @param phase
     * @return
     */
    public static String  getResFileSize(String phase,String fileSize,String extname) {
        String regex = "^.*[a-zA-z]$";
        Pattern p = Pattern.compile(regex);
        if (!StringUtils.isEmpty(fileSize)) {
            fileSize = fileSize.trim();
            Matcher m = p.matcher(fileSize);
            if (!m.matches()) {
                if (phase.equals("1")) {
                    fileSize = fileSize + "KB";
                } else if (phase.equals("2")) {
                    if(extname.contains("rar")||extname.contains("zip")){
                        fileSize = fileSize + "MB";
                    }else{
                        fileSize = fileSize + "KB";
                    }
                }
            }
        }
        return fileSize;
    }
    /**
     * 获取资源扩展名公共方法
     * @param phase
     * @param res
     * @return
     */

    public static ArrayList<HashMap<String,String>>  dealResExtname(String phase,ArrayList<HashMap<String,String>> res){

        if(res!=null && res.size()>0){
            for (HashMap<String, String> map : res) {
                if(StringUtils.isEmpty(map.get("downurl"))){
                    map.put("extname", "");
                }else{
                    map.put("extname", getResExtname(map.get("downurl")));
                }
                map.put("size",getResFileSize(phase,map.get("size"),getResExtname(map.get("downurl"))));
                map.remove("downurl");
            }
        }


        return res;
    }

    /**
     * 查询对应id
     * @return
     * @throws SQLException
     */
    public static ArrayList<HashMap<String,String>> getCorreID(String reid) throws SQLException{
        String sql = "select CloudMap id from BaseDataClass where ID in(" + reid + ")";
        ArrayList<Object> params = new ArrayList<>();
        return SqlServerBaseUtil.querySql(sql,params);
    }

    /**
     * 查询用户教学属性
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getTeachingattr(String uid) throws SQLException{
        String sql = "select SubjectID subid,GradeID grid,VersionID veid from IEnjoy where UserID = ? and IsDefault = 1";
        ArrayList<Object> params = new ArrayList<>();
        params.add(uid);
        return SqlServerBaseUtil.getOneRow(sql,params);
    }

    /**
     * 查询教学属性
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getSubjectPre(String uid) throws SQLException{
        String sql = "select Phase phase from BaseDataClass where ID = (select GradeID from IEnjoy where UserID = ? and IsDefault = 1)";
        ArrayList<Object> params = new ArrayList<>();
        params.add(uid);
        return SqlServerBaseUtil.getOneRow(sql,params);
    }




    /**
     * 查询用户对应小学学科
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getPriSubject(String subid) throws SQLException{
        String sql = "select ID id,relationName name,Pid pid from fs_channel_relation where SubjectId = ? and ClassId = 0 and Pid = 0";
        ArrayList<Object> params = new ArrayList<>();
        params.add(subid);
        return HQPrimDataSqlServeBaseUtil.getOneRow(sql,params);
    }

    /**
     * 查询用户对应小学年级
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getPriGrade(String grid,String pid) throws SQLException{
        String sql = "select ID id,relationName name,Pid pid from fs_channel_relation where GradeId = ? and ClassId = 1 and Pid = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(grid);
        params.add(pid);
        return HQPrimDataSqlServeBaseUtil.getOneRow(sql,params);
    }

    /**
     * 查询用户对应小学版本
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getPriVersion(String veid,String pid) throws SQLException{
        String sql = "select ID id,relationName name,Pid pid from fs_channel_relation where VersionId = ? and ClassId = 2 and Pid = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(veid);
        params.add(pid);
        return HQPrimDataSqlServeBaseUtil.getOneRow(sql,params);
    }

    /**
     * 查询用户对应中学学科
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getMiddSubject(String subid) throws SQLException{
        String sql = "select myocb_id id,myocb_f pid,myocb_name name from hq_ocb where object_id = ? and myocb_star = 0 and myocb_f = 0";
        ArrayList<Object> params = new ArrayList<>();
        params.add(subid);
        return HqdbDataSqlServeBaseUtil.getOneRow(sql,params);
    }

    /**
     * 查询用户对应中学年级
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getMiddGrade(String grid,String pid) throws SQLException{
        String sql = "select myocb_id id,myocb_f pid,myocb_name name from hq_ocb where s_id = ? and myocb_star = 1 and myocb_f = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(grid);
        params.add(pid);
        return HqdbDataSqlServeBaseUtil.getOneRow(sql,params);
    }

    /**
     * 查询用户对应中学版本
     * @return
     * @throws SQLException
     */
    public static HashMap<String,String> getMiddVersion(String veid,String pid) throws SQLException{
        String sql = "select myocb_id id,myocb_f pid,myocb_name name from hq_ocb where bb_id = ? and myocb_star = 2 and myocb_f = ?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(veid);
        params.add(pid);
        return HqdbDataSqlServeBaseUtil.getOneRow(sql,params);
    }


    /**
     * 获取用户学段匹配
     * @param regex
     * @param grade
     * @return
     */
    public static boolean  getUserGrade(String regex,String grade) {
        boolean  flag =false;
        Pattern p = Pattern.compile(regex);
        if (!StringUtils.isEmpty(grade)) {
            Matcher m = p.matcher(grade);
            flag = m.matches();
        }
        return flag;
    }


    /**
     * 查询小学试卷资源
     * @return
     * @throws SQLException
     */
    public static ArrayList<HashMap<String,String>> getIndexTestWarehouseByJunior(String  grade) throws SQLException{

        ArrayList<Object> params = new ArrayList<>();
        params.add(grade);
        String sql="select 1 as phase,case when ClassID=8 then '阶段试卷' when ClassID=9 then '阶段试卷' when  ClassID=50 then '阶段试卷' when ClassID=51 then '双基双测' " +
                " end AS classname, Id roid,title,ResourceUrl downurl,ResourseSize size,stars,DATEDIFF( SECOND, '1970-01-01 08:00:00',CreatTime) createtime from fs_channel_zyxz where hidden = 1" +
                " and ClassID in(8,9,50,51) and GradeId =?  order by CreatTime desc,stars desc,ContentProperty desc OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY";
        ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>();
        res = HQPrimDataSqlServeBaseUtil.querySql(sql, params);
        //测试代码
//        String [] gradede={"12"};
//        String sqlgrade ="insert into t_module_grade_relation(rid,mid,grade,create_time) values";
//
//        ArrayList<Object> paramss = new ArrayList<>();
//        for(int i=0;i<gradede.length;i++){
//            sqlgrade+="(?,?,?,now()),";
//            paramss.add(String.valueOf(Startup.getId()));
//            paramss.add("184191368111850496");
//            paramss.add(gradede[i]);
//        }
//        sqlgrade = sqlgrade.substring(0, sqlgrade.length()-1);
//        MysqlBaseUtil.updateSQL(sqlgrade,paramss);

        return res;
    }

    /**
     * 查询小学资源版本名称
     * @return
     * @throws SQLException
     */
    public static String getVersionNameByJunior(String  veid) throws SQLException {
        String sql="select VersionName from fs_channnel_version where id=?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(veid);
        return HQPrimDataSqlServeBaseUtil.getOneColumnByRow(sql,params);


    }
    /**
     * 查询中学试卷资源
     * @return
     * @throws SQLException
     */
    public static ArrayList<HashMap<String,String>> getIndexTestWarehouseByMiddle(String  grade) throws SQLException{
        String sid="";
        if(grade.equals("7")){
            sid="1";
        }else if(grade.equals("8")){
            sid="2";
        }else if(grade.equals("9")){
            sid="3";
        }else if(grade.equals("10")){
            sid="4";
        }else if(grade.equals("11")){
            sid="5";
        }else if(grade.equals("12")){
            sid="6";
        }else{
            //
        }
        String sql="select 2 as phase,case when c_id=8 then '阶段试卷' when c_id=9 then '阶段试卷' when  c_id=37 then '阶段试卷' when c_id=109 then '双基双测' " +
                " when c_id=10 then '中考' when  c_id=12 then '高考'  when  c_id=11 then '中考' when  c_id=13 then '高考' end AS classname, id roid,topic title,stars stars,url1 downurl,fsize size,DATEDIFF( SECOND, '1970-01-01 08:00:00',tim)createtime from res_data where hidden = 1" +
                " and c_id in(37,8,9,109,10,12,11,13)  ";
        ArrayList<Object> params = new ArrayList<>();
        if(!StringUtils.isEmpty(sid)){
            sql+=" and s_id = ? ";
            params.add(sid);
        }
        sql+="order by tim desc,stars desc,istop desc OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY";
//        String  sqltest="insert into t_module(mid,title,code,icon,create_time,is_delete) values(?,?,?,?,now(),0)";
//        ArrayList<Object> paramstest = new ArrayList<>();
//        paramstest.add(String.valueOf(Startup.getId()));
//        paramstest.add("高考真题");
//        paramstest.add("gkzt");
//        paramstest.add("http://180.150.186.42:80/group1/M00/08/EB/CgqcUFujDL-AMC_AAAAHNYOkHrE954.png");
//        MysqlBaseUtil.updateSQL(sqltest,paramstest);
        ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>();
        res = HqdbDataSqlServeBaseUtil.querySql(sql, params);
        return res;
    }

    /**
     * 查询中学资源版本名称
     * @return
     * @throws SQLException
     */
    public static String getVersionNameByMidele(String  veid) throws SQLException {
        String sql="select mybb_name from  hq_bb where mybb_id=?";
        ArrayList<Object> params = new ArrayList<>();
        params.add(veid);
        return HqdbDataSqlServeBaseUtil.getOneColumnByRow(sql,params);

    }


    /**
     * 查询用户发送资源邮件
     * @return
     * @throws SQLException
     */
    public static  HashMap<String, String>  getSendMailInfo(String  uid) throws SQLException {
        String sql="select id,send_mail,receive_mail,send_url,send_title,create_time,NOW() as now_time from  t_mail_count where send_uid=? order by create_time desc limit 1";
        ArrayList<Object> params = new ArrayList<>();
        params.add(uid);
        return MysqlBaseUtil.getOneRow(sql,params);

    }
    /**
     * 查询用户发送资源次数
     * @return
     * @throws SQLException
     */
    public static  String  getSendMailCount(String  uid) throws SQLException {
        String sql="select count(send_uid)  from  t_mail_count where send_uid=? and TO_DAYS(create_time) = TO_DAYS(NOW()) ";
        ArrayList<Object> params = new ArrayList<>();
        params.add(uid);
        return MysqlBaseUtil.getOneColumnByRow(sql,params);

    }



    /**
     * 添加用户发送资源邮件
     * @return
     * @throws SQLException
     */
    public static  int  insertsendMail(HashMap<String,String>  condition) throws SQLException {
        String sql="INSERT INTO t_mail_count VALUES(?,?,?,?,?,?,NOW())";
        ArrayList<Object> params = new ArrayList<>();
        params.add(String.valueOf(Startup.getId()));
        params.add(condition.get("uid"));
        params.add(condition.get("send_mail"));
        params.add(condition.get("receive_mail"));
        params.add(condition.get("send_url"));
        params.add(condition.get("send_title"));
        return MysqlBaseUtil.updateSQL(sql,params);

    }

    /**
     * 查询用户匹配模块
     * @return
     * @throws SQLException
     */
    public static ArrayList<HashMap<String,String>> getUserModule(String  grade) throws SQLException{

        ArrayList<Object> params = new ArrayList<>();
        params.add(grade);
        String sql="SELECT title,subtitle,code,icon FROM t_module AS tm  INNER JOIN (SELECT MID FROM  t_module_grade_relation WHERE grade=?) AS tmgr  ON tmgr.mid =tm.mid AND tm.is_delete =0 ";
        ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>();
        res = MysqlBaseUtil.querySql(sql, params);
        return res;
    }


    /**
     * 查询用户匹配模块
     * @return
     * @throws SQLException
     */
    public static String getMailUserLastone() throws SQLException{

        ArrayList<Object> params = new ArrayList<>();
        String sql="SELECT  send_mail  FROM t_mail_count order by create_time desc limit 1";
        return MysqlBaseUtil.getOneColumnByRow(sql,params);
    }







}
