package com.hq.api.index;

import com.hq.api.subapps.common.ResourcDatabase;
import com.hq.api.utils.ApiErrorCode;
import com.hq.api.utils.CheckParameterException;
import com.hq.api.utils.CheckParameters;
import com.hq.api.utils.DatabaseManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 获取应用列表
 */
@WebServlet("/index/getIndexRecord.do")
public class GetIndexRecord extends HttpServlet {
    private static final Logger m_logger = Logger.getLogger(GetIndexRecord.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        JSONObject job=null;
        try{
            //参数校验
            CheckParameters check = new CheckParameters(request.getParameterMap());
            m_logger.info(String.format("INPUT params=%s",check.getParameterJSON()));

            Map<String, String> sessionInfo = DatabaseManager.getSession(request);
            String  grade = sessionInfo.get("grade");
            String regexJunior = "^[1-6]$";
            String regexMiddle = "^[7-9]|[1][0,1,2]$";
            String phase="";
            ArrayList<HashMap<String, String>> res = new  ArrayList<HashMap<String, String>>();
            //首页固定资源
            if(ResourcDatabase.getUserGrade(regexJunior,grade)){
                res =ResourcDatabase.getIndexTestWarehouseByJunior(grade);
                phase="1";
            }else if(ResourcDatabase.getUserGrade(regexMiddle,grade)){
                res =ResourcDatabase.getIndexTestWarehouseByMiddle(grade);
                phase="2";
            }


            if(res!=null && res.size()>0){
                res = ResourcDatabase.dealResExtname(phase, res);
            }

            //模块配置  根据用户年级匹配
            ArrayList<HashMap<String, String>> moduleres = ResourcDatabase.getUserModule(grade);

            HashMap<String,Object> bannerone= new HashMap<String,Object>();
            bannerone.put("icon","http://dev.hengqian.net.cn/group1/M00/08/B4/CgqcUFthXNaAKRdGAAfecyAiifs094.png");
            bannerone.put("linkurl","");
            HashMap<String,Object> bannertwo= new HashMap<String,Object>();
            bannertwo.put("icon","http://dev.hengqian.net.cn/group1/M00/08/B4/CgqcUFthXQ-AAHjMAAVPVRrSDNU208.png");
            bannertwo.put("linkurl","https://owa.hengqian.net/wv/wordviewerframe.aspx?WOPISrc=https%3a%2f%2fview.hengqian.net%2f%2fwopi%2ffiles%2fCshzCVpe86qALJtGAAZyAPsIIAI021.doc%3fUriPath%3dC24041A265404F1EB5E7F5CA353B7C7E0E2C1AB6C2F99B2DFF57D8D69B8D0ABAA90A8E1B6239EF9AFE115C972C57A957&access_token=8fe49abfd9ab1d66");
            JSONArray jsonArraybanner = new JSONArray();
            jsonArraybanner.put(bannerone);
            jsonArraybanner.put(bannertwo);

            job=new JSONObject();
            job.put("subapps",moduleres);
            job.put("res",res);
            job.put("banner",jsonArraybanner);
            String ret = ApiErrorCode.echoClassOk(job);
            m_logger.info(String.format("OUTPUT ret_code=%s", ApiErrorCode.SUCCESS));
            response.getWriter().write(ret);
            return;

        } catch(CheckParameterException ce){
            m_logger.info(String.format("OUTPUT ret_code=%s %s",ApiErrorCode.PARAMETER_ERROR,ce));
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.PARAMETER_ERROR));
            return;
        } catch(Exception e){
            m_logger.error(String.format("getResourceInfo Exception=%s",e.getMessage()),e);
            response.getWriter().write(ApiErrorCode.echoErr(ApiErrorCode.SYSTEM_ERROR));
            return;
        }

    }

    public static HashMap<String,Object> assemblydata(String  title,String subtitle,String code,String icon){
        HashMap<String,Object> assemblymap = new  HashMap<String,Object>();
        assemblymap.put("title",title);
        assemblymap.put("subtitle",subtitle);
        assemblymap.put("code",code);
        assemblymap.put("icon",icon);
        return assemblymap;
    }

}
