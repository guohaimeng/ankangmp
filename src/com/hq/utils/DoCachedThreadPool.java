package com.hq.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Administrator on 2016/6/19 0019.
 */
public class DoCachedThreadPool {
    private volatile static DoCachedThreadPool doCachedThreadPool;
    private DoCachedThreadPool(){}
    public  ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
    public static DoCachedThreadPool getCachedThreadPool() {
      if (doCachedThreadPool == null) {
            synchronized (DoCachedThreadPool.class) {
            if (doCachedThreadPool == null) {
                doCachedThreadPool = new DoCachedThreadPool();
             }
             }
         }
        return doCachedThreadPool;
         }

}
