package com.hq.utils;


import com.hq.dispatcher.Config;
import com.hq.dispatcher.ConfigKey;
import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


/**
 * 基本用户信息在REDIS的访问操作类
 * @author Administrator
 *
 */

public class RedisBaseUtil {
	private static final Logger m_logger = Logger.getLogger(RedisBaseUtil.class);
	private static final String  REDIS_CACHE_VERSION = "_"+ Config.getInstance().getString(ConfigKey.KEY_REDIS_CACHE_VERSION);

	// 定义Redis连接池数组，保存每个sharding的连接池
	private static JedisPool baseJedisPool=null;

	
	private static JedisPoolConfig jedisPoolConfig() {
		JedisPoolConfig config = new JedisPoolConfig();
		// 控制一个pool可分配多少个jedis实例，通过pool.getResource()来获取；
		// 如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
		config.setMaxTotal(2000);
		// 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例。
		config.setMaxIdle(100);
		config.setMinIdle(10);
		// 表示当borrow(引入)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException；
		config.setMaxWaitMillis(1000 * 10);
		// 在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
		config.setTestOnBorrow(false);
		return config;
	}

	// 创建一个连接池
	public static JedisPool addPool(String ip,int port) {
		
		JedisPool pool = null;
		JedisPoolConfig config = jedisPoolConfig();
		pool = new JedisPool(config, ip, port);
		m_logger.info("Initialize base JedisPool: " + ip+":"+port);
		return pool;
	}


	/**
	 * 带版本的set
	 * @param key
	 * @param value
	 * @param jedis
	 * @return
	 */
	public static String setByVersion(String key, String value, Jedis jedis){
		return jedis.set(key+REDIS_CACHE_VERSION, value);
	}


	/**
	 * 带版本的get
	 * @param key
	 * @param jedis
	 * @return
	 */
	public static String getByVersion(String key, Jedis jedis){
		return jedis.get(key+REDIS_CACHE_VERSION);
	}

	// 创建一个连接池列表
	public static void initJedisPoolList() throws Exception{
		
		
		//从数据库读取Notify节点相关的REDIS分片配置信息
		/*String getBaseRedisNodeSql = "SELECT ip,port FROM rkcloud_server_config "
									   + "WHERE category=7 AND is_enable=1";
		
		ArrayList<HashMap<String,String>> messageNodeList=MysqlBaseUtil.querySql(getBaseRedisNodeSql, new ArrayList<Object>());
		
		for(HashMap<String,String> messageNode:messageNodeList){
			String ip = messageNode.get("ip");
			int port = Integer.parseInt(messageNode.get("port"));
			baseJedisPool = addPool(ip,port);
		}*/
		
		
		String[] baseRedisInfo = Config.getInstance().getString(ConfigKey.KEY_REDIS_BASE_INFO).split(":");
		baseJedisPool = addPool(baseRedisInfo[0],Integer.parseInt(baseRedisInfo[1]));
		
		
	}

	public static JedisPool getBaseRedis() {		
	
		return baseJedisPool;
	}
	

	public static void shutdownRedisPool() {
		try {
			if (baseJedisPool != null)
				baseJedisPool.destroy();
		} catch (Exception ex) {
			m_logger.error("Close Master Redis Connection Pool failed.", ex);
		}
	}

}
