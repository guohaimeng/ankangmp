package com.hq.utils.aes;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**

 *
 */

public class AESCodec {

	// 密钥算法
	public static final String KEY_ALGORITHM = "AES";

	// 加解密算法/工作模式/填充方式,Java6.0支持PKCS5Padding填充方式,BouncyCastle支持PKCS7Padding填充方式
	public static final String CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";

	/**
	 * 生成密钥 自动生成AES128位密钥 传入保存密钥文件路径 filePath 表示文件存储路径加文件名；
	 * 例如d:\aes.txt
	 * 
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	public static void getAutoCreateAESKey(String filePath)
			throws NoSuchAlgorithmException, IOException {
		KeyGenerator kg = KeyGenerator.getInstance("AES");
		kg.init(128);// 要生成多少位，只需要修改这里即可128, 192或256
		SecretKey sk = kg.generateKey();
		byte[] b = Base64.encode(sk.getEncoded()).getBytes("utf-8");
		FileOutputStream fos = new FileOutputStream(filePath);
		String str = new String(b,"utf-8");
		System.out.println(str);
		fos.write(b);
		fos.flush();
		fos.close();
	}

	/**
	 * 生成密钥
	 */
	public static String initkey() throws Exception {
		KeyGenerator kg = KeyGenerator.getInstance(KEY_ALGORITHM); // 实例化密钥生成器
		kg.init(128); // 初始化密钥生成器:AES要求密钥长度为128,192,256位
		SecretKey secretKey = kg.generateKey(); // 生成密钥
		return Base64.encode(secretKey.getEncoded()); // 获取二进制密钥编码形式
	}

	/**
	 * 转换密钥
	 */
	public static Key toKey(byte[] key) throws Exception {
		return new SecretKeySpec(key, KEY_ALGORITHM);
	}

	/**
	 * 加密数据
	 *
	 * @param data
	 *            待加密数据
	 * @param key
	 *            密钥
	 * @return 加密后的数据
	 * */
	public static String encrypt(String data, String key) throws Exception {
		Key k = toKey(Base64.decode(key)); // 还原密钥
		// 使用PKCS7Padding填充方式,这里就得这么写了(即调用BouncyCastle组件实现)
		// Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, "BC");
		Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM); // 实例化Cipher对象，它用于完成实际的加密操作
		cipher.init(Cipher.ENCRYPT_MODE, k); // 初始化Cipher对象，设置为加密模式
		return Base64.encode(cipher.doFinal(data.getBytes("utf-8"))); // 执行加密操作。加密后的结果通常都会用Base64编码进行传输
	}

	public static String encryptByKeyFromFile(String data, String keyFilePath)
			throws Exception {
		File file = new File(keyFilePath);
		byte[] key = new byte[(int) file.length()];
		FileInputStream fis = new FileInputStream(file);
		fis.read(key);
		fis.close();
		String keyStr = new String(key, "utf-8");
		Key k = toKey(Base64.decode(keyStr));
		// 使用PKCS7Padding填充方式,这里就得这么写了(即调用BouncyCastle组件实现)
		// Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, "BC");
		Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM); // 实例化Cipher对象，它用于完成实际的加密操作
		cipher.init(Cipher.ENCRYPT_MODE, k); // 初始化Cipher对象，设置为加密模式
		return Base64.encode(cipher.doFinal(data.getBytes("utf-8"))); // 执行加密操作。加密后的结果通常都会用Base64编码进行传输
	}

	/**
	 * 解密数据
	 *
	 * @param data
	 *            待解密数据
	 * @param key
	 *            密钥
	 * @return 解密后的数据
	 * */
	public static String decrypt(String data, String key) throws Exception {
		Key k = toKey(Base64.decode(key));
		Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, k); // 初始化Cipher对象，设置为解密模式
		return new String(cipher.doFinal(Base64.decode(data)),"utf-8"); // 执行解密操作
	}

	public static String decryptByKeyFromFile(String data, String keyFilePath)
			throws Exception {
		File file = new File(keyFilePath);
		byte[] key = new byte[(int) file.length()];
		FileInputStream fis = new FileInputStream(file);
		fis.read(key);
		fis.close();
		String keyStr = new String(key, "utf-8");
		Key k = toKey(Base64.decode(keyStr));
		Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, k); // 初始化Cipher对象，设置为解密模式
		return new String(cipher.doFinal(Base64.decode(data))); // 执行解密操作
	}

	public static void main(String [] args) throws Exception {
		//AESCodec.getAutoCreateAESKey("f:/123.txt");
		System.out.println(AESCodec.initkey());
	}
}
