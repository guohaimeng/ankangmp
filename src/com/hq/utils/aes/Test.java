package com.hq.utils.aes;

public class Test {

	public static void main(String[] args) {
		try {
			String source = "最近在做一个项目，要用到数据加密算法，所以就看了下《java加密与解密的艺术》这本书，最后就参考了下AES加密算法来加密文件，一是它加密标准高、密钥建立时间短、灵敏性好、内存需求低，二是因为javaAPI已经自带了AES算法，用起来很方便顺手，当然，这个还不算，密钥的产生还调用了Base64算法对AES产生的密钥进行了二次加密，确保密钥的安全可靠，大家有兴趣的话可以去看看《java加密与解密的艺术》一书，这里我贴上代码供参考：";
			System.out.println("原文：" + source);

			String key = AESCodec.initkey();
			System.out.println("密钥：" + key);
			//String key = "EUKoq661JAdqa/Jfu2aZyQ==";
			String encryptData = AESCodec.encrypt(source, key);
			System.out.println("加密：" + encryptData);

			String decryptData = AESCodec.decrypt(encryptData, key);
			System.out.println("解密: " + decryptData);
			
//			String keyFilePath = "d:\\aes.txt";
//			AESCodec.getAutoCreateAESKey(keyFilePath);
//			String encryptData = AESCodec.encryptByKeyFromFile(source, keyFilePath);
//			System.out.println("加密：" + encryptData);
//			String decryptData = AESCodec.decryptByKeyFromFile(encryptData, keyFilePath);
//			System.out.println("解密: " + decryptData);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
