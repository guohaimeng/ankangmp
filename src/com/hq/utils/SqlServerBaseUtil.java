package com.hq.utils;

import com.hq.dispatcher.Config;
import com.hq.dispatcher.ConfigKey;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.beans.PropertyVetoException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * DB 访问辅助类
 * 该类访问基本数据库，全局共享配置数据都在这里
 * 
 *
 */
public class SqlServerBaseUtil {
	private static final Logger m_logger = Logger.getLogger(SqlServerBaseUtil.class);

	/**
	 * DB 连接池
	 */
	public static HikariDataSource m_connectionPool = null;

	/**
	 * 初始化数据库连接池
	 * 
	 * @throws PropertyVetoException
	 */
	public static void intializeConnectionPool() throws Exception {
		
		String[] baseSqlServerInfo = Config.getInstance().getString(ConfigKey.KEY_SQLSERVER_BASE_INFO).split(";");
		String jdbcUrl="";
		if(baseSqlServerInfo.length==4){
			jdbcUrl = "jdbc:sqlserver://"+ baseSqlServerInfo[0]+ ";DatabaseName=" + baseSqlServerInfo[1];
			m_logger.info("Initialize base sqlServerPool: " + jdbcUrl);
		}else{
			StringBuffer sb = new StringBuffer();
			for(int i = 0; i < baseSqlServerInfo.length; i++){ sb.append(baseSqlServerInfo[i]);}
			throw new IllegalArgumentException("sqlserver base info parameter is: "+sb.toString());
		}

		HikariConfig config = new HikariConfig();
		config.setMaxLifetime(Config.getInstance().getInt("hikaricp.maxLifetime"));
		config.setMaximumPoolSize(Config.getInstance().getInt("hikaricp.maximumPoolSize"));
		config.setJdbcUrl(jdbcUrl);
		config.setUsername(baseSqlServerInfo[2]);
		config.setPassword(baseSqlServerInfo[3]);
		config.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		m_connectionPool = new HikariDataSource(config);
//		m_connectionPool.setDriverClass("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//		m_connectionPool.setJdbcUrl(jdbcUrl);
//		m_connectionPool.setUser(baseSqlServerInfo[2]);
//		m_connectionPool.setPassword(baseSqlServerInfo[3]);
//		m_connectionPool.setMinPoolSize(Config.getInstance().getInt(ConfigKey.KEY_POOL_MIN_SIZE));
//		m_connectionPool.setMaxPoolSize(Config.getInstance().getInt(ConfigKey.KEY_POOL_MAX_SIZE));
//		m_connectionPool.setMaxIdleTime(Config.getInstance().getInt(ConfigKey.KEY_POOL_TIMEOUT));
//		m_connectionPool.setMaxStatements(Config.getInstance().getInt(ConfigKey.KEY_POOL_MAX_SQL));
		m_logger.info("conn state:"+m_connectionPool);
		m_logger.info("conn state1:"+m_connectionPool);
	}




	/**
	 * DB 连接池
	 */
	//public static ComboPooledDataSource m_connectionPool = null;

	/**
	 * 初始化数据库连接池
	 *
	 * @throws PropertyVetoException
	 */
//	public static void intializeConnectionPool() throws Exception {
//
//		String[] baseSqlServerInfo = Config.getInstance().getString(ConfigKey.KEY_SQLSERVER_BASE_INFO).split(";");
//		String jdbcUrl="";
//		if(baseSqlServerInfo.length==4){
//			jdbcUrl = "jdbc:sqlserver://"+ baseSqlServerInfo[0]+ ";DatabaseName=" + baseSqlServerInfo[1];
//			m_logger.info("Initialize base sqlServerPool: " + jdbcUrl);
//		}else{
//			StringBuffer sb = new StringBuffer();
//			for(int i = 0; i < baseSqlServerInfo.length; i++){ sb.append(baseSqlServerInfo[i]);}
//			throw new IllegalArgumentException("sqlserver base info parameter is: "+sb.toString());
//		}
//		m_connectionPool = new ComboPooledDataSource();
//		m_connectionPool.setDriverClass("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//		m_connectionPool.setJdbcUrl(jdbcUrl);
//		m_connectionPool.setUser(baseSqlServerInfo[2]);
//		m_connectionPool.setPassword(baseSqlServerInfo[3]);
//		m_connectionPool.setMinPoolSize(Config.getInstance().getInt(ConfigKey.KEY_POOL_MIN_SIZE));
//		m_connectionPool.setMaxPoolSize(Config.getInstance().getInt(ConfigKey.KEY_POOL_MAX_SIZE));
//		m_connectionPool.setMaxIdleTime(Config.getInstance().getInt(ConfigKey.KEY_POOL_TIMEOUT));
//		m_connectionPool.setMaxStatements(Config.getInstance().getInt(ConfigKey.KEY_POOL_MAX_SQL));
//		m_logger.info("conn state:"+m_connectionPool);
//		m_logger.info("conn state1:"+m_connectionPool);
//	}





	/**
	 * 关闭数据库连接池
	 */
	public static void shutdownConnectionPool() {
		try {
			if (m_connectionPool != null && !m_connectionPool.isClosed())
				m_connectionPool.close();
		} catch (Exception ex) {
			m_logger.error("Close Connection Pool failed", ex);
		}
	}
	

	/**
	 * 执行一个SQL语句。
	 * @param sql
	 * @param params 输入参数，为字符串数组。注意参数和表的 field 的类型必须对应，否则会引起查询效率的降低
	 * @return
	 * @throws SQLException
	 */	
	public static ArrayList<HashMap<String, String>> querySql(String sql, ArrayList<Object> params) throws SQLException {
		if (m_logger.isDebugEnabled()) {
			m_logger.debug(getPreparedSQL(sql, params));
		}
		Connection conn=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		try{
			conn = m_connectionPool.getConnection();
			ps = (PreparedStatement) conn.prepareStatement(sql);
			for (int i = 0; i < params.size(); i++) {
				if(params.get(i) instanceof String){
					ps.setString(i+1, params.get(i).toString());
				}else if(params.get(i) instanceof Integer){
					ps.setInt(i+1, (int) params.get(i));
				}else if(params.get(i) instanceof Long){
					ps.setLong(i+1, (long) params.get(i));
				}else if(params.get(i) instanceof Double){
					ps.setDouble(i+1, (double) params.get(i));
				}else if(params.get(i) instanceof Float){
					ps.setFloat(i+1, (float) params.get(i));
				}else if(params.get(i)==null){
					ps.setNull(i+1,Types.NULL);						
				}else{
					ps.setString(i+1, params.get(i).toString());				
				}
			}
			// 获取结果集
			rs = ps.executeQuery();
			// 循环读数据
			ArrayList<HashMap<String, String>> rows = new ArrayList<HashMap<String, String>>();
			ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
			while (rs.next()) {
				HashMap<String, String> row = new HashMap<String, String>();
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					String colName = rsmd.getColumnLabel(i);
					String colValue = rs.getString(i);
					row.put(colName, colValue);
				}
				rows.add(row);
			}
			return rows;
		}finally{
			safeClose(rs);
			safeClose(ps);
			safeClose(conn);
		}
	}
	
	public static JSONArray getRowsToJSONArray(String sql,ArrayList<Object> params) throws SQLException {
		m_logger.info(getPreparedSQL(sql, params));
		Connection conn =null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		try{
			conn = m_connectionPool.getConnection();
			ps = (PreparedStatement) conn.prepareStatement(sql);
			for (int i = 0; i < params.size(); i++) {
				if(params.get(i) instanceof String){
					ps.setString(i+1, params.get(i).toString());
				}else if(params.get(i) instanceof Integer){
					ps.setInt(i+1, (int) params.get(i));
				}else if(params.get(i) instanceof Long){
					ps.setLong(i+1, (long) params.get(i));
				}else if(params.get(i) instanceof Double){
					ps.setDouble(i+1, (double) params.get(i));
				}else if(params.get(i) instanceof Float){
					ps.setFloat(i+1, (float) params.get(i));
				}else if(params.get(i)==null){
					ps.setNull(i+1,Types.NULL);						
				}else{
					ps.setString(i+1, params.get(i).toString());				
				}
			}
			// 获取结果集
			rs = ps.executeQuery();
			ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
			JSONArray ja = new JSONArray();
			try {
				while(rs.next()){
					JSONObject jo = new JSONObject();
					for (int i = 1; i <= rsmd.getColumnCount(); i++) {
						String colName = rsmd.getColumnLabel(i);
						String colValue = rs.getString(i);
						jo.put(colName, colValue);					
					}
					ja.put(jo);
				}} catch (JSONException e) {
					return null;
				}
			return ja;
		}finally{
			safeClose(rs);
			safeClose(ps);
			safeClose(conn);
		}
	}
	
	public static JSONArray getRowsToJSONArray2(String sql,ArrayList<Object> params, List<String> list) throws SQLException {
		m_logger.info(getPreparedSQL(sql, params));
		Connection conn =null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		try{
			conn = m_connectionPool.getConnection();
			ps = (PreparedStatement) conn.prepareStatement(sql);
			for (int i = 0; i < params.size(); i++) {
				if(params.get(i) instanceof String){
					ps.setString(i+1, params.get(i).toString());
				}else if(params.get(i) instanceof Integer){
					ps.setInt(i+1, (int) params.get(i));
				}else if(params.get(i) instanceof Long){
					ps.setLong(i+1, (long) params.get(i));
				}else if(params.get(i) instanceof Double){
					ps.setDouble(i+1, (double) params.get(i));
				}else if(params.get(i) instanceof Float){
					ps.setFloat(i+1, (float) params.get(i));
				}else if(params.get(i)==null){
					ps.setNull(i+1,Types.NULL);						
				}else{
					ps.setString(i+1, params.get(i).toString());				
				}
			}
			// 获取结果集
			rs = ps.executeQuery();
			ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
			JSONArray ja = new JSONArray();
			try {
				while(rs.next()){
					JSONObject jo = new JSONObject();
					for (int i = 1; i <= rsmd.getColumnCount(); i++) {
						String colName = rsmd.getColumnLabel(i);
						String colValue = rs.getString(i);
						if(colName.equals("uid")) {
							if(colValue!=null){
								list.add(colValue);
							}
							
						}
						jo.put(colName, colValue);					
					}
					ja.put(jo);
				}} catch (JSONException e) {
					return null;
				}
			return ja;
		}finally{
			safeClose(rs);
			safeClose(ps);
			safeClose(conn);
		}
	}
	
	/**
	 * 获取一行中一个字段值
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public static String getOneColumnByRow(String sql,ArrayList<Object> params) throws SQLException {
		if (m_logger.isDebugEnabled()) {
			m_logger.debug(getPreparedSQL(sql, params));
		}
		Connection conn=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		try{
			conn = m_connectionPool.getConnection();
			ps = (PreparedStatement) conn.prepareStatement(sql);
			for (int i = 0; i < params.size(); i++) {
				if(params.get(i) instanceof String){
					ps.setString(i+1, params.get(i).toString());
				}else if(params.get(i) instanceof Integer){
					ps.setInt(i+1, (int) params.get(i));
				}else if(params.get(i) instanceof Long){
					ps.setLong(i+1, (long) params.get(i));
				}else if(params.get(i) instanceof Double){
					ps.setDouble(i+1, (double) params.get(i));
				}else if(params.get(i) instanceof Float){
					ps.setFloat(i+1, (float) params.get(i));
				}else if(params.get(i)==null){
					ps.setNull(i+1,Types.NULL);						
				}else{
					ps.setString(i+1, params.get(i).toString());				
				}
			}

			// 获取结果集
			rs = ps.executeQuery();
			String result=null;
			while(rs.next()){
				result = rs.getString(1);
			}
			return result;
		}finally{
			safeClose(rs);
			safeClose(ps);
			safeClose(conn);
		}
	}
	
	/**
	 * 获取一列值
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<String> getOneColumns(String sql,ArrayList<Object> params) throws SQLException {
		if (m_logger.isDebugEnabled()) {
			m_logger.debug(getPreparedSQL(sql, params));
		}
		Connection conn=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		try{
			conn = m_connectionPool.getConnection();
			ps = (PreparedStatement) conn.prepareStatement(sql);
			for (int i = 0; i < params.size(); i++) {
				if(params.get(i) instanceof String){
					ps.setString(i+1, params.get(i).toString());
				}else if(params.get(i) instanceof Integer){
					ps.setInt(i+1, (int) params.get(i));
				}else if(params.get(i) instanceof Long){
					ps.setLong(i+1, (long) params.get(i));
				}else if(params.get(i) instanceof Double){
					ps.setDouble(i+1, (double) params.get(i));
				}else if(params.get(i) instanceof Float){
					ps.setFloat(i+1, (float) params.get(i));
				}else if(params.get(i)==null){
					ps.setNull(i+1,Types.NULL);						
				}else{
					ps.setString(i+1, params.get(i).toString());				
				}
			}
			// 获取结果集
			rs = ps.executeQuery();
			ArrayList<String> result=new ArrayList<String>();
			while(rs.next()){
				result.add(rs.getString(1));
			}
			return result;
		}finally{
			safeClose(rs);
			safeClose(ps);
			safeClose(conn);
		}
	}
	
	public static HashMap<String,String> getOneRow(String sql,ArrayList<Object> params) throws SQLException {
		if (m_logger.isDebugEnabled()) {
			m_logger.debug(getPreparedSQL(sql, params));
		}
		Connection conn=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		try{
			conn = m_connectionPool.getConnection();
			ps = (PreparedStatement) conn.prepareStatement(sql);
			for (int i = 0; i < params.size(); i++) {
				if(params.get(i) instanceof String){
					ps.setString(i+1, params.get(i).toString());
				}else if(params.get(i) instanceof Integer){
					ps.setInt(i+1, (int) params.get(i));
				}else if(params.get(i) instanceof Long){
					ps.setLong(i+1, (long) params.get(i));
				}else if(params.get(i) instanceof Double){
					ps.setDouble(i+1, (double) params.get(i));
				}else if(params.get(i) instanceof Float){
					ps.setFloat(i+1, (float) params.get(i));
				}else if(params.get(i)==null){
					ps.setNull(i+1,Types.NULL);						
				}else{
					ps.setString(i+1, params.get(i).toString());				
				}
			}
			// 获取结果集
			rs = ps.executeQuery();
			ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
			HashMap<String,String> row=new HashMap<String,String>();
			while(rs.next()){
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					String colName = rsmd.getColumnLabel(i);
					String colValue = rs.getString(i);
					row.put(colName, colValue);
				}
			}
			return row;
		}finally{
			safeClose(rs);
			safeClose(ps);
			safeClose(conn);
		}
	}

	
	public static boolean batchUpdateSql(String[] sql){
		
		Connection conn=null;
		PreparedStatement ps=null;
		try{
			conn = m_connectionPool.getConnection();
			conn.setAutoCommit(false);		
			for(int i=0;i<sql.length;i++){
				ps = (PreparedStatement) conn.prepareStatement(sql[i]);
				ps.execute();
			}
			conn.commit();
			return true;
		}catch(SQLException se){
			try {
				if(conn != null) {
					conn.rollback();
				}
			} catch (SQLException e) {
				m_logger.error("", e);
			}
		}
		finally{
			safeClose(ps);
			safeClose(conn);		
		}
		return false;
	}
	
	
	public static boolean batchUpdateSql(List<String> sql,ArrayList<ArrayList<Object>> paramGroups){		
		Connection conn=null;
		PreparedStatement ps=null;
		try{
			conn = m_connectionPool.getConnection();
			conn.setAutoCommit(false);			
			for(int j=0;j<sql.size();j++){
				ps = (PreparedStatement) conn.prepareStatement(sql.get(j));	
				ArrayList<Object> params = paramGroups.get(j);
				if (m_logger.isDebugEnabled()) {
					m_logger.debug(getPreparedSQL(sql.get(j), params));
				}
				for (int i = 0; i < params.size(); i++) {
					if(params.get(i) instanceof String){
						ps.setString(i+1, params.get(i).toString());
					}else if(params.get(i) instanceof Integer){
						ps.setInt(i+1, (int) params.get(i));
					}else if(params.get(i) instanceof Long){
						ps.setLong(i+1, (long)params.get(i));
					}else if(params.get(i) instanceof Double){
						ps.setDouble(i+1, (double) params.get(i));
					}else if(params.get(i) instanceof Float){
						ps.setFloat(i+1, (float) params.get(i));
					}else if(params.get(i)==null){
						ps.setNull(i+1,Types.NULL);						
					}else{
						ps.setString(i+1, params.get(i).toString());				
					}
				}	
				ps.execute();
			}
			conn.commit();
			return true;
		}catch(Exception se){
			m_logger.error("batchUpdateSql error", se);
			try {
				if(conn != null) {
					conn.rollback();
				}
			} catch (SQLException e) {
				m_logger.error("batchUpdateSql error", e);
			}
		}
		finally{
			safeClose(ps);
			safeClose(conn);		
		}
		return false;
	}
	
	
	public static int updateSQL(String sql, ArrayList<Object> params)throws SQLException {
		if (m_logger.isDebugEnabled()) {
			m_logger.debug(getPreparedSQL(sql, params));
		}
		getPreparedSQL(sql, params);
		Connection conn =null;
		PreparedStatement ps = null;
		try{
			conn = m_connectionPool.getConnection();
			ps = (PreparedStatement) conn.prepareStatement(sql);
			for (int i = 0; i < params.size(); i++) {
				if(params.get(i) instanceof String){
					ps.setString(i+1, params.get(i).toString());
				}else if(params.get(i) instanceof Integer){
					ps.setInt(i+1, (int) params.get(i));
				}else if(params.get(i) instanceof Long){
					ps.setLong(i+1, (long) params.get(i));
				}else if(params.get(i) instanceof Double){
					ps.setDouble(i+1, (double) params.get(i));
				}else if(params.get(i) instanceof Float){
					ps.setFloat(i+1, (float) params.get(i));
				}else if(params.get(i)==null){
					ps.setNull(i+1,Types.NULL);
				}else{
					ps.setString(i+1, params.get(i).toString());
				}
			}
			// 获取影响结果行
			int updateCount = ps.executeUpdate();
			return updateCount;
		}finally{
			safeClose(ps);
			safeClose(conn);
		}
	}



	public static Connection getBaseMysqlConn() throws SQLException {
		return m_connectionPool.getConnection();
	}
	
	

	/**
	 * 执行一个存储过程。目前参数仅支持字符串、整型；返回值也只支持整型。
	 * 

	 *            db 连接
	 * @param sql
	 *            存储过程的 SQL，如："{ call p_nwc_exit(?, ?, ?) }"
	 * @param params
	 *            输入参数。
	 * @return 返回值，整数
	 * @throws SQLException
	 */
	public static void executeStoreProcedure(String sql,ArrayList<Object> params,int retNum,HashMap<Integer,String> out) throws SQLException {
		if (m_logger.isDebugEnabled()) {
			m_logger.debug(getPreparedSQL(sql, params));
		}
		Connection conn=null;
		CallableStatement cs=null;
		try{
			conn = m_connectionPool.getConnection();		
			cs = conn.prepareCall(sql);
			// 设置 SP 的输入参数，暂时只支持字符串和整型
			for (int i = 0; i < params.size(); i++) {
				if(params.get(i) instanceof String){
					cs.setString(i+1, params.get(i).toString());
				}else if(params.get(i) instanceof Integer){
					cs.setInt(i+1, (int) params.get(i));
				}else if(params.get(i) instanceof Long){
					cs.setLong(i+1, (long) params.get(i));
				}else if(params.get(i) instanceof Double){
					cs.setDouble(i+1, (double) params.get(i));
				}else if(params.get(i) instanceof Float){
					cs.setFloat(i+1, (float) params.get(i));
				}else if(params.get(i)==null){
					cs.setNull(i+1,Types.NULL);				
				}else{
					cs.setString(i+1, params.get(i).toString());				
				}
			}
			// 设置 SP 的输出，暂时只支持整型
			for(int i=0;i<retNum;i++){
				cs.registerOutParameter(params.size() + i+1, Types.VARCHAR);
			}
			if (!cs.execute()) {
				for(int i=0;i<retNum;i++){
					out.put(i+1,cs.getString(params.size() + i+1)); // 返回值
				}
			}
		}finally{
			safeClose(cs);
			safeClose(conn);
		}		
	}


	/**
	 * 安全关闭结果集
	 * 
	 * @param rs
	 */
	public static void safeClose(ResultSet rs) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception ex) {
			m_logger.warn("Close ResultSet failed.", ex);
		}
	}

	/**
	 * 安全关闭 PreparedStatement
	 * 
	 * @param stat
	 */
	public static void safeClose(PreparedStatement stat) {
		try {
			if (stat != null)
				stat.close();
		} catch (Exception ex) {
			m_logger.warn("Close ResultSet failed.", ex);
		}
	}

	/**
	 * 安全关闭 Connection
	 * 
	 * @param conn
	 */
	public static void safeClose(Connection conn) {
		try {
			if (conn != null)
				conn.close();
		} catch (Exception ex) {
			m_logger.warn("Close db connection failed.", ex);
		}
	}


	/**
	 * 获得PreparedStatement向数据库提交的SQL语句
	 */
	public static String getPreparedSQL(String sql, ArrayList<Object> params) {
		if (1 > params.size())
			return sql;
		StringBuffer returnSQL = new StringBuffer();
		String[] subSQL = sql.split("\\?");
		for (int i = 0; i < params.size(); i++) {
			if(params.get(i) instanceof String){
				returnSQL.append(subSQL[i]).append("'").append(params.get(i)).append("'");
			}
			else{
				returnSQL.append(subSQL[i]).append(params.get(i));
			}
		}
		if (subSQL.length > params.size()) {
			returnSQL.append(subSQL[subSQL.length - 1]);
		}
		return returnSQL.toString();
	}




}
