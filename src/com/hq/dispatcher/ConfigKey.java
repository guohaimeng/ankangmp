package com.hq.dispatcher;

/**
 * 保存配置文件中的 Key 值
 * 
 */
public interface ConfigKey {
	
	/**
	 * 定义一个唯一的SERVLET API服务器，保证产生全局唯一的消息ID号
	 */
	public static final String KEY_SERVLET_ID = "servlet.id";
	
	/**
	 * 基础MYSQL数据库
	 * 需要从该数据库获取分片信息
	 */
	public static final String KEY_MYSQL_BASE_INFO = "mysql.base.info";
	
	public static final String KEY_SQLSERVER_BASE_INFO = "sqlserver.base.info";
	
	public static final String KEY_REDIS_BASE_INFO = "redis.base.info";
	public static final String KEY_REDIS_CACHE_VERSION = "redis.cache.version";

	public static final String KEY_POOL_MIN_SIZE = "c3p0.min.size";
	public static final int V_POOL_MIN_SIZE = 5;

	public static final String KEY_POOL_MAX_SIZE = "c3p0.max.size";
	public static final int V_POOL_MAX_SIZE = 20;

	public static final String KEY_POOL_TIMEOUT = "c3p0.timeout";
	public static final int V_POOL_TIMEOUT = 3600;

	public static final String KEY_POOL_MAX_SQL = "c3p0.max.statements";

	public static final String KEY_TESTCONNECTIONONCHECKIN = "c3p0.testConnectionOnCheckin";

	public static final String KEY_AUTOMATICTESTTABLE = "c3p0.automaticTestTable";

	public static final String KEY_IDLECONNECTIONTESTPERIOD = "c3p0.idleConnectionTestPeriod";

	public static final String KEY_MAXIDLETIME = "c3p0.maxIdleTime";

	public static final String KEY_TESTCONNECTIONONCHECKOUT = "c3p0.testConnectionOnCheckout";

	public static final int V_POOL_MAX_SQL = 100;

	public static final String KEY_POOL_VALIDATE = "c3p0.validate";
	
	public static final boolean V_POOL_VALIDATE = false;
	
	public static final String VERSION = "1.0"; 
	
	public static final String KEY_THUMB_WIDTH = "thumb.width";
	public static final int V_THUMB_WIDTH = 450;
	
	public static final String KEY_THUMB_HEIGHT = "thumb.height";
	public static final int V_THUMB_HEIGHT = 450;
	
	public static final String KEY_AVATAR_THUMB_WIDTH = "avatar.thumb.width";
	public static final int V_AVATAR_THUMB_WIDTH = 450;
	
	public static final String KEY_AVATAR_THUMB_HEIGHT = "avatar.thumb.height";
	public static final int V_AVATAR_THUMB_HEIGHT = 450;
	
	public static final String KEY_FDFS_HOST = "fdfs.host";
	//public static final String KEY_FDFS_PORT = "fdfs.port";
	public static final String KEY_FDFS_LOCAL_PORT = "fdfs.local.port";
	public static final String KEY_FDFS_DOWNLOAD = "fdfs.download";
	public static final String KEY_MAX_FILE_LIMIT = "max.file.limit";
/*	public static final int V_MAX_HEADIMG_FILE_LIMIT = 1;
	public static final int V_MAX_FILE_LIMIT = 4;*/
	
	public static final String PIN_TRY_COUNT = "pincode.count";
	public static final String PIN_TIMEOUT = "pincode.timeout";//在规定的时间内产生相同的pincode码

	public static final String MAIL_SMTP = "mail.smtp";
	public static final String MAIL_USER = "mail.user";
	public static final String MAIL_PASS = "mail.pass";
	public static final String MAIL_RES_PASS = "mail.res.pass";
	
	public static final String SMS_API = "sms.server.api";
	public static final String SMS_TPL = "sms.tpl";
	public static final String SMS_USER = "sms.server.name";
	public static final String SMS_PASS = "sms.server.pass";

}